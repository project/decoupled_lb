/** @type {import("ts-jest").JestConfigWithTsJest} */

import { pathsToModuleNameMapper } from "ts-jest";

export default {
  preset: "ts-jest",
  moduleNameMapper: {
    ...pathsToModuleNameMapper(
      {
        "@/*": [`<rootDir>/src/*`],
      },
      { useESM: true },
    ),
    ".+\\.(pcss)$": `identity-obj-proxy`,
  },
  testMatch: [
    "**/__tests__/**/*.test.[jt]s?(x)",
    "**/?(*.)+(spec|test).[jt]s?(x)",
  ],
  testEnvironment: "jsdom",
  collectCoverageFrom: [
    "./src/**/*.{ts,tsx}",
    "!./src/stories/**",
    "!./src/**/*.stories.ts",
  ],
  transform: {
    "^.+\\.[tj]sx?$": "ts-jest",
    "^.+\\.mdx?$": "@storybook/addon-docs/jest-transform-mdx",
  },
  setupFilesAfterEnv: ["<rootDir>/jest-setup.ts"],
};
