import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";

import { DrupalState } from "@/types/drupal.types.ts";

const setBaseUrl: CaseReducer<DrupalState, PayloadAction<string>> = (
  state,
  action,
) => ({
  ...state,
  baseUrl: action.payload,
});

export default setBaseUrl;
