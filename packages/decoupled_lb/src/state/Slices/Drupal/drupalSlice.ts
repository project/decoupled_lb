import { createSlice } from "@reduxjs/toolkit";
import reducers from "./reducers";
import { RootState } from "../../store.ts";
import { DrupalState } from "@/types/drupal.types.ts";

const INITIAL_STATE: DrupalState = {
  baseUrl: "/",
};

const SLICE_NAME = "drupal";

const drupalSlice = createSlice({
  name: SLICE_NAME,
  initialState: INITIAL_STATE,
  reducers,
});

export default drupalSlice;

export const { setBaseUrl } = drupalSlice.actions;
export const selectBaseUrl = (state: RootState) => state[SLICE_NAME].baseUrl;
