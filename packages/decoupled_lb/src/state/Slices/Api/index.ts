import {
  BaseQueryApi,
  BaseQueryFn,
  createApi,
  FetchArgs,
  fetchBaseQuery,
  FetchBaseQueryError,
} from "@reduxjs/toolkit/query/react";
import {
  BlockInfo,
  BlockList,
  SectionInfo,
  SectionList,
} from "@/types/block.types.ts";
import { LayoutIdentifiers } from "@/types/layout.types.ts";
import { RootState } from "@/state/store.ts";
import {
  createEntityAdapter,
  createSelector,
  EntityState,
} from "@reduxjs/toolkit";
import {
  DisplayData,
  DisplayResponse,
  TypedDisplayIdentifiers,
} from "@/types/displayMode.types.ts";
import { HttpError } from "@/utilities/HttpError.ts";

export type PartialLayoutIdentifiers = Pick<
  LayoutIdentifiers,
  "sectionStorage" | "sectionStorageType"
>;
const rawBaseQuery = (identifiers: LayoutIdentifiers) => {
  const { baseUrl, sectionStorageType, sectionStorage } = identifiers;
  const defaultQuery = fetchBaseQuery({
    baseUrl,
  });
  return async (
    arg: string | FetchArgs,
    api: BaseQueryApi,
    extraOptions: object = {},
  ) => {
    const url = typeof arg == "string" ? arg : arg.url;
    const newUrl = url
      .replace("{section_storage_type}", sectionStorageType)
      .replace("{section_storage}", sectionStorage);
    const newArg = typeof arg == "string" ? newUrl : { ...arg, url: newUrl };
    return defaultQuery(newArg, api, extraOptions);
  };
};

export const baseQuery: BaseQueryFn<
  string | FetchArgs,
  unknown,
  FetchBaseQueryError
> = async (args, api, extraOptions) => {
  const state = api.getState() as RootState;
  const baseUrl = state.drupal.baseUrl;
  if (!baseUrl) {
    return {
      error: {
        status: 400,
        statusText: "Bad Request",
        data: "No host found",
      },
    };
  }
  return rawBaseQuery({
    baseUrl,
    sectionStorageType: state.layout.sectionStorageType,
    sectionStorage: state.layout.sectionStorage,
  })(args, api, extraOptions);
};

const sectionsAdapter = createEntityAdapter<SectionInfo>({
  selectId: (section) => section.id,
});
const initialSectionState = sectionsAdapter.getInitialState({
  status: "idle",
  error: null,
});

export type SectionListState = EntityState<SectionInfo> & {
  status: string;
  error: null;
};

const blocksAdapter = createEntityAdapter<BlockInfo>({
  selectId: (block) => block.id,
});
const initialBlockState = blocksAdapter.getInitialState({
  status: "idle",
  error: null,
});

export type BlockListState = EntityState<BlockInfo> & {
  status: string;
  error: null;
};

export const drupalApi = createApi({
  reducerPath: "drupalApi",
  baseQuery,
  endpoints: (builder) => ({
    getBlockList: builder.query<BlockListState, void>({
      query: () =>
        `api/v1/layout-builder/blocks/{section_storage_type}/{section_storage}`,
      transformResponse: (responseData: BlockList) => {
        return blocksAdapter.setAll(
          initialBlockState,
          Object.values(responseData.blocks),
        );
      },
    }),
    getSectionList: builder.query<SectionListState, void>({
      query: () =>
        `api/v1/layout-builder/sections/{section_storage_type}/{section_storage}`,
      transformResponse: (responseData: SectionList) => {
        return sectionsAdapter.setAll(
          initialSectionState,
          Object.values(responseData.sections),
        );
      },
    }),
    getDisplayPlugin: builder.query<DisplayData, TypedDisplayIdentifiers>({
      query: (display: TypedDisplayIdentifiers) => {
        const { type, viewMode } = display;
        return `jsonapi/entity_${type}_display/entity_${type}_display/${viewMode}`;
      },
      transformResponse: (responseData: DisplayResponse, _meta, display) => {
        const { type, viewMode } = display;
        if (!responseData.data.id) {
          throw new HttpError("No such display", {
            fetch: {
              message: `Entity ${type} display ${viewMode} does not exist`,
              identifier: "fetch",
            },
          });
        }
        return {
          ...display,
          components: responseData.data.attributes.content,
        };
      },
    }),
  }),
});

export const {
  useGetBlockListQuery,
  useGetSectionListQuery,
  useGetDisplayPluginQuery,
} = drupalApi;

export const selectSectionsResult = drupalApi.endpoints.getSectionList.select();

const selectSection = createSelector(
  selectSectionsResult,
  (sectionsResult) => sectionsResult.data,
);
export const {
  selectAll: selectAllSections,
  selectById: selectSectionById,
  selectEntities: selectKeyedSections,
} = sectionsAdapter.getSelectors(
  (state: RootState) => selectSection(state) ?? initialSectionState,
);

export const selectBlocksResult = drupalApi.endpoints.getBlockList.select();

const selectBlock = createSelector(
  selectBlocksResult,
  (blocksResult) => blocksResult.data,
);
export const {
  selectAll: selectAllBlocks,
  selectById: selectBlockById,
  selectEntities: selectKeyedBlocks,
} = blocksAdapter.getSelectors(
  (state: RootState) => selectBlock(state) ?? initialBlockState,
);
