import { createSlice } from "@reduxjs/toolkit";
import reducers from "./reducers";
import { RootState } from "../../store.ts";
import {
  MODE_SELECT,
  SIDEBAR_TAB_ENTITY,
  SIDEBAR_TAB_INSERT,
  UiState,
} from "@/types/ui.types.ts";

const INITIAL_STATE: UiState = {
  activeStartSidebarTab: SIDEBAR_TAB_INSERT,
  mode: MODE_SELECT,
  selectedComponent: null,
  selectedSection: null,
  startSidebarOpen: false,
  primaryAction: {
    label: "Save",
  },
  secondaryActions: [],
  endSidebarOpen: false,
  activeEndSidebarTab: SIDEBAR_TAB_ENTITY,
  activeDrag: null,
};

const SLICE_NAME = "ui";

const uiSlice = createSlice({
  name: SLICE_NAME,
  initialState: INITIAL_STATE,
  reducers,
});

export default uiSlice;

export const {
  setUnsavedChanges,
  setEndSidebarOpen,
  setStartSidebarOpen,
  setActiveSidebarStartPane,
  setSelectedComponent,
  setActiveDrag,
  setActiveSidebarEndPane,
  setMode,
  setSelectedSection,
} = uiSlice.actions;

export const selectorUiState = (state: RootState) => state[SLICE_NAME];
export const selectSelectedComponent = (state: RootState) =>
  state[SLICE_NAME].selectedComponent;

export const selectSelectedSection = (state: RootState) =>
  state[SLICE_NAME].selectedSection;
export const selectMode = (state: RootState) => state[SLICE_NAME].mode;
export const selectActiveDrag = (state: RootState) =>
  state[SLICE_NAME].activeDrag;
