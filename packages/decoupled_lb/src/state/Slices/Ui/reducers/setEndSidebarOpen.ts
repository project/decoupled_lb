import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";

import { UiState } from "@/types/ui.types.ts";

const setEndSidebarOpen: CaseReducer<UiState, PayloadAction<boolean>> = (
  state,
  action,
) => ({
  ...state,
  endSidebarOpen: action.payload,
});

export default setEndSidebarOpen;
