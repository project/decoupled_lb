import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";
import { StartSidebarTab, UiState } from "@/types/ui.types.ts";

const setActiveSidebarStartPane: CaseReducer<
  UiState,
  PayloadAction<StartSidebarTab>
> = (state, action) => ({
  ...state,
  startSidebarOpen: true,
  activeStartSidebarTab: action.payload,
});

export default setActiveSidebarStartPane;
