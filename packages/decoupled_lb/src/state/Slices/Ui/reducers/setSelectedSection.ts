import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";
import { UiState } from "@/types/ui.types.ts";

const setSelectedSection: CaseReducer<UiState, PayloadAction<string | null>> = (
  state,
  action,
) => ({
  ...state,
  selectedSection: action.payload,
});

export default setSelectedSection;
