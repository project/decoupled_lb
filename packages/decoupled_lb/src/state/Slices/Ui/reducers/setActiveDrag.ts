import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";
import { ActiveDrag, UiState } from "@/types/ui.types.ts";

const setActiveDrag: CaseReducer<UiState, PayloadAction<ActiveDrag>> = (
  state,
  action,
) => ({
  ...state,
  activeDrag: action.payload,
});

export default setActiveDrag;
