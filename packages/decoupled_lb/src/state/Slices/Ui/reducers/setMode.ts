import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";
import { Mode, UiState } from "@/types/ui.types.ts";

const setMode: CaseReducer<UiState, PayloadAction<Mode>> = (state, action) => ({
  ...state,
  mode: action.payload,
});

export default setMode;
