import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";
import { Mode, UiState } from "@/types/ui.types.ts";

export interface SetSelectedBlockPayload {
  uuid: string;
  mode?: Mode;
}

export interface ClearSelectedBlockPayload {
  uuid: null;
  mode?: Mode;
}
const setSelectedComponent: CaseReducer<
  UiState,
  PayloadAction<SetSelectedBlockPayload | ClearSelectedBlockPayload>
> = (state, action) => ({
  ...state,
  selectedComponent: action.payload.uuid,
  selectedSection: null,
  mode: action.payload.mode ? action.payload.mode : state.mode,
});

export default setSelectedComponent;
