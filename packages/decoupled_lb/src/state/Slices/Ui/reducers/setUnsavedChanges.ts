import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";

import { UiState } from "@/types/ui.types.ts";

const setUnsavedChanges: CaseReducer<UiState, PayloadAction<boolean>> = (
  state,
  action,
) => ({
  ...state,
  hasUnsavedChanges: action.payload,
});

export default setUnsavedChanges;
