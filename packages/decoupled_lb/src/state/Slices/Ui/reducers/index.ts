import setEndSidebarOpen from "./setEndSidebarOpen.ts";
import setUnsavedChanges from "./setUnsavedChanges.ts";
import setSelectedComponent from "./setSelectedComponent.ts";
import setActiveDrag from "./setActiveDrag.ts";
import setStartSidebarOpen from "@/state/Slices/Ui/reducers/setStartSidebarOpen.ts";
import setActiveSidebarStartPane from "@/state/Slices/Ui/reducers/setActiveSidebarStartPane.ts";
import setActiveSidebarEndPane from "@/state/Slices/Ui/reducers/setActiveSidebarEndPane.ts";
import setMode from "@/state/Slices/Ui/reducers/setMode.ts";
import setSelectedSection from "@/state/Slices/Ui/reducers/setSelectedSection.ts";

export type {
  ClearSelectedBlockPayload,
  SetSelectedBlockPayload,
} from "./setSelectedComponent.ts";

export default {
  setActiveDrag,
  setUnsavedChanges,
  setEndSidebarOpen,
  setMode,
  setStartSidebarOpen,
  setSelectedComponent,
  setActiveSidebarStartPane,
  setActiveSidebarEndPane,
  setSelectedSection,
};
