import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";
import { EndSidebarTab, UiState } from "@/types/ui.types.ts";

const setActiveSidebarEndPane: CaseReducer<
  UiState,
  PayloadAction<EndSidebarTab>
> = (state, action) => ({
  ...state,
  endSidebarOpen: true,
  activeEndSidebarTab: action.payload,
});

export default setActiveSidebarEndPane;
