import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";

import { UiState } from "@/types/ui.types.ts";

const setStartSidebarOpen: CaseReducer<UiState, PayloadAction<boolean>> = (
  state,
  action,
) => ({
  ...state,
  startSidebarOpen: action.payload,
});

export default setStartSidebarOpen;
