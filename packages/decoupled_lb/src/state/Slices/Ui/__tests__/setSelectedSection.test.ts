import { storeFactory } from "@/state/store.ts";
import { setSelectedSection } from "@/state/Slices/Ui/uiSlice.ts";

describe("Set selected section", () => {
  it("Should set selected section", () => {
    const store = storeFactory({
      baseUrl: "/",
      sectionStorageType: "overrides",
      sectionStorage: "node.5",
    });
    store.dispatch(setSelectedSection("abcd"));
    expect(store.getState().ui.selectedSection).toEqual("abcd");
  });
  it("Should clear selected section", () => {
    const store = storeFactory({
      baseUrl: "/",
      sectionStorageType: "overrides",
      sectionStorage: "node.5",
    });
    store.dispatch(setSelectedSection(null));
    expect(store.getState().ui.selectedSection).toBeNull();
  });
});
