import layoutSlice from "./Layout/layoutSlice.ts";
import uiSlice from "@/state/Slices/Ui/uiSlice.ts";
import drupalSlice from "./Drupal/drupalSlice.ts";
import { drupalApi } from "@/state/Slices/Api";
export default {
  [layoutSlice.name]: layoutSlice.reducer,
  [uiSlice.name]: uiSlice.reducer,
  [drupalSlice.name]: drupalSlice.reducer,
  [drupalApi.reducerPath]: drupalApi.reducer,
};
