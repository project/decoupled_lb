import { ErrorSet } from "@/types/errorSet.ts";
import { LayoutIdentifiers, ReceivedLayout } from "@/types/layout.types.ts";
import { HttpError } from "@/utilities/HttpError.ts";

export const doFetchLayout = ({
  baseUrl,
  sectionStorage,
  sectionStorageType,
}: LayoutIdentifiers): Promise<ReceivedLayout> =>
  fetch(
    `${baseUrl}api/v1/layout-builder/layout/${sectionStorageType}/${sectionStorage}`,
  ).then(async (r) => {
    if (r.ok) {
      const json = (await r.json()) as { data: ReceivedLayout };
      return json.data;
    }
    const json = (await r.json()) as { errors: ErrorSet };
    throw new HttpError(r.statusText, json.errors);
  });
