import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";
import { normalizeLayout } from "../utilities.ts";
import { LayoutState, ReceivedLayout } from "@/types/layout.types.ts";
import { STATE_CLEAN } from "@/types/constants.ts";

const loadOrSaveLayoutSuccess: CaseReducer<
  LayoutState,
  PayloadAction<ReceivedLayout>
> = (state, action) => ({
  ...state,
  layout: normalizeLayout(action.payload),
  state: STATE_CLEAN,
});

export default loadOrSaveLayoutSuccess;
