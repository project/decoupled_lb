import { PayloadAction } from "@reduxjs/toolkit";
import { LayoutState } from "@/types/layout.types.ts";
import { LayoutBlock, BlockConfiguration } from "@/types/block.types.ts";
import { Mapping, STATE_DIRTY } from "@/types/constants.ts";
import { NoInfer } from "@reduxjs/toolkit/src/tsHelpers.ts";
import { Draft } from "immer";

export interface BlockUpdatePayload<
  ConfigurationType extends BlockConfiguration = BlockConfiguration,
  AdditionalPropertiesType extends Mapping = Mapping,
> {
  componentId: string;
  values: LayoutBlock<ConfigurationType, AdditionalPropertiesType>;
}
const updateBlock = <
  ConfigurationType extends BlockConfiguration = BlockConfiguration,
  AdditionalPropertiesType extends Mapping = Mapping,
>(
  state: LayoutState,
  action: PayloadAction<
    BlockUpdatePayload<ConfigurationType, AdditionalPropertiesType>
  >,
): NoInfer<LayoutState> | void | Draft<NoInfer<LayoutState>> => ({
  ...state,
  state: STATE_DIRTY,
  layout: {
    ...state.layout,
    components: {
      ...state.layout.components,
      [action.payload.componentId]: action.payload.values,
    },
  },
});

export default updateBlock;
