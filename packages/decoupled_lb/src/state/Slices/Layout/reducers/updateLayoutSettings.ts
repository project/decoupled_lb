import { PayloadAction } from "@reduxjs/toolkit";
import { LayoutSettings, LayoutState } from "@/types/layout.types.ts";
import { NoInfer } from "@reduxjs/toolkit/src/tsHelpers.ts";
import { Draft } from "immer";
import { STATE_DIRTY } from "@/types/constants.ts";

export interface UpdateLayoutSettingsPayload<
  LayoutSettingsType extends LayoutSettings = LayoutSettings,
> {
  sectionId: string;
  layoutSettings: LayoutSettingsType;
}

const updateLayoutSettings = <
  LayoutSettingsType extends LayoutSettings = LayoutSettings,
>(
  state: LayoutState,
  action: PayloadAction<UpdateLayoutSettingsPayload<LayoutSettingsType>>,
): NoInfer<LayoutState> | void | Draft<NoInfer<LayoutState>> => ({
  ...state,
  state: STATE_DIRTY,
  layout: {
    ...state.layout,
    sections: {
      ...state.layout.sections,
      [action.payload.sectionId]: {
        ...state.layout.sections[action.payload.sectionId],
        layout_settings: {
          ...action.payload.layoutSettings,
        },
      },
    },
  },
});

export default updateLayoutSettings;
