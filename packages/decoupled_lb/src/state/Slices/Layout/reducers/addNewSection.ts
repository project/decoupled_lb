import { CaseReducer, nanoid, PayloadAction } from "@reduxjs/toolkit";
import { SectionInfo } from "@/types/block.types.ts";
import { LayoutState } from "@/types/layout.types.ts";
import { sortComponentsByWeight } from "@/state/Slices/Layout/utilities.ts";
import { STATE_DIRTY } from "@/types/constants.ts";

export interface AddNewSectionPayload {
  at: number;
  sectionInfo: SectionInfo;
}

const addNewSection: CaseReducer<
  LayoutState,
  PayloadAction<AddNewSectionPayload>
> = (state, action) => {
  const { sections } = state.layout;
  const { at, sectionInfo } = action.payload;
  const sectionKeys = Object.keys(sections);
  const newItemKey = nanoid(32);
  sectionKeys.splice(at, 0, newItemKey);
  const newRegions = Object.keys(sectionInfo.region_labels).reduce(
    (carry, region) => {
      const newRegionId = nanoid();
      return {
        ...carry,
        [newRegionId]: { id: newRegionId, region, section: newItemKey },
      };
    },
    {},
  );
  return {
    ...state,
    state: STATE_DIRTY,
    layout: sortComponentsByWeight({
      ...state.layout,
      regions: {
        ...state.layout.regions,
        ...newRegions,
      },
      sections: sectionKeys.reduce(
        (newSections, key, weight) => ({
          ...newSections,
          [key]: {
            ...(key === newItemKey
              ? {
                  layout_id: sectionInfo.id,
                  layout_settings: { label: sectionInfo.label },
                  components: {},
                  id: newItemKey,
                }
              : state.layout.sections[key]),
            weight,
          },
        }),
        {},
      ),
    }),
  };
};

export default addNewSection;
