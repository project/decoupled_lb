import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";
import { LayoutState } from "@/types/layout.types.ts";
import { STATE_SAVING } from "@/types/constants.ts";

const saveLayoutStart: CaseReducer<LayoutState, PayloadAction> = (state) => ({
  ...state,
  state: STATE_SAVING,
});

export default saveLayoutStart;
