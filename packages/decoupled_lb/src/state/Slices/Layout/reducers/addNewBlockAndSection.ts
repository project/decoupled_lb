import { CaseReducer, nanoid, PayloadAction } from "@reduxjs/toolkit";
import { LayoutState, LayoutRegion } from "@/types/layout.types.ts";
import { LayoutBlock, BlockInfo, SectionInfo } from "@/types/block.types.ts";
import { sortComponentsByWeight } from "@/state/Slices/Layout/utilities.ts";
import { STATE_DIRTY } from "@/types/constants.ts";

export interface AddNewBlockAndSectionPayload {
  at: number;
  blockInfo: BlockInfo;
  sectionInfo: SectionInfo;
}

const addNewBlockAndSection: CaseReducer<
  LayoutState,
  PayloadAction<AddNewBlockAndSectionPayload>
> = (state, action) => {
  const { sections } = state.layout;
  const { at, sectionInfo, blockInfo } = action.payload;
  const sectionKeys = Object.keys(sections);
  const newItemKey = nanoid(32);
  sectionKeys.splice(at, 0, newItemKey);
  const newRegions: Record<string, LayoutRegion> = Object.keys(
    sectionInfo.region_labels,
  ).reduce((carry, region) => {
    const newRegionId = nanoid();
    return {
      ...carry,
      [newRegionId]: { id: newRegionId, region, section: newItemKey },
    };
  }, {});
  const newComponentId = nanoid(32);
  const newRegionId =
    Object.entries(newRegions)
      // eslint-disable-next-line no-unused-vars,@typescript-eslint/no-unused-vars
      .filter(([_, region]) => region.region === blockInfo.default_region)
      .map(([key]) => key)
      .pop() || "content";
  const newComponent: LayoutBlock = {
    additional: {},
    configuration: {
      ...blockInfo.default_configuration,
      label: blockInfo.label,
    },
    weight: 0,
    uuid: newComponentId,
    regionId: newRegionId,
  };
  return {
    ...state,
    state: STATE_DIRTY,
    layout: sortComponentsByWeight({
      ...state.layout,
      regions: {
        ...state.layout.regions,
        ...newRegions,
      },
      components: {
        ...state.layout.components,
        [newComponentId]: newComponent,
      },
      sections: sectionKeys.reduce(
        (newSections, key, weight) => ({
          ...newSections,
          [key]: {
            ...(key === newItemKey
              ? {
                  layout_id: sectionInfo.id,
                  layout_settings: { label: sectionInfo.label },
                  id: newItemKey,
                }
              : state.layout.sections[key]),
            weight,
          },
        }),
        {},
      ),
    }),
  };
};

export default addNewBlockAndSection;
