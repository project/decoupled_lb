import loadOrSaveLayoutSuccess from "./loadOrSaveLayoutSuccess.ts";
import reorderLayoutSections from "./reorderLayoutSections.ts";
import moveBlock from "./moveBlock.ts";
import loadLayoutStart from "./loadLayoutStart.ts";
import loadOrSaveLayoutFailure from "./loadOrSaveLayoutFailure.ts";
import updateBlock from "./updateBlock.ts";
import addNewSection from "@/state/Slices/Layout/reducers/addNewSection.ts";
import addNewBlock from "@/state/Slices/Layout/reducers/addNewBlock.ts";
import addNewBlockAndSection from "@/state/Slices/Layout/reducers/addNewBlockAndSection.ts";
import updateLayoutSettings from "@/state/Slices/Layout/reducers/updateLayoutSettings.ts";
import saveLayoutStart from "@/state/Slices/Layout/reducers/saveLayoutStart.ts";

export type { MoveBlockPayload } from "./moveBlock.ts";
export type { SectionReorderPayload } from "./reorderLayoutSections.ts";
export type { BlockUpdatePayload } from "./updateBlock.ts";
export type { AddNewBlockAndSectionPayload } from "./addNewBlockAndSection.ts";
export type { AddNewBlockPayload } from "./addNewBlock.ts";
export type { AddNewSectionPayload } from "./addNewSection.ts";

export default {
  reorderLayoutSections,
  moveBlock,
  loadLayoutStart,
  loadOrSaveLayoutSuccess,
  loadOrSaveLayoutFailure,
  updateBlock,
  addNewSection,
  addNewBlock,
  addNewBlockAndSection,
  updateLayoutSettings,
  saveLayoutStart,
};
