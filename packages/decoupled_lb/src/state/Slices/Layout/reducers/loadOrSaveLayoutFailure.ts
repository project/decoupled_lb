import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";

import { ErrorSet } from "@/types/errorSet.ts";
import { LayoutState } from "@/types/layout.types.ts";
import { STATE_ERROR } from "@/types/constants.ts";

const loadOrSaveLayoutFailure: CaseReducer<
  LayoutState,
  PayloadAction<ErrorSet>
> = (state, action) => ({
  ...state,
  errors: action.payload,
  state: STATE_ERROR,
});

export default loadOrSaveLayoutFailure;
