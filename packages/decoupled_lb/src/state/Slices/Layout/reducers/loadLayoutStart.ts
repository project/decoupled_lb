import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";
import { LayoutIdentifiers, LayoutState } from "@/types/layout.types.ts";
import { STATE_LOADING } from "@/types/constants.ts";

const loadLayoutStart: CaseReducer<
  LayoutState,
  PayloadAction<LayoutIdentifiers>
> = (state, action) => ({
  ...state,
  state: STATE_LOADING,
  sectionStorage: action.payload.sectionStorage,
  sectionStorageType: action.payload.sectionStorageType,
});

export default loadLayoutStart;
