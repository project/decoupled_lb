import moveBlock from "../moveBlock.ts";

import { LayoutState } from "@/types/layout.types.ts";
import { STATE_DIRTY } from "@/types/constants.ts";

describe("Reducer: moveBlock", () => {
  const initialState: LayoutState = {
    errors: {},
    sectionStorageType: "defaults",
    sectionStorage: "node.basic.full",
    layout: {
      components: {
        "5baf6dd5-dd5b-4afd-96a9-4f5c7d148920": {
          uuid: "5baf6dd5-dd5b-4afd-96a9-4f5c7d148920",
          regionId: "section1content",
          configuration: {
            id: "inline_block:basic",
            label_display: false,
            label: "Block 1",
          },
          weight: 0,
          additional: {},
        },
        "46353f65-ce3e-4901-9f9b-5faa943575ff": {
          uuid: "46353f65-ce3e-4901-9f9b-5faa943575ff",
          regionId: "section1content",
          configuration: {
            id: "inline_block:basic",
            label: "Block 2",
            label_display: false,
          },
          weight: 1,
          additional: {},
        },
        "e7b38b4f-88d1-48fd-980a-4afc738c1d51": {
          uuid: "e7b38b4f-88d1-48fd-980a-4afc738c1d51",
          regionId: "section2first",
          configuration: {
            id: "inline_block:basic",
            label_display: false,
            label: "Block 3",
          },
          weight: 2,
          additional: {},
        },
        "745728df-ae48-4137-ac7f-83dba5d00a19": {
          uuid: "745728df-ae48-4137-ac7f-83dba5d00a19",
          regionId: "section2second",
          configuration: {
            id: "inline_block:basic",
            label_display: false,
            label: "Block 4",
          },
          weight: 3,
          additional: {},
        },
      },
      sections: {
        section1: {
          layout_id: "layout_onecol",
          layout_settings: {},
          id: "section1",
          weight: 0,
        },
        section2: {
          layout_id: "layout_twocol_section",
          layout_settings: {},
          id: "section2",
          weight: 1,
        },
      },
      regions: {
        section1content: {
          section: "section1",
          region: "content",
          id: "section1content",
        },
        section2first: {
          section: "section2",
          region: "first",
          id: "section2first",
        },
        section2second: {
          section: "section2",
          region: "second",
          id: "section2second",
        },
      },
    },
    state: STATE_DIRTY,
  };
  it("Should support moving a block inside the same region and sections", () => {
    const state = moveBlock(initialState, {
      type: "layout/moveBlock",
      payload: {
        from: { index: 0, droppableId: "section1content" },
        to: { droppableId: "section1content", index: 1 },
        componentUuid: "5baf6dd5-dd5b-4afd-96a9-4f5c7d148920",
      },
    });
    expect(state).toEqual({
      errors: {},
      sectionStorageType: "defaults",
      sectionStorage: "node.basic.full",
      layout: {
        components: {
          "46353f65-ce3e-4901-9f9b-5faa943575ff": {
            uuid: "46353f65-ce3e-4901-9f9b-5faa943575ff",
            regionId: "section1content",
            configuration: {
              id: "inline_block:basic",
              label: "Block 2",
              label_display: false,
            },
            weight: 0,
            additional: {},
          },
          "5baf6dd5-dd5b-4afd-96a9-4f5c7d148920": {
            uuid: "5baf6dd5-dd5b-4afd-96a9-4f5c7d148920",
            regionId: "section1content",
            configuration: {
              id: "inline_block:basic",
              label_display: false,
              label: "Block 1",
            },
            weight: 1,
            additional: {},
          },
          "e7b38b4f-88d1-48fd-980a-4afc738c1d51": {
            uuid: "e7b38b4f-88d1-48fd-980a-4afc738c1d51",
            regionId: "section2first",
            configuration: {
              id: "inline_block:basic",
              label_display: false,
              label: "Block 3",
            },
            weight: 2,
            additional: {},
          },
          "745728df-ae48-4137-ac7f-83dba5d00a19": {
            uuid: "745728df-ae48-4137-ac7f-83dba5d00a19",
            regionId: "section2second",
            configuration: {
              id: "inline_block:basic",
              label_display: false,
              label: "Block 4",
            },
            weight: 3,
            additional: {},
          },
        },
        sections: {
          section1: {
            layout_id: "layout_onecol",
            layout_settings: {},
            id: "section1",
            weight: 0,
          },
          section2: {
            layout_id: "layout_twocol_section",
            layout_settings: {},
            id: "section2",
            weight: 1,
          },
        },
        regions: {
          section1content: {
            section: "section1",
            region: "content",
            id: "section1content",
          },
          section2first: {
            section: "section2",
            region: "first",
            id: "section2first",
          },
          section2second: {
            section: "section2",
            region: "second",
            id: "section2second",
          },
        },
      },
      state: STATE_DIRTY,
    });
  });
  it("Should support moving a block between regions in the same sections", () => {
    // ignore
    const state = moveBlock(initialState, {
      type: "layout/moveBlock",
      payload: {
        from: { index: 0, droppableId: "section2first" },
        to: { droppableId: "section2second", index: 1 },
        componentUuid: "e7b38b4f-88d1-48fd-980a-4afc738c1d51",
      },
    });
    expect(state).toEqual({
      errors: {},
      sectionStorageType: "defaults",
      sectionStorage: "node.basic.full",
      layout: {
        components: {
          "5baf6dd5-dd5b-4afd-96a9-4f5c7d148920": {
            uuid: "5baf6dd5-dd5b-4afd-96a9-4f5c7d148920",
            regionId: "section1content",
            configuration: {
              id: "inline_block:basic",
              label_display: false,
              label: "Block 1",
            },
            weight: 0,
            additional: {},
          },
          "46353f65-ce3e-4901-9f9b-5faa943575ff": {
            uuid: "46353f65-ce3e-4901-9f9b-5faa943575ff",
            regionId: "section1content",
            configuration: {
              id: "inline_block:basic",
              label: "Block 2",
              label_display: false,
            },
            weight: 2,
            additional: {},
          },
          "745728df-ae48-4137-ac7f-83dba5d00a19": {
            uuid: "745728df-ae48-4137-ac7f-83dba5d00a19",
            regionId: "section2second",
            configuration: {
              id: "inline_block:basic",
              label_display: false,
              label: "Block 4",
            },
            weight: 1,
            additional: {},
          },
          "e7b38b4f-88d1-48fd-980a-4afc738c1d51": {
            uuid: "e7b38b4f-88d1-48fd-980a-4afc738c1d51",
            regionId: "section2second",
            configuration: {
              id: "inline_block:basic",
              label_display: false,
              label: "Block 3",
            },
            weight: 3,
            additional: {},
          },
        },
        sections: {
          section1: {
            layout_id: "layout_onecol",
            layout_settings: {},
            id: "section1",
            weight: 0,
          },
          section2: {
            layout_id: "layout_twocol_section",
            layout_settings: {},
            id: "section2",
            weight: 1,
          },
        },
        regions: {
          section1content: {
            section: "section1",
            region: "content",
            id: "section1content",
          },
          section2first: {
            section: "section2",
            region: "first",
            id: "section2first",
          },
          section2second: {
            section: "section2",
            region: "second",
            id: "section2second",
          },
        },
      },
      state: STATE_DIRTY,
    });
  });
  it("Should support moving a block between two sections", () => {
    const state = moveBlock(initialState, {
      type: "layout/moveBlock",
      payload: {
        from: { index: 0, droppableId: "section1content" },
        to: { droppableId: "section2first", index: 1 },
        componentUuid: "5baf6dd5-dd5b-4afd-96a9-4f5c7d148920",
      },
    });
    expect(state).toEqual({
      errors: {},
      sectionStorageType: "defaults",
      sectionStorage: "node.basic.full",
      layout: {
        components: {
          "46353f65-ce3e-4901-9f9b-5faa943575ff": {
            uuid: "46353f65-ce3e-4901-9f9b-5faa943575ff",
            regionId: "section1content",
            configuration: {
              id: "inline_block:basic",
              label: "Block 2",
              label_display: false,
            },
            weight: 2,
            additional: {},
          },
          "e7b38b4f-88d1-48fd-980a-4afc738c1d51": {
            uuid: "e7b38b4f-88d1-48fd-980a-4afc738c1d51",
            regionId: "section2first",
            configuration: {
              id: "inline_block:basic",
              label_display: false,
              label: "Block 3",
            },
            weight: 0,
            additional: {},
          },
          "5baf6dd5-dd5b-4afd-96a9-4f5c7d148920": {
            uuid: "5baf6dd5-dd5b-4afd-96a9-4f5c7d148920",
            regionId: "section2first",
            configuration: {
              id: "inline_block:basic",
              label_display: false,
              label: "Block 1",
            },
            weight: 1,
            additional: {},
          },
          "745728df-ae48-4137-ac7f-83dba5d00a19": {
            uuid: "745728df-ae48-4137-ac7f-83dba5d00a19",
            regionId: "section2second",
            configuration: {
              id: "inline_block:basic",
              label_display: false,
              label: "Block 4",
            },
            weight: 3,
            additional: {},
          },
        },
        sections: {
          section1: {
            layout_id: "layout_onecol",
            layout_settings: {},
            id: "section1",
            weight: 0,
          },
          section2: {
            layout_id: "layout_twocol_section",
            layout_settings: {},
            id: "section2",
            weight: 1,
          },
        },
        regions: {
          section1content: {
            section: "section1",
            region: "content",
            id: "section1content",
          },
          section2first: {
            section: "section2",
            region: "first",
            id: "section2first",
          },
          section2second: {
            section: "section2",
            region: "second",
            id: "section2second",
          },
        },
      },
      state: STATE_DIRTY,
    });
  });
});
