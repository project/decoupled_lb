import { storeFactory } from "@/state/store.ts";
import { dummyLayoutState } from "@/state/__tests__/DummyLayoutState.ts";
import { updateLayoutSettings } from "@/state/Slices/Layout/layoutSlice.ts";
import { LayoutSettings } from "@/types/layout.types.ts";

describe("Update section label", () => {
  it("Should update section label", () => {
    const store = storeFactory(
      {
        baseUrl: "/",
        sectionStorageType: "overrides",
        sectionStorage: "node.5",
      },
      dummyLayoutState,
    );
    expect(
      store.getState().layout.layout.sections.s1.layout_settings.label,
    ).toEqual("s1");
    store.dispatch(
      updateLayoutSettings({
        sectionId: "s1",
        layoutSettings: { label: "new label" },
      }),
    );
    expect(
      store.getState().layout.layout.sections.s1.layout_settings.label,
    ).toEqual("new label");
  });
  it("Should update arbitray layout settings", () => {
    const store = storeFactory(
      {
        baseUrl: "/",
        sectionStorageType: "overrides",
        sectionStorage: "node.5",
      },
      dummyLayoutState,
    );
    interface ExtendedLayoutSettings extends LayoutSettings {
      displayStyle: "padded" | "full-width";
    }
    const settings = store.getState().layout.layout.sections.s1
      .layout_settings as ExtendedLayoutSettings;
    expect(settings.displayStyle).toEqual(undefined);
    store.dispatch(
      updateLayoutSettings({
        sectionId: "s1",
        layoutSettings: { label: "new label", displayStyle: "padded" },
      }),
    );
    expect(store.getState().layout.layout.sections.s1.layout_settings).toEqual({
      label: "new label",
      displayStyle: "padded",
    });
  });
});
