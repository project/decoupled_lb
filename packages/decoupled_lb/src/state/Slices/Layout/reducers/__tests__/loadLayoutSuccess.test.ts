import layoutSlice, { loadOrSaveLayoutSuccess } from "../../layoutSlice.ts";
import { LayoutState } from "@/types/layout.types.ts";
import { STATE_CLEAN, STATE_LOADING } from "@/types/constants.ts";

describe("Reducer: loadOrSaveLayoutSuccess", () => {
  const INITIAL_STATE: LayoutState = {
    errors: {},
    sectionStorageType: "defaults",
    sectionStorage: "node.basic.full",
    layout: {
      sections: {},
      components: {},
      regions: {},
    },
    state: STATE_LOADING,
  };
  it("Should mutate the loading state", () => {
    expect(
      layoutSlice.reducer(
        INITIAL_STATE,
        loadOrSaveLayoutSuccess({ sections: [] }),
      ).state,
    ).toEqual(STATE_CLEAN);
  });
});
