import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";

import { LayoutState } from "@/types/layout.types.ts";
import { STATE_DIRTY } from "@/types/constants.ts";

export interface SectionReorderPayload {
  from: number;
  to: number;
}
const reorderLayoutSections: CaseReducer<
  LayoutState,
  PayloadAction<SectionReorderPayload>
> = (state, action) => {
  const { sections } = state.layout;
  const { from, to } = action.payload;
  const sectionKeys = Object.keys(sections);
  const reorderedItem = sectionKeys[from];
  const newList = [
    ...sectionKeys.slice(0, from),
    ...sectionKeys.slice(from + 1),
  ];
  newList.splice(to, 0, reorderedItem);
  return {
    ...state,
    state: STATE_DIRTY,
    layout: {
      ...state.layout,
      sections: newList.reduce(
        (newSections, key, weight) => ({
          ...newSections,
          [key]: {
            ...state.layout.sections[key],
            weight,
          },
        }),
        {},
      ),
    },
  };
};

export default reorderLayoutSections;
