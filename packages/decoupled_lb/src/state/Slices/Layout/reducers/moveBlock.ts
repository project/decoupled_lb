import { CaseReducer, PayloadAction } from "@reduxjs/toolkit";
import { DraggableLocation } from "react-beautiful-dnd";
import { sortComponentsByWeight } from "../utilities.ts";
import { LayoutState } from "@/types/layout.types.ts";
import { STATE_DIRTY } from "@/types/constants.ts";

export interface MoveBlockPayload {
  from: DraggableLocation;
  to: DraggableLocation;
  componentUuid: string;
}
const moveBlock: CaseReducer<LayoutState, PayloadAction<MoveBlockPayload>> = (
  state,
  action,
) => {
  const { droppableId: fromRegion, index: indexFrom } = action.payload.from;
  const { droppableId: toRegion, index: indexTo } = action.payload.to;
  const movedComponentId = action.payload.componentUuid;
  // Get the IDs of all blocks in the destination region.
  let destinationComponents = Object.entries(state.layout.components)
    .map(([, component]) => component)
    .filter((component) => component.regionId === toRegion)
    .map((component) => component.uuid);
  // We're moving a block within the same region.
  if (fromRegion === toRegion) {
    // Get the blocks without the moved blocks ID.
    destinationComponents = [
      ...destinationComponents.slice(0, indexFrom),
      ...destinationComponents.slice(indexFrom + 1),
    ];
  }
  // Add the moved block back into the list of components IDs in the destination
  // region at the given index position.
  destinationComponents.splice(indexTo, 0, movedComponentId);
  return {
    ...state,
    state: STATE_DIRTY,
    layout: sortComponentsByWeight({
      regions: state.layout.regions,
      sections: state.layout.sections,
      components: {
        ...state.layout.components,
        // Update all components in the destination region.
        ...destinationComponents.reduce(
          (newComponents, componentId, weight) => ({
            ...newComponents,
            [componentId]: {
              ...state.layout.components[componentId],
              // Give them a new weight based on the new order.
              weight,
              // Update the moved item's region in case of movement between
              // regions. We don't need to limit this to the moved component
              // because at this point we're working only with component IDs
              // that are in this region.
              regionId: toRegion,
            },
          }),
          {},
        ),
      },
    }),
  };
};

export default moveBlock;
