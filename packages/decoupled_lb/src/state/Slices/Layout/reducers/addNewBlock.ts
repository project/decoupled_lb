import { CaseReducer, nanoid, PayloadAction } from "@reduxjs/toolkit";
import { DraggableLocation } from "react-beautiful-dnd";
import { LayoutState } from "@/types/layout.types.ts";
import { LayoutBlock, BlockInfo } from "@/types/block.types.ts";
import { sortComponentsByWeight } from "@/state/Slices/Layout/utilities.ts";
import { STATE_DIRTY } from "@/types/constants.ts";

export interface AddNewBlockPayload {
  at: DraggableLocation;
  blockInfo: BlockInfo;
}
const addNewBlock: CaseReducer<
  LayoutState,
  PayloadAction<AddNewBlockPayload>
> = (state, action) => {
  const { blockInfo } = action.payload;
  const { droppableId: toRegion, index: indexTo } = action.payload.at;
  const newComponentId = nanoid(32);
  const newComponent: LayoutBlock = {
    additional: {},
    configuration: {
      ...blockInfo.default_configuration,
      label: blockInfo.label,
    },
    weight: 0,
    uuid: newComponentId,
    regionId: toRegion,
  };
  // Get the IDs of all blocks in the destination region.
  const destinationComponents = Object.entries(state.layout.components)
    .map(([, component]) => component)
    .filter((component) => component.regionId === toRegion)
    .map((component) => component.uuid);
  // Add the moved block back into the list of components IDs in the destination
  // region at the given index position.
  destinationComponents.splice(indexTo, 0, newComponentId);
  return {
    ...state,
    state: STATE_DIRTY,
    layout: sortComponentsByWeight({
      regions: state.layout.regions,
      sections: state.layout.sections,
      components: {
        ...state.layout.components,
        // Update all components in the destination region.
        ...destinationComponents.reduce(
          (newComponents, componentId, weight) => ({
            ...newComponents,
            [componentId]: {
              ...(componentId === newComponentId
                ? newComponent
                : state.layout.components[componentId]),
              // Give them a new weight based on the new order.
              weight,
            },
          }),
          {},
        ),
      },
    }),
  };
};

export default addNewBlock;
