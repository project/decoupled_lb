import {
  dummyLayoutState,
  dummyLoadedLayout,
} from "@/state/__tests__/DummyLayoutState.ts";
import {
  denormalizeLayout,
  normalizeLayout,
} from "@/state/Slices/Layout/utilities.ts";

describe("Denormalize and normalize layout", () => {
  it("Should denormalize a layout back to shape needed by Drupal", () => {
    const out = denormalizeLayout(dummyLayoutState);
    expect(out).toEqual(dummyLoadedLayout);
  });
  it("Should normalize a layout to the shape needed by the application", () => {
    const inLayout = normalizeLayout(dummyLoadedLayout);
    expect(inLayout.regions[inLayout.components.c1.regionId].region).toEqual(
      "content",
    );
  });
});
