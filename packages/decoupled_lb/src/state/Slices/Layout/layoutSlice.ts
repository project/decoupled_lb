import { createSlice, PayloadAction, ThunkAction } from "@reduxjs/toolkit";
import reducers, { BlockUpdatePayload } from "./reducers";
import { RootState } from "../../store.ts";
import { doFetchLayout } from "./doFetchLayout.ts";

import { ErrorSet } from "@/types/errorSet.ts";
import {
  LayoutIdentifiers,
  LayoutSettings,
  LayoutState,
  ReceivedLayout,
} from "@/types/layout.types.ts";
import { HttpError } from "@/utilities/HttpError.ts";
import { BlockConfiguration } from "@/types/block.types.ts";
import { Mapping, STATE_IDLE, STATE_SAVING } from "@/types/constants.ts";
import { UpdateLayoutSettingsPayload } from "@/state/Slices/Layout/reducers/updateLayoutSettings.ts";
import { doSaveLayout } from "@/state/Slices/Layout/doSaveLayout.ts";

export const INITIAL_STATE: LayoutState = {
  errors: {},
  sectionStorageType: "defaults",
  sectionStorage: "",
  layout: {
    sections: {},
    components: {},
    regions: {},
  },
  state: STATE_IDLE,
};

const SLICE_NAME = "layout";

const layoutSlice = createSlice({
  name: SLICE_NAME,
  initialState: INITIAL_STATE,
  reducers,
});

export default layoutSlice;

export const {
  loadLayoutStart,
  loadOrSaveLayoutSuccess,
  loadOrSaveLayoutFailure,
  reorderLayoutSections,
  moveBlock,
  addNewSection,
  addNewBlockAndSection,
  addNewBlock,
  saveLayoutStart,
} = layoutSlice.actions;

export const updateBlock = <
  ConfigurationType extends BlockConfiguration = BlockConfiguration,
  AdditionalPropertiesType extends Mapping = Mapping,
>(
  payload: BlockUpdatePayload<ConfigurationType, AdditionalPropertiesType>,
): PayloadAction<
  BlockUpdatePayload<ConfigurationType, AdditionalPropertiesType>,
  "layout/updateBlock"
> =>
  layoutSlice.actions.updateBlock(payload) as PayloadAction<
    BlockUpdatePayload<ConfigurationType, AdditionalPropertiesType>,
    "layout/updateBlock"
  >;

export const updateLayoutSettings = <
  LayoutSettingsType extends LayoutSettings = LayoutSettings,
>(
  payload: UpdateLayoutSettingsPayload<LayoutSettingsType>,
): PayloadAction<
  UpdateLayoutSettingsPayload<LayoutSettingsType>,
  "layout/updateLayoutSettings"
> =>
  layoutSlice.actions.updateLayoutSettings(payload) as PayloadAction<
    UpdateLayoutSettingsPayload<LayoutSettingsType>,
    "layout/updateLayoutSettings"
  >;

export const selectLayout = (state: RootState) => state[SLICE_NAME];
export const selectLayoutSections = (state: RootState) =>
  state[SLICE_NAME].layout.sections;

export const selectComponents = (state: RootState) =>
  state[SLICE_NAME].layout.components;

export const selectRegions = (state: RootState) =>
  state[SLICE_NAME].layout.regions;

export const selectLayoutState = (state: RootState) => state[SLICE_NAME].state;

export const selectRegion = (state: RootState, regionId: string) =>
  state[SLICE_NAME].layout.regions[regionId] ?? null;

export const loadLayout =
  (
    layoutIdentifiers: LayoutIdentifiers,
  ): ThunkAction<
    void,
    RootState,
    unknown,
    | PayloadAction<LayoutIdentifiers>
    | PayloadAction<ErrorSet>
    | PayloadAction<ReceivedLayout>
  > =>
  async (dispatch, getState) => {
    const status = selectLayoutState(getState());
    if (status !== STATE_IDLE) {
      // Another component is loading this layout.
      return;
    }
    dispatch(loadLayoutStart(layoutIdentifiers));

    try {
      const data = await doFetchLayout(layoutIdentifiers);
      dispatch(loadOrSaveLayoutSuccess(data));
    } catch (e) {
      if (e instanceof HttpError) {
        dispatch(loadOrSaveLayoutFailure(e.getErrors()));
        return;
      }
      dispatch(
        loadOrSaveLayoutFailure({
          fetch: { message: String(e), identifier: "fetch" },
        }),
      );
    }
  };

export const saveLayout =
  (
    layoutIdentifiers: LayoutIdentifiers,
  ): ThunkAction<
    void,
    RootState,
    unknown,
    PayloadAction | PayloadAction<ErrorSet> | PayloadAction<ReceivedLayout>
  > =>
  async (dispatch, getState) => {
    const state = getState();
    const status = selectLayoutState(state);
    if (status === STATE_SAVING) {
      // Another component is saving this layout.
      return;
    }
    dispatch(saveLayoutStart());
    try {
      const data = await doSaveLayout(layoutIdentifiers, selectLayout(state));
      dispatch(loadOrSaveLayoutSuccess(data));
    } catch (e) {
      if (e instanceof HttpError) {
        dispatch(loadOrSaveLayoutFailure(e.getErrors()));
        return;
      }
      dispatch(
        loadOrSaveLayoutFailure({
          fetch: { message: String(e), identifier: "fetch" },
        }),
      );
    }
  };
