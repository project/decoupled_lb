import { ErrorSet } from "@/types/errorSet.ts";
import {
  LayoutIdentifiers,
  LayoutState,
  ReceivedLayout,
} from "@/types/layout.types.ts";
import { HttpError } from "@/utilities/HttpError.ts";
import { denormalizeLayout } from "@/state/Slices/Layout/utilities.ts";

export const doSaveLayout = (
  { baseUrl, sectionStorage, sectionStorageType }: LayoutIdentifiers,
  layout: LayoutState,
): Promise<ReceivedLayout> =>
  fetch(`${baseUrl}session/token`)
    .then((r) => r.text())
    .then((token) => {
      return fetch(
        `${baseUrl}api/v1/layout-builder/layout/${sectionStorageType}/${sectionStorage}`,
        {
          method: "PUT",
          body: JSON.stringify({ data: denormalizeLayout(layout.layout) }),
          headers: {
            "X-CSRF-Token": token,
          },
        },
      ).then(async (r) => {
        if (r.ok) {
          const json = (await r.json()) as { data: ReceivedLayout };
          return json.data;
        }
        const json = (await r.json()) as { errors: ErrorSet };
        throw new HttpError(r.statusText, json.errors);
      });
    });
