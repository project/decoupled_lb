import { nanoid } from "@reduxjs/toolkit";
import { NormalizedLayout, ReceivedLayout } from "@/types/layout.types.ts";

export const sortComponentsByWeight = (
  layout: NormalizedLayout,
): NormalizedLayout => {
  return {
    ...layout,
    sections: Object.keys(layout.sections)
      .sort((keyA, keyB) => {
        return layout.sections[keyA].weight - layout.sections[keyB].weight;
      })
      .reduce(
        (newSections, key, weight) => ({
          ...newSections,
          [key]: {
            ...layout.sections[key],
            weight,
          },
        }),
        {},
      ),
    components: Object.keys(layout.components)
      .sort((keyA, keyB) => {
        return layout.components[keyA].weight - layout.components[keyB].weight;
      })
      .reduce(
        (newComponents, key, weight) => ({
          ...newComponents,
          [key]: {
            ...layout.components[key],
            weight,
          },
        }),
        {},
      ),
  };
};

export const normalizeLayout = (layout: ReceivedLayout): NormalizedLayout => {
  const normalize: NormalizedLayout = {
    sections: {},
    regions: {},
    components: {},
  };
  layout.sections.forEach((section, weight) => {
    // Remove decoupled_lb_apis from tps.
    const { decoupled_lb_api, ...third_party_settings } =
      section.third_party_settings;
    const sectionId = decoupled_lb_api.uuid;
    normalize.sections[sectionId] = {
      weight,
      id: sectionId,
      ...section,
      third_party_settings,
    };
    const regionMap: Record<string, string> = {};
    Object.keys(section.components).forEach((componentId) => {
      const component = section.components[componentId];
      if (component.region in regionMap) {
        normalize.components[componentId] = {
          ...component,
          regionId: regionMap[component.region],
        };
        return;
      }
      const regionId =
        decoupled_lb_api.region_uuids[component.region] || nanoid();
      regionMap[component.region] = regionId;
      normalize.regions[regionId] = {
        id: regionId,
        region: component.region,
        section: sectionId,
      };
      normalize.components[componentId] = {
        ...component,
        regionId,
      };
    });
  });
  return sortComponentsByWeight(normalize);
};

export const denormalizeLayout = (layout: NormalizedLayout): ReceivedLayout => {
  const sortedSectionIds = Object.keys(layout.sections).sort((keyA, keyB) => {
    return layout.sections[keyA].weight - layout.sections[keyB].weight;
  });
  return {
    sections: sortedSectionIds.map((sectionId) => {
      const regionIds = Object.entries(layout.regions)
        // eslint-disable-next-line no-unused-vars,@typescript-eslint/no-unused-vars
        .filter(([_regionId, region]) => region.section === sectionId)
        .map(([regionId]) => regionId);
      const { layout_id, layout_settings, third_party_settings } =
        layout.sections[sectionId];
      return {
        layout_id,
        layout_settings,
        third_party_settings: {
          ...third_party_settings,
          decoupled_lb_api: {
            uuid: sectionId,
            region_uuids: regionIds.reduce(
              (regionUuidMap, regionId) => ({
                ...regionUuidMap,
                [layout.regions[regionId].region]: regionId,
              }),
              {},
            ),
          },
        },
        components: Object.entries(layout.components)
          // eslint-disable-next-line no-unused-vars,@typescript-eslint/no-unused-vars
          .filter(([_componentId, component]) =>
            regionIds.includes(component.regionId),
          )
          .reduce(
            (components, [componentId, component]) => ({
              ...components,
              [componentId]: {
                uuid: component.uuid,
                configuration: component.configuration,
                weight: component.weight,
                additional: component.additional,
                region: layout.regions[component.regionId].region,
                fallback: component.fallback,
              },
            }),
            {},
          ),
      };
    }),
  };
};
