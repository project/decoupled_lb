import { combineReducers, configureStore } from "@reduxjs/toolkit";
import slices from "./Slices/index.ts";
import {
  useDispatch as reduxUseDispatch,
  useSelector as reduxUseSelector,
  TypedUseSelectorHook,
} from "react-redux";
import { LayoutIdentifiers, NormalizedLayout } from "@/types/layout.types.ts";
import { INITIAL_STATE } from "@/state/Slices/Layout/layoutSlice.ts";
import { drupalApi } from "@/state/Slices/Api";
import { setupListeners } from "@reduxjs/toolkit/query";

const reducers = combineReducers(slices);

export const storeFactory = (
  preloadedState: LayoutIdentifiers,
  defaultLayout: NormalizedLayout | null = null,
) => {
  const store = configureStore({
    reducer: reducers,
    devTools: process.env.NODE_ENV !== "production",
    preloadedState: {
      drupal: { baseUrl: preloadedState.baseUrl },
      layout: {
        ...INITIAL_STATE,
        layout: {
          ...INITIAL_STATE.layout,
          ...(defaultLayout || {}),
        },
        sectionStorageType: preloadedState.sectionStorageType,
        sectionStorage: preloadedState.sectionStorage,
      },
    },
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(drupalApi.middleware),
  });
  setupListeners(store.dispatch);
  return store;
};

// see `setupListeners` docs - takes an optional callback as the 2nd arg for customization
export type Store = ReturnType<typeof storeFactory>;
export type RootState = ReturnType<Store["getState"]>;
export type LayoutBuilderDispatch = Store["dispatch"];

export type DispatchFunction = () => LayoutBuilderDispatch;
export const useDispatch: DispatchFunction = reduxUseDispatch;
export const useSelector: TypedUseSelectorHook<RootState> = reduxUseSelector;
