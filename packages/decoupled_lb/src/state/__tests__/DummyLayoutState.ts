import { NormalizedLayout, ReceivedLayout } from "@/types/layout.types.ts";

export const dummyLoadedLayout: ReceivedLayout = {
  sections: [
    {
      layout_settings: {
        label: "s1",
      },
      layout_id: "l1",
      third_party_settings: {
        decoupled_lb_api: {
          uuid: "s1",
          region_uuids: {
            content: "r1",
          },
        },
      },
      components: {
        c1: {
          weight: 0,
          configuration: {
            id: "fallback",
            label_display: true,
            label: "c1",
          },
          uuid: "c1",
          additional: {},
          region: "content",
          fallback: "the c1 block",
        },
      },
    },
    {
      layout_settings: {},
      layout_id: "l1",
      third_party_settings: {
        decoupled_lb_api: {
          uuid: "s2",
          region_uuids: {
            col1: "r2",
            col2: "r3",
          },
        },
      },
      components: {
        c2: {
          weight: 1,
          configuration: {
            id: "fallback",
            label_display: false,
            label: "c2",
          },
          uuid: "c2",
          additional: {},
          region: "col1",
          fallback: "the c2 block",
        },
        c3: {
          weight: 2,
          configuration: {
            id: "fallback",
            label_display: false,
            label: "c3",
          },
          uuid: "c3",
          additional: {},
          region: "col2",
          fallback: "the c3 block",
        },
        c4: {
          weight: 3,
          configuration: {
            id: "fallback",
            label_display: false,
            label: "c4",
          },
          uuid: "c4",
          additional: {},
          region: "col2",
          fallback: "the c4 block",
        },
      },
    },
  ],
};

export const dummyLayoutState: NormalizedLayout = {
  sections: {
    s1: {
      id: "s1",
      layout_settings: {
        label: "s1",
      },
      layout_id: "l1",
      weight: 0,
    },
    s2: {
      id: "s2",
      layout_settings: {},
      layout_id: "l1",
      weight: 1,
    },
  },
  regions: {
    r1: {
      region: "content",
      section: "s1",
      id: "r1",
    },
    r2: {
      region: "col1",
      section: "s2",
      id: "r2",
    },
    r3: {
      region: "col2",
      section: "s2",
      id: "r3",
    },
  },
  components: {
    c1: {
      regionId: "r1",
      weight: 0,
      configuration: {
        id: "fallback",
        label_display: true,
        label: "c1",
      },
      uuid: "c1",
      additional: {},
      fallback: "the c1 block",
    },
    c2: {
      regionId: "r2",
      weight: 1,
      configuration: {
        id: "fallback",
        label_display: false,
        label: "c2",
      },
      uuid: "c2",
      additional: {},
      fallback: "the c2 block",
    },
    c3: {
      regionId: "r3",
      weight: 2,
      configuration: {
        id: "fallback",
        label_display: false,
        label: "c3",
      },
      uuid: "c3",
      additional: {},
      fallback: "the c3 block",
    },
    c4: {
      regionId: "r3",
      weight: 3,
      configuration: {
        id: "fallback",
        label_display: false,
        label: "c4",
      },
      uuid: "c4",
      additional: {},
      fallback: "the c4 block",
    },
  },
};
