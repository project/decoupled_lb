import { storeFactory } from "@/state/store.ts";
import { Provider } from "react-redux";

import React from "react";
import { NormalizedLayout } from "@/types/layout.types.ts";

export interface StoreDecoratorProps {
  /**
   * Children
   */
  children: React.ReactNode;
  /**
   * Default layout.
   */
  defaultLayout?: NormalizedLayout;
}

/**
 * A decorator for rendering store-reliant components in storybook.
 */
const StoreDecorator: React.FC<StoreDecoratorProps> = ({
  children,
  defaultLayout,
}) => {
  return (
    <Provider
      store={storeFactory(
        {
          baseUrl: "/",
          sectionStorageType: "overrides",
          sectionStorage: "node.5",
        },
        defaultLayout,
      )}
    >
      {children}
    </Provider>
  );
};

export default StoreDecorator;
