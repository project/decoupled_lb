import { BlockPlugin } from "../types/block.types.ts";

const handlers = import.meta.glob<BlockPlugin>(
  "/src/components/BlockPlugins/*.tsx",
);
const pluginIds = import.meta.glob<string>(
  "/src/components/BlockPlugins/*.tsx",
  {
    import: "id",
    eager: true,
  },
);

const blockPlugins = Object.entries(pluginIds).reduce(
  (carry: Record<string, () => Promise<BlockPlugin>>, [key, value]) => ({
    ...carry,
    [value]: handlers[key],
  }),
  {},
);

export default blockPlugins;
