import { FieldValues } from "../types/field.types.ts";
import { WidgetPlugin } from "@/types/widget.types.ts";

const handlers = import.meta.glob<WidgetPlugin<FieldValues>>(
  "/src/components/Widgets/*.tsx",
);
const pluginIds = import.meta.glob<string>("/src/components/Widgets/*.tsx", {
  import: "id",
  eager: true,
});

// For local development we just load all the defined plugins
const WidgetPlugins = Object.entries(pluginIds).reduce(
  (
    carry: Record<string, () => Promise<WidgetPlugin<FieldValues>>>,
    [key, value],
  ) => ({
    ...carry,
    [value]: handlers[key],
  }),
  {},
);

export default WidgetPlugins;
