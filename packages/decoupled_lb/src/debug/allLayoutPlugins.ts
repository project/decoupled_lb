import { LayoutPlugin } from "../types/layout.types.ts";

const handlers = import.meta.glob<LayoutPlugin>(
  "/src/components/Layouts/*.tsx",
);
const pluginIds = import.meta.glob<string>("/src/components/Layouts/*.tsx", {
  import: "id",
  eager: true,
});

// For local development we just load all the defined plugins
const layoutPlugins = Object.entries(pluginIds).reduce(
  (carry: Record<string, () => Promise<LayoutPlugin>>, [key, value]) => ({
    ...carry,
    [value]: handlers[key],
  }),
  {},
);

export default layoutPlugins;
