import { FieldValues } from "../types/field.types.ts";
import { FormatterPlugin } from "../types/formatter.types.ts";

const handlers = import.meta.glob<FormatterPlugin<FieldValues>>(
  "/src/components/Formatters/*.tsx",
);
const pluginIds = import.meta.glob<string>("/src/components/Formatters/*.tsx", {
  import: "id",
  eager: true,
});

// For local development we just load all the defined plugins
const FormatterPlugins = Object.entries(pluginIds).reduce(
  (
    carry: Record<string, () => Promise<FormatterPlugin<FieldValues>>>,
    [key, value],
  ) => ({
    ...carry,
    [value]: handlers[key],
  }),
  {},
);

export default FormatterPlugins;
