import LayoutEditor from "@/components/LayoutEditor/LayoutEditor.tsx";
import Formatter from "@/components/LayoutEditor/Formatter.tsx";
import Region from "@/components/LayoutEditor/Region.tsx";
import Block from "@/components/LayoutEditor/Block.tsx";
import Widget from "@/components/LayoutEditor/Widget.tsx";
import DefaultLayoutSettings from "@/components/LayoutEditor/DefaultLayoutSettings.tsx";
import DefaultBlockSettings from "@/components/LayoutEditor/DefaultBlockSettings.tsx";
import Display from "@/utilities/Displays/Display.ts";
export {
  LayoutEditor,
  Formatter,
  Block,
  Region,
  Display,
  Widget,
  DefaultBlockSettings,
  DefaultLayoutSettings,
};
export { decodeError } from "@/utilities/ErrorHelpers.ts";
export { useLayoutPlugin } from "@/hooks/useLayoutPlugin.tsx";
export { useWidgetPlugin } from "@/hooks/useWidgetPlugin.tsx";
export { useFormatterPlugin } from "@/hooks/useFormatterPlugin.tsx";
export { useBlockPlugin } from "@/hooks/useBlockPlugin.tsx";
export { storeFactory, useDispatch, useSelector } from "@/state/store.ts";
export {
  default as drupalSlice,
  selectBaseUrl,
} from "@/state/Slices/Drupal/drupalSlice.ts";
export {
  default as layoutSlice,
  selectLayout,
  selectLayoutSections,
  selectRegions,
  loadLayoutStart,
  loadOrSaveLayoutSuccess,
  loadOrSaveLayoutFailure,
  reorderLayoutSections,
  moveBlock,
  addNewSection,
  saveLayout,
  saveLayoutStart,
  selectLayoutState,
  addNewBlock,
  addNewBlockAndSection,
  loadLayout,
  updateLayoutSettings,
  selectComponents,
  selectRegion,
  updateBlock,
} from "@/state/Slices/Layout/layoutSlice.ts";
export {
  normalizeLayout,
  denormalizeLayout,
  sortComponentsByWeight,
} from "@/state/Slices/Layout/utilities.ts";
export {
  default as uiSlice,
  selectActiveDrag,
  selectSelectedSection,
  setSelectedSection,
  selectSelectedComponent,
  setActiveDrag,
  setSelectedComponent,
  setActiveSidebarEndPane,
  setActiveSidebarStartPane,
  setStartSidebarOpen,
  setEndSidebarOpen,
  selectMode,
  setMode,
  selectorUiState,
  setUnsavedChanges,
} from "@/state/Slices/Ui/uiSlice.ts";
export { default as Field } from "@/components/Theme/Field.tsx";
export { default as Editor } from "@/components/CKEditor/Editor.tsx";
import * as BlockTitleComponent from "@/components/Ui/BlockTitle.tsx";
export const BlockTitle = BlockTitleComponent;

export type {
  LayoutSettings,
  LayoutPlugin,
  AsyncLayoutPlugin,
  LayoutIdentifiers,
  BaseSection,
  ReceivedSection,
  ReceivedLayout,
  LayoutRegion,
  NormalizedLayout,
  LayoutState,
  LayoutSection,
  LayoutPluginProps,
} from "./types/layout.types.ts";
export type {
  BlockConfiguration,
  BaseBlock,
  BlockPlugin,
  BlockInfo,
  BlockList,
  LayoutBlock,
  SectionInfo,
  SectionList,
  AsyncBlockPlugin,
  Restriction,
} from "./types/block.types.ts";
export type { FieldValues } from "./types/field.types.ts";
export {
  useGetDisplayPluginQuery,
  useGetBlockListQuery,
  useGetSectionListQuery,
  selectKeyedSections,
  selectKeyedBlocks,
  selectSectionsResult,
  selectBlocksResult,
  selectAllBlocks,
  selectAllSections,
  selectBlockById,
  selectSectionById,
} from "@/state/Slices/Api";
export type {
  FormatterPlugin,
  FormatterProps,
  AsyncFormatterPlugin,
} from "./types/formatter.types.ts";
export type {
  WidgetPlugin,
  WidgetProps,
  AsyncWidgetPlugin,
} from "./types/widget.types.ts";
export type {
  STATE_LOADING,
  STATE_CLEAN,
  STATE_DIRTY,
  STATE_IDLE,
  STATE_SAVING,
  STATE_ERROR,
  LoadingState,
  Mapping,
} from "./types/constants.ts";
export type { ErrorSet, Error } from "./types/errorSet.ts";
export type {
  ActiveDragOutlineSection,
  ActiveDragOutlineBlock,
  ActiveDragInsertSection,
  ActiveDragInsertBlock,
  ActiveDrag,
  ActiveDragSection,
  ActiveDragBlock,
  DRAG_INSERT_BLOCK,
  DRAG_SECTION,
  DRAG_BLOCK,
  DRAG_INSERT_SECTION,
  DRAG_OUTLINE_BLOCK,
  DRAG_OUTLINE_SECTION,
  Mode,
  MODE_SELECT,
  MODE_EDIT,
  SIDEBAR_TAB_INSERT,
  SIDEBAR_TAB_OUTLINE,
  StartSidebarTab,
  EndSidebarTab,
  UiState,
  SIDEBAR_TAB_SETTINGS,
  SIDEBAR_TAB_ENTITY,
} from "./types/ui.types.ts";
export type {
  DisplayConfiguration,
  DisplaySettings,
  DisplayIdentifiers,
  DisplayType,
  DisplayData,
  DisplayInterface,
  DisplayResponse,
  TypedDisplayIdentifiers,
} from "./types/displayMode.types.ts";
export type { DrupalState } from "./types/drupal.types.ts";
