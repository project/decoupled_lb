import type { ReactRenderer } from "@storybook/react";
import { Meta, StoryObj } from "@storybook/react";
import Block from "./Block.tsx";
import StoreDecorator from "@/state/__tests__/StoreDecorator.tsx";
import { blockPlugins } from "@/components/LayoutEditor/__tests__/mockPluginProviders.ts";
import DragContext from "@/components/LayoutEditor/DragContext.tsx";
import DroppableStrictMode from "@/components/DragAndDrop/DroppableStrictMode.tsx";
import BlockPluginProvider from "@/components/LayoutEditor/BlockPluginProvider.tsx";

export type _ReactRenderer = ReactRenderer;

const meta = {
  title: "Components/LayoutEditor/Block",
  component: Block,
  parameters: {
    layout: "centered",
  },
  decorators: [
    (story) => (
      <StoreDecorator>
        <BlockPluginProvider plugins={blockPlugins}>
          <DragContext>
            <DroppableStrictMode droppableId={"123456"}>
              {(provided) => (
                <div {...provided.droppableProps} ref={provided.innerRef}>
                  {story()}
                </div>
              )}
            </DroppableStrictMode>
          </DragContext>
        </BlockPluginProvider>
      </StoreDecorator>
    ),
  ],
} satisfies Meta<typeof Block>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    regionId: "123456",
    uuid: "b",
    weight: 0,
    index: 0,
    additional: {},
    configuration: {
      id: "fallback",
      label_display: true,
      label: "A block",
    },
  },
};
