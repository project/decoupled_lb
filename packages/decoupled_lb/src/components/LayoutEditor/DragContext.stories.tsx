import { Meta, StoryObj } from "@storybook/react";
import DragContext from "./DragContext.tsx";
import StoreDecorator from "@/state/__tests__/StoreDecorator.tsx";

import type { ReactRenderer } from "@storybook/react";
export type _ReactRenderer = ReactRenderer;

const meta = {
  title: "Components/LayoutEditor/DragContext",
  component: DragContext,
  parameters: {
    layout: "centered",
  },
  decorators: [(story) => <StoreDecorator>{story()}</StoreDecorator>],
} satisfies Meta<typeof DragContext>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    children: <div>Draggable and Droppable components can be added here</div>,
  },
};
