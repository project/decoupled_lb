import React from "react";
import { t } from "@/utilities/Translation.ts";
import { useDispatch } from "@/state/store.ts";
import { updateLayoutSettings } from "@/state/Slices/Layout/layoutSlice.ts";
import { LayoutSection } from "@/types/layout.types.ts";

/**
 * Provides a default layout settings pane.
 */
const DefaultLayoutSettings: React.FC<LayoutSection> = (section) => {
  const { id, layout_settings } = section;
  const labelId = `label-${id}`;
  const dispatch = useDispatch();
  const setLabel = (e: { currentTarget: { value: string } }) => {
    dispatch(
      updateLayoutSettings({
        sectionId: id,
        layoutSettings: { label: e.currentTarget.value },
      }),
    );
  };
  return (
    <>
      <div className={"form-item"}>
        <label htmlFor={labelId}>{t("Label")}</label>
        <input
          onChange={setLabel}
          type={"text"}
          value={layout_settings.label || ""}
          id={labelId}
        />
      </div>
    </>
  );
};

export default DefaultLayoutSettings;
