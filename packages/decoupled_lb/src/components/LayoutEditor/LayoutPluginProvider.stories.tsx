import { Meta, StoryObj } from "@storybook/react";
import LayoutPluginProvider from "./LayoutPluginProvider.tsx";

const meta = {
  title: "Components/LayoutEditor/LayoutPluginProvider",
  component: LayoutPluginProvider,
  parameters: {
    layout: "centered",
  },
} satisfies Meta<typeof LayoutPluginProvider>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    children: (
      <div>
        Children here can use <code>useLayoutPlugin('plugin_id')</code>
      </div>
    ),
    plugins: {},
  },
};
