import { Meta, StoryObj } from "@storybook/react";
import WidgetPluginProvider from "./WidgetPluginProvider.tsx";

const meta = {
  title: "Components/LayoutEditor/WidgetPluginProvider",
  component: WidgetPluginProvider,
  parameters: {
    layout: "centered",
  },
} satisfies Meta<typeof WidgetPluginProvider>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    children: (
      <div>
        Children here can use <code>useWidgetPlugin('plugin_id')</code>
      </div>
    ),
    plugins: {},
  },
};
