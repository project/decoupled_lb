import { render, waitFor, screen } from "@testing-library/react";
import DragContext from "@/components/LayoutEditor/DragContext.tsx";
import { Provider } from "react-redux";
import { storeFactory } from "@/state/store.ts";
import { dummyLayoutState } from "@/state/__tests__/DummyLayoutState.ts";
import Region from "@/components/LayoutEditor/Region.tsx";

describe("Region", () => {
  it("Should render", async () => {
    const store = storeFactory(
      {
        baseUrl: "/",
        sectionStorageType: "overrides",
        sectionStorage: "node.5",
      },
      dummyLayoutState,
    );
    const { container } = render(
      <Provider store={store}>
        <DragContext>
          <Region regionId={"r1"} />
        </DragContext>
      </Provider>,
    );
    await waitFor(() => {
      expect(
        screen.getByText("The c1 block", { exact: false }),
      ).toBeInTheDocument();
    });
    expect(container).toMatchSnapshot();
  });
});
