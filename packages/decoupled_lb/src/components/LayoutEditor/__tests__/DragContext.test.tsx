import { render, screen, waitFor } from "@testing-library/react";
import DragContext, {
  DROPPABLE_ID,
} from "@/components/LayoutEditor/DragContext.tsx";
import DroppableStrictMode from "@/components/DragAndDrop/DroppableStrictMode.tsx";
import Section from "@/components/LayoutEditor/Section.tsx";
import { storeFactory } from "@/state/store.ts";
import { Provider } from "react-redux";
import { dummyLayoutState } from "@/state/__tests__/DummyLayoutState.ts";

jest.useFakeTimers();
export const setup = () => {
  const store = storeFactory(
    {
      baseUrl: "/",
      sectionStorageType: "overrides",
      sectionStorage: "node.5",
    },
    dummyLayoutState,
  );
  render(
    <Provider store={store}>
      <DragContext>
        <DroppableStrictMode droppableId={DROPPABLE_ID}>
          {(provided) => (
            <div {...provided.droppableProps} ref={provided.innerRef}>
              <Section
                index={0}
                id={"s1"}
                layout_id={"fallback"}
                layout_settings={{}}
                weight={0}
              />
              <Section
                index={1}
                id={"s2"}
                layout_id={"fallback"}
                layout_settings={{}}
                weight={1}
              />
              {provided.placeholder}
            </div>
          )}
        </DroppableStrictMode>
      </DragContext>
    </Provider>,
  );
  return store;
};

describe("Drag context", () => {
  it("Should render", async () => {
    setup();
    await waitFor(() => {
      expect(
        screen.getByText(
          "The section with weight 0 is using the layout plugin fallback",
          { exact: false },
        ),
      ).toBeInTheDocument();
    });
  });
});
