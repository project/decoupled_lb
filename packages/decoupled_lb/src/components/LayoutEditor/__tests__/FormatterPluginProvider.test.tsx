import { render, screen, waitFor } from "@testing-library/react";
import FormatterPluginProvider from "@/components/LayoutEditor/FormatterPluginProvider.tsx";
import { useFormatterPlugin } from "@/hooks/useFormatterPlugin";
import { FormatterPlugin } from "@/types/formatter.types.ts";
import { FieldValues } from "@/types/field.types.ts";
import * as Fallback from "@/components/Formatters/Fallback.tsx";

const MockChild: React.FC = () => {
  const Plugin = useFormatterPlugin("plugin_id");
  if (!Plugin) {
    return <div>...loading</div>;
  }
  const mockFormatter = {
    type: "plugin_id",
    weight: 0,
    region: "content" as const,
    third_party_settings: {},
    settings: {},
  };
  return (
    <Plugin.Formatter
      key={0}
      formatter={mockFormatter}
      itemValues={[]}
      fieldName={"some_field"}
    />
  );
};

const plugins = {
  plugin_id: () =>
    new Promise<FormatterPlugin<FieldValues>>((resolve) => resolve(Fallback)),
};
describe("Formatter plugin provider", () => {
  it("Should resolve to a Formatter plugin", async () => {
    const { container } = render(
      <FormatterPluginProvider plugins={plugins}>
        <MockChild />
      </FormatterPluginProvider>,
    );
    expect(container).toMatchSnapshot();
    await waitFor(() => {
      expect(
        screen.getByText(
          `The some_field field is using the plugin_id formatter that doesn't have`,
          { exact: false },
        ),
      ).toBeInTheDocument();
    });
  });
});
