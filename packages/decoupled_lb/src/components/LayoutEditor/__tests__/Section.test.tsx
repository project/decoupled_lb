import { render, screen, waitFor } from "@testing-library/react";
import DragContext from "@/components/LayoutEditor/DragContext.tsx";
import { Provider } from "react-redux";
import { storeFactory } from "@/state/store.ts";
import {
  dummyLayoutState,
  dummyLoadedLayout,
} from "@/state/__tests__/DummyLayoutState.ts";
import LayoutPluginProvider from "@/components/LayoutEditor/LayoutPluginProvider.tsx";
import { layoutPlugins } from "@/components/LayoutEditor/__tests__/mockPluginProviders.ts";
import Section from "@/components/LayoutEditor/Section.tsx";
import Layout from "@/components/LayoutEditor/Layout.tsx";
import { loadOrSaveLayoutSuccess } from "@/state/Slices/Layout/layoutSlice.ts";
import { userEvent } from "@testing-library/user-event";

const user = userEvent.setup();

describe("Section", () => {
  it("Should render and support selection", async () => {
    const identifiers = {
      baseUrl: "/",
      sectionStorageType: "overrides",
      sectionStorage: "node.5",
    };
    const store = storeFactory(identifiers, dummyLayoutState);
    store.dispatch(loadOrSaveLayoutSuccess(dummyLoadedLayout));
    render(
      <Provider store={store}>
        <LayoutPluginProvider plugins={layoutPlugins}>
          <DragContext>
            <Layout {...identifiers}>
              <Section
                index={0}
                {...store.getState().layout.layout.sections.s1}
              />
            </Layout>
          </DragContext>
        </LayoutPluginProvider>
      </Provider>,
    );
    const block = await waitFor(() => {
      const el = screen.getByText("The c1 block", { exact: false });
      expect(el).toBeInTheDocument();
      return el;
    });
    const section = await waitFor(() => {
      const el = screen.getByText("The section with weight 0", {
        exact: false,
      });
      expect(el).toBeInTheDocument();
      return el;
    });
    await user.click(section);
    const components = store.getState().layout.layout.components;
    const regions = store.getState().layout.layout.regions;
    const sectionOneId = regions[components["c1"].regionId].section;
    expect(store.getState().ui.selectedSection).toEqual(sectionOneId);
    const section2 = await waitFor(() => {
      const el = screen.getByText("The section with weight 1", {
        exact: false,
      });
      expect(el).toBeInTheDocument();
      return el;
    });
    await user.click(section2);
    expect(store.getState().ui.selectedSection).not.toEqual(sectionOneId);
    await user.click(block);
    expect(store.getState().ui.selectedSection).toEqual(sectionOneId);
  });
});
