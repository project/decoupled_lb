import { render, screen, waitFor } from "@testing-library/react";
import LayoutEditor, {
  LayoutEditorProps,
} from "@/components/LayoutEditor/LayoutEditor.tsx";
import { userEvent } from "@testing-library/user-event";
import {
  blockPlugins,
  formatterPlugins,
  layoutPlugins,
  widgetPlugins,
} from "@/components/LayoutEditor/__tests__/mockPluginProviders.ts";

const setup = (args: Partial<LayoutEditorProps> = {}) => {
  const { container } = render(
    <LayoutEditor
      {...{
        title: "Sample layout builder",
        sectionStorage: "node.5",
        sectionStorageType: "overrides",
        blockPlugins,
        layoutPlugins,
        formatterPlugins,
        widgetPlugins,
        baseUrl: "/",
        ...args,
      }}
    />,
  );
  return container;
};

const user = userEvent.setup();

describe("Layout editor", () => {
  it("Should render", () => {
    const container = setup();
    expect(container).toMatchSnapshot();
    expect(screen.getByText("Sample layout builder")).toBeInTheDocument();
  });
  it("Should open sidebar", async () => {
    setup();
    expect(screen.getByText("Sample layout builder")).toBeInTheDocument();
    const openSidebarFirst = await waitFor(() => {
      const el = screen.getByRole("button", { name: "➕" });
      expect(el).toBeInTheDocument();
      return el;
    });
    await user.click(openSidebarFirst);
    await waitFor(() => {
      expect(
        screen.getByRole("heading", { name: "Content block" }),
      ).toBeInTheDocument();
    });
  });
});
