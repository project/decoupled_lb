import { render, screen, waitFor } from "@testing-library/react";
import WidgetPluginProvider from "@/components/LayoutEditor/WidgetPluginProvider.tsx";
import { useWidgetPlugin } from "@/hooks/useWidgetPlugin";
import { WidgetPlugin } from "@/types/widget.types.ts";
import { FieldValues } from "@/types/field.types.ts";
import * as Fallback from "@/components/Widgets/Fallback.tsx";

const MockChild: React.FC = () => {
  const Plugin = useWidgetPlugin("plugin_id");
  if (!Plugin) {
    return <div>...loading</div>;
  }
  const mockWidget = {
    type: "plugin_id",
    weight: 0,
    region: "content" as const,
    third_party_settings: {},
    settings: {},
  };
  return (
    <Plugin.Widget
      key={0}
      widget={mockWidget}
      update={() => {}}
      itemValues={[]}
      fieldName={"some_field"}
    />
  );
};

const plugins = {
  plugin_id: () =>
    new Promise<WidgetPlugin<FieldValues>>((resolve) => resolve(Fallback)),
};
describe("Widget plugin provider", () => {
  it("Should resolve to a widget plugin", async () => {
    const { container } = render(
      <WidgetPluginProvider plugins={plugins}>
        <MockChild />
      </WidgetPluginProvider>,
    );
    expect(container).toMatchSnapshot();
    await waitFor(() => {
      expect(
        screen.getByText(
          `The some_field field is using the plugin_id widget that doesn't have`,
          { exact: false },
        ),
      ).toBeInTheDocument();
    });
  });
});
