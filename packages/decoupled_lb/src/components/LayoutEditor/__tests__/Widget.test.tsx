import WidgetPluginProvider from "@/components/LayoutEditor/WidgetPluginProvider.tsx";
import { widgetPlugins } from "@/components/LayoutEditor/__tests__/mockPluginProviders.ts";
import Widget from "@/components/LayoutEditor/Widget.tsx";
import { render, screen, waitFor } from "@testing-library/react";

const defaultArgs = {
  itemValues: [
    { value: "Hi this is a field value from the string_textfield widget" },
  ],
  widget: {
    type: "string_textfield",
    settings: {},
    third_party_settings: {},
    region: "content" as const,
    weight: 0,
  },
  fieldName: "field_field",
  update: () => {},
};
describe("Widget", () => {
  it("Should render", async () => {
    const { container } = render(
      <WidgetPluginProvider plugins={widgetPlugins}>
        <Widget {...defaultArgs} />
      </WidgetPluginProvider>,
    );
    expect(container).toMatchSnapshot();
    await waitFor(() => {
      expect(
        screen.getByText(
          "Hi this is a field value from the string_textfield widget",
        ),
      ).toBeInTheDocument();
    });
  });
});
