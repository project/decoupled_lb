import BlockPluginProvider from "@/components/LayoutEditor/BlockPluginProvider.tsx";
import { blockPlugins } from "@/components/LayoutEditor/__tests__/mockPluginProviders.ts";
import DragContext from "@/components/LayoutEditor/DragContext.tsx";
import DroppableStrictMode from "@/components/DragAndDrop/DroppableStrictMode.tsx";
import { render, screen, waitFor } from "@testing-library/react";
import Block from "@/components/LayoutEditor/Block.tsx";
import { storeFactory } from "@/state/store.ts";
import { Provider } from "react-redux";
import { userEvent } from "@testing-library/user-event";

const user = userEvent.setup();

describe("Block", () => {
  it("Should render and allow setting active block", async () => {
    const store = storeFactory({
      baseUrl: "/",
      sectionStorageType: "overrides",
      sectionStorage: "node.5",
    });
    const { container } = render(
      <Provider store={store}>
        <BlockPluginProvider plugins={blockPlugins}>
          <DragContext>
            <DroppableStrictMode droppableId={"123456"}>
              {(provided) => (
                <div {...provided.droppableProps} ref={provided.innerRef}>
                  <Block
                    {...{
                      regionId: "123456",
                      uuid: "this-is-the-uuid",
                      weight: 0,
                      index: 0,
                      additional: {},
                      fallback: "The A block block with plugin ID the fallback",
                      configuration: {
                        id: "fallback",
                        label_display: true,
                        label: "A block",
                      },
                    }}
                  />
                </div>
              )}
            </DroppableStrictMode>
          </DragContext>
        </BlockPluginProvider>
      </Provider>,
    );
    const section = await waitFor(() => {
      const el = screen.getByText(
        "The A block block with plugin ID the fallback",
        {
          exact: false,
        },
      );
      expect(el);
      return el;
    });
    expect(container).toMatchSnapshot();
    await user.click(section);
    await waitFor(() => {
      expect(store.getState().ui.selectedComponent).toEqual("this-is-the-uuid");
    });
  });
});
