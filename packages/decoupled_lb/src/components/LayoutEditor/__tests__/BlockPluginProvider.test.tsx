import { render, screen, waitFor } from "@testing-library/react";
import BlockPluginProvider from "@/components/LayoutEditor/BlockPluginProvider.tsx";
import { useBlockPlugin } from "@/hooks/useBlockPlugin";
import { BlockPlugin } from "@/types/block.types.ts";
import * as Fallback from "@/components/BlockPlugins/Fallback.tsx";

const MockChild: React.FC = () => {
  const Plugin = useBlockPlugin("plugin_id");
  if (!Plugin) {
    return <div>...loading</div>;
  }
  return (
    <Plugin.Edit
      uuid={"d80aa44a-5483-439f-8dc6-5a82de92d1cd"}
      configuration={{
        id: "plugin_id",
        label: "Label",
        label_display: true,
      }}
      fallback={"The label block with plugin ID the plugin_id"}
      regionId={"172508d4-6495-49a9-85c0-d5283f63097c"}
      weight={0}
      additional={{}}
    />
  );
};

const plugins = {
  plugin_id: () => new Promise<BlockPlugin>((resolve) => resolve(Fallback)),
};
describe("Block plugin provider", () => {
  it("Should resolve to a Block plugin", async () => {
    const { container } = render(
      <BlockPluginProvider plugins={plugins}>
        <MockChild />
      </BlockPluginProvider>,
    );
    expect(container).toMatchSnapshot();
    await waitFor(() => {
      expect(
        screen.getByText(`The Label block with plugin ID the plugin_id`, {
          exact: false,
        }),
      ).toBeInTheDocument();
    });
  });
});
