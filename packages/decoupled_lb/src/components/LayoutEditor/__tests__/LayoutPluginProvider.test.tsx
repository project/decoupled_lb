import { render, screen, waitFor } from "@testing-library/react";
import LayoutPluginProvider from "@/components/LayoutEditor/LayoutPluginProvider.tsx";
import { useLayoutPlugin } from "@/hooks/useLayoutPlugin";
import { LayoutPlugin } from "@/types/layout.types.ts";
import * as Fallback from "@/components/Layouts/Fallback.tsx";
import React from "react";

const MockChild: React.FC = () => {
  const Plugin = useLayoutPlugin("plugin_id");
  if (!Plugin) {
    return <div>...loading</div>;
  }
  return (
    <Plugin.Preview
      regions={{}}
      section={{
        weight: 0,
        id: "d80aa44a-5483-439f-8dc6-5a82de92d1cd",
        layout_id: "layout_twocol",
        layout_settings: {},
      }}
      sectionProps={{
        ref: () => {},
      }}
    />
  );
};

const plugins = {
  plugin_id: () => new Promise<LayoutPlugin>((resolve) => resolve(Fallback)),
};
describe("Layout plugin provider", () => {
  it("Should resolve to a Layout plugin", async () => {
    const { container } = render(
      <LayoutPluginProvider plugins={plugins}>
        <MockChild />
      </LayoutPluginProvider>,
    );
    expect(container).toMatchSnapshot();
    await waitFor(() => {
      expect(
        screen.getByText(
          `The section with weight 0 is using the layout plugin`,
          { exact: false },
        ),
      ).toBeInTheDocument();
    });
  });
});
