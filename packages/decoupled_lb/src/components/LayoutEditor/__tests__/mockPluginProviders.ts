import { BlockPlugin } from "@/types/block.types.ts";
import { LayoutPlugin } from "@/types/layout.types.ts";
import { FormatterPlugin } from "@/types/formatter.types.ts";
import { FieldValues } from "@/types/field.types.ts";
import { WidgetPlugin } from "@/types/widget.types.ts";

/**
 * Can't use import.meta.glob in jest tests.
 */
export const blockPlugins: Record<string, () => Promise<BlockPlugin>> = {
  inline_block: () => import("../../BlockPlugins/InlineBlock.tsx"),
};

export const layoutPlugins: Record<string, () => Promise<LayoutPlugin>> = {
  layout_onecol: () => import("../../Layouts/OneColumn.tsx"),
  layout_twocol_section: () => import("../../Layouts/TwoColumn.tsx"),
};

export const formatterPlugins: Record<
  string,
  () => Promise<FormatterPlugin<FieldValues>>
> = {
  string: () => import("../../Formatters/String.tsx"),
  text_default: () => import("../../Formatters/TextDefault.tsx"),
};

export const widgetPlugins = {
  string_textfield: () => import("../../Widgets/StringTextField.tsx"),
  text_textarea: () => import("../../Widgets/TextAreaWidget.tsx"),
} as unknown as Record<string, () => Promise<WidgetPlugin<FieldValues>>>;
