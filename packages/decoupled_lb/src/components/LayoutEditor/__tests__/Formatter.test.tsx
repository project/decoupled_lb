import FormatterPluginProvider from "@/components/LayoutEditor/FormatterPluginProvider.tsx";
import { formatterPlugins } from "@/components/LayoutEditor/__tests__/mockPluginProviders.ts";
import Formatter from "@/components/LayoutEditor/Formatter.tsx";
import { Default } from "../Formatter.stories.tsx";
import { render, screen, waitFor } from "@testing-library/react";

describe("Formatter", () => {
  it("Should render", async () => {
    const { container } = render(
      <FormatterPluginProvider plugins={formatterPlugins}>
        <Formatter {...Default.args} />
      </FormatterPluginProvider>,
    );
    expect(container).toMatchSnapshot();
    await waitFor(() => {
      expect(
        screen.getByText("Hi this is a field value from the string formatter"),
      ).toBeInTheDocument();
    });
  });
});
