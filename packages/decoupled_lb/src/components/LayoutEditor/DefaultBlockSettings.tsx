import React from "react";
import { LayoutBlock } from "@/types/block.types.ts";
import { t } from "@/utilities/Translation.ts";
import { useDispatch } from "@/state/store.ts";
import { updateBlock } from "@/state/Slices/Layout/layoutSlice.ts";

/**
 * Provides a default block settings pane.
 */
const DefaultBlockSettings: React.FC<LayoutBlock> = (block) => {
  const { configuration, uuid } = block;
  const labelDisplayId = `label-display-${uuid}`;
  const labelId = `label-${uuid}`;
  const dispatch = useDispatch();
  const setLabelDisplay = (e: { currentTarget: { checked: boolean } }) => {
    dispatch(
      updateBlock({
        componentId: uuid,
        values: {
          ...block,
          configuration: {
            ...configuration,
            label_display: e.currentTarget.checked,
          },
        },
      }),
    );
  };
  const setLabel = (e: { currentTarget: { value: string } }) => {
    dispatch(
      updateBlock({
        componentId: uuid,
        values: {
          ...block,
          configuration: {
            ...configuration,
            label: e.currentTarget.value,
          },
        },
      }),
    );
  };
  return (
    <>
      <div className={"form-item"}>
        <label htmlFor={labelId}>{t("Label")}</label>
        <input
          onChange={setLabel}
          type={"text"}
          value={configuration.label || ""}
          id={labelId}
        />
      </div>
      <div className={"form-item"}>
        <label htmlFor={labelDisplayId}>
          <input
            onChange={setLabelDisplay}
            type={"checkbox"}
            id={labelDisplayId}
            checked={configuration.label_display}
          />
          {t("Display label")}
        </label>
      </div>
    </>
  );
};

export default DefaultBlockSettings;
