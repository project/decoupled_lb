import React from "react";
import { Provider } from "react-redux";
import { storeFactory } from "@/state/store.ts";
import BlockPluginProvider from "./BlockPluginProvider.tsx";
import Layout from "./Layout.tsx";
import LayoutPluginProvider from "./LayoutPluginProvider.tsx";
import FormatterPluginProvider from "./FormatterPluginProvider.tsx";
import { BlockPlugin } from "@/types/block.types.ts";
import { LayoutIdentifiers, LayoutPlugin } from "@/types/layout.types.ts";
import { FieldValues } from "@/types/field.types.ts";
import { FormatterPlugin } from "@/types/formatter.types.ts";
import { WidgetPlugin } from "@/types/widget.types.ts";
import WidgetPluginProvider from "@/components/LayoutEditor/WidgetPluginProvider.tsx";
import TopBar from "@/components/Ui/TopBar/TopBar.tsx";
import SidebarStart from "@/components/Ui/Sidebar/SidebarStart.tsx";
import SidebarEnd from "@/components/Ui/Sidebar/SidebarEnd.tsx";
import Footer from "../Ui/Footer/Footer.tsx";
import "./layout-editor.pcss";
import "../Ui/colors.pcss";
import "../Ui/spacing.pcss";
import "../Ui/typography.pcss";
import "../Ui/utilities.pcss";
import DragContext from "@/components/LayoutEditor/DragContext.tsx";

export interface LayoutEditorProps extends LayoutIdentifiers {
  /**
   * A map of BlockPlugins keyed by plugin ID. Values are a function that will
   * return a promise that will resolve the file containing the plugin
   * component.
   */
  blockPlugins: Record<string, () => Promise<BlockPlugin>>;
  /**
   * A map of LayoutPlugins keyed by plugin ID. Values are a function that will
   * return a promise that will resolve the file containing the plugin
   * component.
   */
  layoutPlugins: Record<string, () => Promise<LayoutPlugin>>;
  /**
   * A map of FormatterPlugins keyed by plugin ID. Values are a function that
   * will return a promise that will resolve the file containing the plugin
   * component.
   */
  formatterPlugins: Record<string, () => Promise<FormatterPlugin<FieldValues>>>;
  /**
   * A map of WidgetPlugins keyed by plugin ID. Values are a function that will
   * return a promise that will resolve the file containing the plugin
   * component.
   */
  widgetPlugins: Record<string, () => Promise<WidgetPlugin<FieldValues>>>;
  /**
   * Page title.
   */
  title: string;
}

/**
 * Main entry for the layout editor.
 *
 * Provides the layout editor component and all associated stores and UI.
 */
const LayoutEditor: React.FC<LayoutEditorProps> = ({
  blockPlugins,
  layoutPlugins,
  formatterPlugins,
  widgetPlugins,
  title,
  ...layoutIdentifier
}) => {
  return (
    <Provider store={storeFactory(layoutIdentifier)}>
      <BlockPluginProvider plugins={blockPlugins}>
        <LayoutPluginProvider plugins={layoutPlugins}>
          <FormatterPluginProvider plugins={formatterPlugins}>
            <WidgetPluginProvider plugins={widgetPlugins}>
              <div className={"dlb"}>
                <TopBar title={title} />
                <DragContext>
                  <SidebarStart />
                  <Layout {...layoutIdentifier} />
                  <SidebarEnd />
                </DragContext>
                <Footer />
              </div>
            </WidgetPluginProvider>
          </FormatterPluginProvider>
        </LayoutPluginProvider>
      </BlockPluginProvider>
    </Provider>
  );
};

export default LayoutEditor;
