import React, { createContext, useState } from "react";
import { FieldValues } from "@/types/field.types.ts";
import { WidgetPlugin } from "@/types/widget.types.ts";
import { PluginCollection } from "@/types/plugin.types.ts";

export interface WidgetPluginProviderProps {
  /**
   * Children of the provider. These components will have access to the hook.
   */
  children: React.ReactNode;
  /**
   * Map of widget plugins keyed by plugin ID. The values are a promise that
   * will resolve to an object that implements WidgetPlugin.
   */
  plugins: Record<string, () => Promise<WidgetPlugin<FieldValues>>>;
}
export const WidgetPluginContext = createContext<
  PluginCollection<WidgetPlugin<FieldValues>>
>({ promises: {}, resolved: {}, updatePlugins: () => {} });

/**
 * Widget plugin context provider.
 */
const WidgetPluginProvider: React.FC<WidgetPluginProviderProps> = ({
  children,
  plugins,
}) => {
  const [providerState, updatePlugins] = useState({
    promises: plugins,
    resolved: {},
  });
  return (
    <WidgetPluginContext.Provider value={{ ...providerState, updatePlugins }}>
      {children}
    </WidgetPluginContext.Provider>
  );
};

export default WidgetPluginProvider;
