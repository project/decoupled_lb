import { Meta, StoryObj } from "@storybook/react";
import FormatterPluginProvider from "./FormatterPluginProvider.tsx";

const meta = {
  title: "Components/LayoutEditor/FormatterPluginProvider",
  component: FormatterPluginProvider,
  parameters: {
    layout: "centered",
  },
} satisfies Meta<typeof FormatterPluginProvider>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    children: (
      <div>
        Children here can use <code>useFormatterPlugin('plugin_id')</code>
      </div>
    ),
    plugins: {},
  },
};
