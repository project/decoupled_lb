import React from "react";
import { useBlockPlugin } from "../../hooks/useBlockPlugin.tsx";
import { useDispatch, useSelector } from "@/state/store.ts";
import { selectRegion } from "@/state/Slices/Layout/layoutSlice.ts";
import { Draggable } from "react-beautiful-dnd";
import { LayoutBlock as BlockInterface } from "../../types/block.types.ts";
import { Indexed } from "@/types/constants.ts";
import {
  selectMode,
  selectSelectedComponent,
  setSelectedComponent,
} from "@/state/Slices/Ui/uiSlice.ts";
import { DRAG_BLOCK, MODE_EDIT } from "@/types/ui.types.ts";
import classNames from "classnames";
import "./block.pcss";

const Block: React.FC<BlockInterface & Indexed> = ({
  uuid,
  configuration,
  regionId,
  weight,
  additional,
  index,
  fallback,
}) => {
  const El = useBlockPlugin(configuration.id, true);
  const dispatch = useDispatch();
  const selectedBlock = useSelector(selectSelectedComponent);
  const region = useSelector((state) => selectRegion(state, regionId));
  const mode = useSelector(selectMode);

  if (!El) {
    return <div>...Loading</div>;
  }

  const handleComponentSelection = (
    e: React.KeyboardEvent<HTMLElement> | React.MouseEvent<HTMLElement>,
  ) => {
    e.preventDefault();
    if (selectedBlock === uuid) {
      // Block is already selected, no we enter EDIT mode.
      dispatch(setSelectedComponent({ uuid, mode: MODE_EDIT }));
      return;
    }
    dispatch(setSelectedComponent({ uuid }));
  };

  const handleKeyDown = (e: React.KeyboardEvent<HTMLElement>) => {
    if (e.key !== "Enter") {
      return;
    }
    handleComponentSelection(e);
  };
  const blockLabel =
    configuration.label || `Block ${index} in ${region.region}`;
  if (selectedBlock === uuid && mode === MODE_EDIT) {
    return (
      <Draggable draggableId={`${DRAG_BLOCK}:${uuid}`} index={index}>
        {(provided) => (
          <div
            className={classNames({
              "dlb__component--active": selectedBlock === uuid,
            })}
            data-block={true}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            aria-label={blockLabel}
            ref={provided.innerRef}
            data-uuid={configuration.label}
          >
            <El.Edit
              uuid={uuid}
              fallback={fallback}
              configuration={configuration}
              regionId={regionId}
              weight={weight}
              additional={additional}
            />
          </div>
        )}
      </Draggable>
    );
  }
  return (
    <Draggable draggableId={`${DRAG_BLOCK}:${uuid}`} index={index}>
      {(provided) => (
        <div
          className={classNames("block", "block--previewing", {
            "dlb__component--active": selectedBlock === uuid,
          })}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          aria-label={blockLabel}
          aria-roledescription={"Draggable block"}
          aria-describedby={`${uuid}-description`}
          ref={provided.innerRef}
          data-uuid={configuration.label}
          onKeyDown={handleKeyDown}
          onClick={handleComponentSelection}
        >
          <El.Preview
            uuid={uuid}
            fallback={fallback}
            configuration={configuration}
            regionId={regionId}
            weight={weight}
            additional={additional}
          />
          <span hidden={true} id={`${uuid}-description`}>
            Press space bar to start a drag. Press ENTER to enter edit mode.
            When dragging you can use the arrow keys to move the item around and
            escape to cancel. Ensure your screen reader is in focus mode or
            forms mode
          </span>
        </div>
      )}
    </Draggable>
  );
};

export default Block;
