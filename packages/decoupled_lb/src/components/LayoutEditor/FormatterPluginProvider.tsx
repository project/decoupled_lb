import React, { createContext, useState } from "react";
import { FieldValues } from "@/types/field.types.ts";
import { FormatterPlugin } from "@/types/formatter.types.ts";
import { PluginCollection } from "@/types/plugin.types.ts";

export interface FormatterPluginProviderProps {
  children: React.ReactNode;
  plugins: Record<string, () => Promise<FormatterPlugin<FieldValues>>>;
}

export const FormatterPluginContext = createContext<
  PluginCollection<FormatterPlugin<FieldValues>>
>({
  resolved: {},
  promises: {},
  updatePlugins: () => {},
});
const FormatterPluginProvider: React.FC<FormatterPluginProviderProps> = ({
  children,
  plugins,
}) => {
  const [providerState, updatePlugins] = useState({
    promises: plugins,
    resolved: {},
  });
  return (
    <FormatterPluginContext.Provider
      value={{ ...providerState, updatePlugins }}
    >
      {children}
    </FormatterPluginContext.Provider>
  );
};

export default FormatterPluginProvider;
