import React, { DetailedHTMLProps, HTMLAttributes } from "react";
import Block from "./Block.tsx";
import DroppableStrictMode from "../DragAndDrop/DroppableStrictMode.tsx";
import { useSelector } from "@/state/store.ts";
import {
  selectComponents,
  selectLayoutSections,
  selectRegions,
} from "@/state/Slices/Layout/layoutSlice.ts";
import { selectActiveDrag } from "@/state/Slices/Ui/uiSlice.ts";
import "./region.pcss";
import { selectKeyedBlocks, useGetBlockListQuery } from "@/state/Slices/Api";
import { BlockInfo } from "@/types/block.types.ts";
import { Dictionary } from "@reduxjs/toolkit";
import { DRAG_BLOCK, DRAG_INSERT_BLOCK } from "@/types/ui.types.ts";

import { useRegionContext } from "@/hooks/useRegionContext.ts";

export interface RegionProps
  extends Partial<
    DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>
  > {
  /**
   * Region ID to render.
   */
  regionId: string;
  /**
   * HTML element to use to render the region.
   */
  as?: React.ElementType;
}

/**
 * Checks if the given block can be added to this section.
 */
const dropEligible = (
  blockId: string,
  regionId: string,
  sectionLayoutId: string,
  blockInfo: Dictionary<BlockInfo>,
): boolean => {
  const blockRestrictions = blockInfo[blockId]?.section_restrictions || [];
  return (
    blockRestrictions.filter(
      (restriction) =>
        restriction.layout_id === sectionLayoutId &&
        restriction.regions.includes(regionId),
    ).length > 0
  );
};

/**
 * A component that represents a region in a layout.
 */
const Region: React.FC<RegionProps> = ({
  regionId,
  as: Tag = "div",
  ...regionProps
}) => {
  const components = useSelector(selectComponents);
  const componentsInRegion = Object.entries(components)
    .map(([, component]) => component)
    .filter((component) => component.regionId === regionId);
  const regions = useSelector(selectRegions);
  const sections = useSelector(selectLayoutSections);
  const sectionLayoutId =
    regionId in regions ? sections[regions[regionId].section].layout_id : null;
  useGetBlockListQuery();
  const blockInfo = useSelector(selectKeyedBlocks);
  const activeDrag = useSelector(selectActiveDrag);
  const dropIsDisabled =
    !sectionLayoutId ||
    activeDrag === null ||
    ![DRAG_BLOCK, DRAG_INSERT_BLOCK].includes(activeDrag.type) ||
    (activeDrag.type === DRAG_INSERT_BLOCK &&
      !dropEligible(
        activeDrag.info.id,
        regions[regionId].region,
        sectionLayoutId,
        blockInfo,
      )) ||
    (activeDrag.type === DRAG_BLOCK &&
      !dropEligible(
        activeDrag.info.configuration.id,
        regions[regionId].region,
        sectionLayoutId,
        blockInfo,
      ));
  const regionContext = useRegionContext();
  return (
    <DroppableStrictMode
      droppableId={`${regionId}`}
      ignoreContainerClipping={true}
      isDropDisabled={dropIsDisabled}
    >
      {(provided, snapshot) => (
        <Tag
          data-region
          data-drop={
            activeDrag &&
            [DRAG_BLOCK, DRAG_INSERT_BLOCK].includes(activeDrag.type)
              ? dropIsDisabled
                ? "not-allowed"
                : snapshot?.isDraggingOver
                ? "allowed-over"
                : "allowed"
              : null
          }
          onMouseOver={(e: React.MouseEvent<HTMLElement>) => {
            e.preventDefault();
            regionContext.setIsOverRegion(true);
          }}
          onMouseOut={(e: React.MouseEvent<HTMLElement>) => {
            e.preventDefault();
            regionContext.setIsOverRegion(false);
          }}
          {...provided.droppableProps}
          {...regionProps}
          ref={provided.innerRef}
        >
          {componentsInRegion.map((component, ix) => (
            <Block
              key={component.uuid}
              {...component}
              index={ix}
              regionId={regionId}
            />
          ))}
          {provided.placeholder}
        </Tag>
      )}
    </DroppableStrictMode>
  );
};

export default Region;
