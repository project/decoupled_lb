import type { ReactRenderer } from "@storybook/react";
import { Meta, StoryObj } from "@storybook/react";
import Region from "./Region.tsx";
import DragContext from "@/components/LayoutEditor/DragContext.tsx";
import { Provider } from "react-redux";
import { storeFactory } from "@/state/store.ts";
import { dummyLayoutState } from "@/state/__tests__/DummyLayoutState.ts";

export type _ReactRenderer = ReactRenderer;

const store = storeFactory(
  {
    baseUrl: "/",
    sectionStorageType: "overrides",
    sectionStorage: "node.5",
  },
  dummyLayoutState,
);
const meta = {
  title: "Components/LayoutEditor/Region",
  component: Region,
  parameters: {
    layout: "centered",
  },
  decorators: [
    (story) => (
      <Provider store={store}>
        <DragContext>{story()}</DragContext>
      </Provider>
    ),
  ],
} satisfies Meta<typeof Region>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    regionId: "r1",
  },
};
