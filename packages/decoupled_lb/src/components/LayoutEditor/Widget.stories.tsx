import type { ReactRenderer } from "@storybook/react";
import { Meta, StoryObj } from "@storybook/react";
import Widget from "./Widget.tsx";
import WidgetPluginProvider from "@/components/LayoutEditor/WidgetPluginProvider.tsx";
import { widgetPlugins } from "@/components/LayoutEditor/__tests__/mockPluginProviders.ts";

export type _ReactRenderer = ReactRenderer;

const meta = {
  title: "Components/LayoutEditor/Widget",
  component: Widget,
  parameters: {
    layout: "centered",
  },
  decorators: [
    (story) => (
      <WidgetPluginProvider plugins={widgetPlugins}>
        {story()}
      </WidgetPluginProvider>
    ),
  ],
} satisfies Meta<typeof Widget>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    itemValues: [
      { value: "Hi this is a field value from the string_textfield widget" },
    ],
    widget: {
      type: "string_textfield",
      settings: {},
      third_party_settings: {},
      region: "content" as const,
      weight: 0,
    },
    fieldName: "field_field",
    update: () => {},
  },
};
