import { Meta, StoryObj } from "@storybook/react";
import BlockPluginProvider from "./BlockPluginProvider.tsx";

const meta = {
  title: "Components/LayoutEditor/BlockPluginProvider",
  component: BlockPluginProvider,
  parameters: {
    layout: "centered",
  },
} satisfies Meta<typeof BlockPluginProvider>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    children: (
      <div>
        Children here can use <code>useBlockPlugin('plugin_id')</code>
      </div>
    ),
    plugins: {},
  },
};
