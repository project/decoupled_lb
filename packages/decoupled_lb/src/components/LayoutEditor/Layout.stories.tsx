import type { ReactRenderer } from "@storybook/react";
import { Meta, StoryObj } from "@storybook/react";
import Layout from "./Layout.tsx";
import StoreDecorator from "@/state/__tests__/StoreDecorator.tsx";
import LayoutPluginProvider from "@/components/LayoutEditor/LayoutPluginProvider.tsx";
import {
  blockPlugins,
  layoutPlugins,
} from "@/components/LayoutEditor/__tests__/mockPluginProviders.ts";
import DragContext from "@/components/LayoutEditor/DragContext.tsx";
import BlockPluginProvider from "@/components/LayoutEditor/BlockPluginProvider.tsx";

export type _ReactRenderer = ReactRenderer;

const meta = {
  title: "Components/LayoutEditor/Layout",
  component: Layout,
  parameters: {
    layout: "fullscreen",
  },
  decorators: [
    (story) => (
      <StoreDecorator>
        <LayoutPluginProvider plugins={layoutPlugins}>
          <BlockPluginProvider plugins={blockPlugins}>
            <DragContext>{story()}</DragContext>
          </BlockPluginProvider>
        </LayoutPluginProvider>
      </StoreDecorator>
    ),
  ],
} satisfies Meta<typeof Layout>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    baseUrl: "/",
    sectionStorageType: "overrides",
    sectionStorage: "node.5",
  },
};
