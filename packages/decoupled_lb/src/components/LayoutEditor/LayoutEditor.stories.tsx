import { Meta, StoryObj } from "@storybook/react";
import LayoutEditor from "./LayoutEditor.tsx";

import blockPlugins from "../../debug/allBlockPlugins.ts";
import layoutPlugins from "../../debug/allLayoutPlugins.ts";
import formatterPlugins from "../../debug/allFormatterPlugins.ts";
import widgetPlugins from "../../debug/allWidgetPlugins.ts";

const meta = {
  title: "Components/LayoutEditor/LayoutEditor",
  component: LayoutEditor,
  parameters: {
    layout: "fullscreen",
  },
  tags: ["autodocs"],
} satisfies Meta<typeof LayoutEditor>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    title: "Sample layout builder",
    sectionStorage: "node.5",
    sectionStorageType: "overrides",
    baseUrl: "/",
    blockPlugins,
    layoutPlugins,
    formatterPlugins,
    widgetPlugins,
  },
};
