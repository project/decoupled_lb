import React, { createContext, useState } from "react";

import { BlockPlugin } from "@/types/block.types.ts";
import { PluginCollection } from "@/types/plugin.types.ts";

export interface BlockPluginProviderProps {
  children: React.ReactNode;
  plugins: Record<string, () => Promise<BlockPlugin>>;
}

export const BlockPluginContext = createContext<PluginCollection<BlockPlugin>>({
  resolved: {},
  promises: {},
  updatePlugins: () => {},
});

const BlockPluginProvider: React.FC<BlockPluginProviderProps> = ({
  children,
  plugins,
}) => {
  const [providerState, updatePlugins] = useState({
    promises: plugins,
    resolved: {},
  });
  return (
    <BlockPluginContext.Provider value={{ ...providerState, updatePlugins }}>
      {children}
    </BlockPluginContext.Provider>
  );
};

export default BlockPluginProvider;
