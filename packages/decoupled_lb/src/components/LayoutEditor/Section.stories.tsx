import type { ReactRenderer } from "@storybook/react";
import { Meta, StoryObj } from "@storybook/react";
import Section from "./Section.tsx";
import StoreDecorator from "@/state/__tests__/StoreDecorator.tsx";
import DragContext, {
  DROPPABLE_ID,
} from "@/components/LayoutEditor/DragContext.tsx";
import LayoutPluginProvider from "@/components/LayoutEditor/LayoutPluginProvider.tsx";
import { layoutPlugins } from "@/components/LayoutEditor/__tests__/mockPluginProviders.ts";
import DroppableStrictMode from "../DragAndDrop/DroppableStrictMode.tsx";

export type _ReactRenderer = ReactRenderer;

const meta = {
  title: "Components/LayoutEditor/Section",
  component: Section,
  parameters: {
    layout: "centered",
  },
  decorators: [
    (story) => (
      <StoreDecorator>
        <LayoutPluginProvider plugins={layoutPlugins}>
          <DragContext>
            <DroppableStrictMode droppableId={DROPPABLE_ID}>
              {(provided) => (
                <div {...provided.droppableProps} ref={provided.innerRef}>
                  {story()}
                </div>
              )}
            </DroppableStrictMode>
          </DragContext>
        </LayoutPluginProvider>
      </StoreDecorator>
    ),
  ],
} satisfies Meta<typeof Section>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    index: 0,
    id: "s1",
    layout_id: "fallback",
    layout_settings: {},
    weight: 0,
  },
};
