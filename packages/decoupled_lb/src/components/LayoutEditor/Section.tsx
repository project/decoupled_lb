import React from "react";
import { Draggable } from "react-beautiful-dnd";
import { useDispatch, useSelector } from "@/state/store.ts";
import { selectRegions } from "@/state/Slices/Layout/layoutSlice.ts";
import { useLayoutPlugin } from "@/hooks/useLayoutPlugin.tsx";
import { LayoutRegion, LayoutSection } from "@/types/layout.types.ts";
import { Indexed } from "@/types/constants.ts";
import "./section.pcss";
import {
  selectSelectedSection,
  setSelectedSection,
} from "@/state/Slices/Ui/uiSlice.ts";
import { selectSectionById, useGetSectionListQuery } from "@/state/Slices/Api";
import { nanoid } from "@reduxjs/toolkit";
import { DRAG_SECTION } from "@/types/ui.types.ts";

/**
 * The section component is responsible for loading and rendering a layout
 * plugin component. It takes care of adding drag and drop support for the
 * section so the layout plugin doesn't need to be concerned with this.
 */
const Section: React.FC<LayoutSection & Indexed> = ({ index, ...section }) => {
  const regions = useSelector(selectRegions);
  const dispatch = useDispatch();
  useGetSectionListQuery();
  const sectionInfo = useSelector((state) =>
    selectSectionById(state, section.layout_id),
  );
  const regionsInSection = Object.entries(regions)
    .map(([, region]) => region)
    .filter((region) => region.section === section.id)
    .reduce((carry, region) => ({ ...carry, [region.region]: region }), {});
  const defaultSections: Record<string, LayoutRegion> = Object.keys(
    sectionInfo ? sectionInfo.region_labels : {},
  ).reduce((carry, regionId) => {
    const uniqueId = nanoid();
    return {
      ...carry,
      [regionId]: {
        id: uniqueId,
        region: regionId,
        sectionId: section.id,
      },
    };
  }, {});
  const El = useLayoutPlugin(section.layout_id, true);
  const selectedSection = useSelector(selectSelectedSection);
  if (!El) {
    return <div>...Loading</div>;
  }
  const sectionLabel = section.layout_settings.label || `Section ${index + 1}`;
  return (
    <Draggable draggableId={`${DRAG_SECTION}:${section.id}`} index={index}>
      {(provided) => {
        return (
          <>
            <El.Preview
              section={section}
              regions={{ ...defaultSections, ...regionsInSection }}
              aria-label={section.layout_settings.label}
              sectionProps={{
                "aria-label": sectionLabel,
                "aria-roledescription": "Draggable section",
                ...provided.draggableProps,
                ...provided.dragHandleProps,
                "aria-describedby": `${section.id}-description`,
                onClick: () => dispatch(setSelectedSection(section.id)),
                "data-selected": selectedSection === section.id,
                ref: provided.innerRef,
              }}
            />
            <span hidden={true} id={`${section.id}-description`}>
              Press space bar to start a drag. Press ENTER to enter edit mode.
              When dragging you can use the arrow keys to move the item around
              and escape to cancel. Ensure your screen reader is in focus mode
              or forms mode
            </span>
          </>
        );
      }}
    </Draggable>
  );
};

export default Section;
