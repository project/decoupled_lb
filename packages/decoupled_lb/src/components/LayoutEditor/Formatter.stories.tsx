import type { ReactRenderer } from "@storybook/react";
import { Meta, StoryObj } from "@storybook/react";
import Formatter from "./Formatter.tsx";
import FormatterPluginProvider from "@/components/LayoutEditor/FormatterPluginProvider.tsx";
import { formatterPlugins } from "@/components/LayoutEditor/__tests__/mockPluginProviders.ts";

export type _ReactRenderer = ReactRenderer;

const meta = {
  title: "Components/LayoutEditor/Formatter",
  component: Formatter,
  parameters: {
    layout: "centered",
  },
  decorators: [
    (story) => (
      <FormatterPluginProvider plugins={formatterPlugins}>
        {story()}
      </FormatterPluginProvider>
    ),
  ],
} satisfies Meta<typeof Formatter>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    itemValues: [
      { value: "Hi this is a field value from the string formatter" },
    ],
    formatter: {
      type: "string",
      settings: {},
      third_party_settings: {},
      region: "content" as const,
      weight: 0,
    },
    fieldName: "field_field",
  },
};
