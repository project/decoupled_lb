import React, { createContext, useState } from "react";

import { LayoutPlugin } from "@/types/layout.types.ts";
import { PluginCollection } from "@/types/plugin.types.ts";

export interface LayoutPluginProviderProps {
  /**
   * Child components that need access to layout plugins via the hook.
   */
  children: React.ReactNode;
  plugins: Record<string, () => Promise<LayoutPlugin>>;
}

export const LayoutPluginContext = createContext<
  PluginCollection<LayoutPlugin>
>({
  resolved: {},
  promises: {},
  updatePlugins: () => {},
});
const LayoutPluginProvider: React.FC<LayoutPluginProviderProps> = ({
  children,
  plugins,
}) => {
  const [providerState, updatePlugins] = useState({
    promises: plugins,
    resolved: {},
  });
  return (
    <LayoutPluginContext.Provider value={{ ...providerState, updatePlugins }}>
      {children}
    </LayoutPluginContext.Provider>
  );
};

export default LayoutPluginProvider;
