import { FieldValues } from "@/types/field.types.ts";
import { WidgetProps } from "@/types/widget.types.ts";
import { useWidgetPlugin } from "@/hooks/useWidgetPlugin.tsx";

const Widget = <ValueType extends FieldValues>({
  itemValues,
  widget,
  fieldName,
  update,
}: WidgetProps<ValueType>) => {
  const WidgetPlugin = useWidgetPlugin(widget.type, true);
  if (!WidgetPlugin) {
    return null;
  }
  return (
    <WidgetPlugin.Widget
      itemValues={itemValues}
      widget={widget}
      fieldName={fieldName}
      update={update}
    />
  );
};

export default Widget;
