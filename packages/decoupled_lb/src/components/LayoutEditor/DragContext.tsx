import React, { createContext, useState } from "react";
import { RootState, useDispatch, useSelector } from "@/state/store.ts";
import {
  selectActiveDrag,
  selectSelectedComponent,
} from "@/state/Slices/Ui/uiSlice.ts";
import {
  BeforeCapture,
  DragDropContext,
  DropResult,
} from "react-beautiful-dnd";
import {
  selectKeyedBlocks,
  selectKeyedSections,
  useGetBlockListQuery,
  useGetSectionListQuery,
} from "@/state/Slices/Api";
import { handleBeforeDrag, handleDragEnd } from "@/utilities/DragHelpers.ts";
import { selectLayout } from "@/state/Slices/Layout/layoutSlice.ts";

export interface DragContextProps {
  children: React.ReactNode;
}

export interface RegionHoverContextInterface {
  isOverRegion: boolean;
  // eslint-disable-next-line no-unused-vars
  setIsOverRegion: (isOver: boolean) => void;
}

export const RegionHoverContext = createContext<RegionHoverContextInterface>({
  isOverRegion: false,
  setIsOverRegion: () => {},
});

export const DROPPABLE_ID = "layout";

const DragContext: React.FC<DragContextProps> = ({ children }) => {
  const dispatch = useDispatch();
  const selectedComponent = useSelector(selectSelectedComponent);
  const activeDrag = useSelector(selectActiveDrag);
  const [isOverRegion, setIsOverRegion] = useState<boolean>(false);

  useGetBlockListQuery();
  useGetSectionListQuery();
  const blockInfo = useSelector((state: RootState) => selectKeyedBlocks(state));
  const sectionInfo = useSelector((state: RootState) =>
    selectKeyedSections(state),
  );
  const layout = useSelector(selectLayout);
  const regionHoverContextValue = {
    isOverRegion,
    setIsOverRegion,
  };
  return (
    <RegionHoverContext.Provider value={regionHoverContextValue}>
      <DragDropContext
        onDragEnd={(result: DropResult) => {
          handleDragEnd(
            result,
            selectedComponent,
            dispatch,
            activeDrag,
            sectionInfo,
          );
        }}
        onBeforeCapture={(result: BeforeCapture) => {
          handleBeforeDrag(
            dispatch,
            result,
            blockInfo,
            sectionInfo,
            layout.layout,
          );
        }}
        onDragUpdate={() => {}}
        // onDragStart={dragStart}
      >
        {children}
      </DragDropContext>
    </RegionHoverContext.Provider>
  );
};

export default DragContext;
