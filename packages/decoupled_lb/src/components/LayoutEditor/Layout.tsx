import React, { useEffect } from "react";
import Section from "./Section.tsx";
import DroppableStrictMode from "../DragAndDrop/DroppableStrictMode.tsx";
import { useDispatch, useSelector } from "@/state/store.ts";
import {
  loadLayout,
  saveLayout,
  selectLayoutSections,
  selectLayoutState,
} from "@/state/Slices/Layout/layoutSlice.ts";

import { LayoutIdentifiers } from "@/types/layout.types.ts";
import {
  selectActiveDrag,
  selectorUiState,
} from "@/state/Slices/Ui/uiSlice.ts";
import classNames from "classnames";
import { DROPPABLE_ID } from "@/components/LayoutEditor/DragContext.tsx";
import { STATE_DIRTY, STATE_IDLE } from "@/types/constants.ts";
import {
  DRAG_INSERT_BLOCK,
  DRAG_INSERT_SECTION,
  DRAG_SECTION,
} from "@/types/ui.types.ts";
import { useRegionContext } from "@/hooks/useRegionContext.ts";

const Layout: React.FC<LayoutIdentifiers> = ({
  baseUrl,
  sectionStorage,
  sectionStorageType,
}) => {
  const dispatch = useDispatch();
  const layoutState = useSelector(selectLayoutState);
  const dragIsActive = useSelector(selectActiveDrag);
  const { startSidebarOpen, endSidebarOpen } = useSelector(selectorUiState);
  useEffect(() => {
    if (layoutState === STATE_IDLE) {
      dispatch(loadLayout({ baseUrl, sectionStorage, sectionStorageType }));
    }
  }, [layoutState, dispatch, baseUrl, sectionStorage, sectionStorageType]);

  useEffect(() => {
    if (layoutState === STATE_DIRTY && dragIsActive === null) {
      const timeout = setTimeout(() => {
        dispatch(saveLayout({ baseUrl, sectionStorage, sectionStorageType }));
      }, 5000);
      return () => clearTimeout(timeout);
    }
  }, [
    layoutState,
    dispatch,
    baseUrl,
    sectionStorage,
    sectionStorageType,
    dragIsActive,
  ]);

  const regionContext = useRegionContext();
  const activeDrag = useSelector(selectActiveDrag);
  const sections = useSelector(selectLayoutSections);
  return (
    <div
      className={classNames("dlb__layout", {
        "dlb__layout--start-sidebar-open": startSidebarOpen,
        "dlb__layout--end-sidebar-open": endSidebarOpen,
      })}
    >
      <DroppableStrictMode
        droppableId={DROPPABLE_ID}
        isDropDisabled={
          activeDrag === null ||
          regionContext.isOverRegion ||
          ![DRAG_SECTION, DRAG_INSERT_SECTION, DRAG_INSERT_BLOCK].includes(
            activeDrag.type,
          )
        }
      >
        {(provided) => (
          <div
            {...provided.droppableProps}
            ref={provided.innerRef}
            data-section-drop={
              activeDrag !== null &&
              [DRAG_INSERT_BLOCK, DRAG_INSERT_SECTION, DRAG_SECTION].includes(
                activeDrag.type,
              )
            }
          >
            {Object.keys(sections).map((sectionId, index) => (
              <Section key={sectionId} {...sections[sectionId]} index={index} />
            ))}
            {provided.placeholder}
          </div>
        )}
      </DroppableStrictMode>
    </div>
  );
};

export default Layout;
