import { useFormatterPlugin } from "../../hooks/useFormatterPlugin.tsx";
import { FieldValues } from "@/types/field.types.ts";
import { FormatterProps } from "@/types/formatter.types.ts";

const Formatter = <ValueType extends FieldValues>({
  itemValues,
  formatter,
  fieldName,
}: FormatterProps<ValueType>) => {
  const FormatterPlugin = useFormatterPlugin(formatter.type, true);
  if (!FormatterPlugin) {
    return <div>...Loading</div>;
  }
  return (
    <FormatterPlugin.Formatter
      itemValues={itemValues}
      formatter={formatter}
      fieldName={fieldName}
    />
  );
};

export default Formatter;
