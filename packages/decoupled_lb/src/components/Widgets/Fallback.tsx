import React from "react";
import { WidgetProps } from "@/types/widget.types.ts";
import { FieldValues } from "@/types/field.types.ts";

export const Widget: React.FC<WidgetProps<FieldValues>> = ({
  fieldName,
  widget,
}) => {
  return (
    <div className="fallback">
      The {fieldName} field is using the {widget.type} widget that doesn't have
      a React version. So is being rendered by a fallback.
    </div>
  );
};

export const id = "fallback";
