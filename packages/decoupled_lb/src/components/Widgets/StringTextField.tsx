import React from "react";
import { WidgetProps } from "@/types/widget.types.ts";
import Field from "@/components/Theme/Field.tsx";
import { FieldValues } from "@/types/field.types.ts";
import { DisplayConfiguration } from "@/types/displayMode.types.ts";

export interface StringItemFieldValues extends FieldValues {
  value: string;
}
export interface StringTextFieldSettings extends DisplayConfiguration {
  size: number;
  placeholder: string;
}
export const Widget: React.FC<
  WidgetProps<StringItemFieldValues, StringTextFieldSettings>
> = ({ itemValues, fieldName }) => {
  return (
    <Field
      multiple={true}
      items={itemValues.map((item) => ({
        content: item.value,
        attributes: {
          contentEditable: true,
          tabIndex: 0,
          role: "textbox",
          "aria-label": `Field: ${fieldName}`,
        },
      }))}
    />
  );
};

export const id = "string_textfield";
