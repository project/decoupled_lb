import React from "react";
import { WidgetProps } from "@/types/widget.types.ts";
import Field from "@/components/Theme/Field.tsx";
import Editor from "@/components/CKEditor/Editor.tsx";

export const Widget: React.FC<
  WidgetProps<{ value: string; processed: string; format: string }>
> = ({ itemValues, update }) => {
  return (
    <Field
      items={itemValues.map((item, delta) => ({
        content: (
          <Editor
            data={item.value}
            id={delta}
            onChange={(_, editor) => {
              const data = editor.getData();
              const newValues = [...itemValues];
              newValues.splice(delta, 1, {
                value: data,
                processed: data,
                format: item.format,
              });
              update(newValues);
            }}
          />
        ),
      }))}
    />
  );
};

export const id = "text_textarea";
