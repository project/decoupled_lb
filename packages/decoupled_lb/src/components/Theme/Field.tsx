import { DetailedHTMLProps, HTMLAttributes, ReactNode } from "react";

interface ItemProps {
  // Contents of the item.
  content: ReactNode;
  // Item attributes.
  attributes?: Partial<
    DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>
  >;
}

export interface FieldProps {
  /**
   * Array of field items with keys 'content' for the content and optional
   * attributes for individual item attributes.
   */
  items: Array<ItemProps>;
  /**
   * True if the label is hidden.
   */
  labelHidden?: boolean;
  /**
   * Field label.
   */
  label?: string;
  /**
   * True if multiple items are being output
   */
  multiple?: boolean;
  /**
   * Label attributes.
   */
  labelAttributes?: Partial<
    DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>
  >;
  /**
   * Component attributes.
   */
  attributes?: Partial<
    DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>
  >;
}

/**
 * Markup for rendering a field item list.
 */
const Field = ({
  items,
  label = "",
  labelHidden = true,
  multiple = false,
  attributes = {},
  labelAttributes = {},
}: FieldProps) => {
  if (labelHidden) {
    if (multiple) {
      return (
        <div {...attributes}>
          {items.map((item, ix) => (
            <div {...(item.attributes || {})} key={ix}>
              {item.content}
            </div>
          ))}
        </div>
      );
    }
    return items.map((item, ix) => (
      <div {...attributes} key={ix}>
        {item.content}
      </div>
    ));
  }
  return (
    <div {...attributes}>
      <div {...labelAttributes}>{label}</div>
      {multiple && (
        <div>
          {items.map((item, ix) => (
            <div {...(item.attributes || {})} key={ix}>
              {item.content}
            </div>
          ))}
        </div>
      )}
      {!multiple &&
        items.map((item, ix) => (
          <div {...(item.attributes || {})} key={ix}>
            {item.content}
          </div>
        ))}
    </div>
  );
};

export default Field;
