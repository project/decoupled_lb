import { Meta, StoryObj } from "@storybook/react";
import Field from "./Field.tsx";

const meta = {
  title: "Theme/Field",
  component: Field,
  parameters: {
    layout: "centered",
  },
  tags: ["autodocs"],
} satisfies Meta<typeof Field>;

export default meta;
type Story = StoryObj<typeof meta>;

const defaultItems = [{ content: "An item" }];
const multipleItems = [{ content: "Item 1" }, { content: "Item 2" }];

export const WithLabel: Story = {
  args: {
    items: defaultItems,
    labelHidden: false,
    label: "Field label",
  },
};

export const WithAttributes: Story = {
  args: {
    items: [
      {
        content: "An item",
        attributes: {
          tabIndex: 0,
          "aria-description": "This is the field item",
        },
      },
    ],
    labelAttributes: {
      tabIndex: 0,
      "aria-description": "This is the field label",
    },
    attributes: {
      tabIndex: 0,
      "aria-description": "This is the field output",
    },
    labelHidden: false,
    label: "Field label",
  },
};

export const Multiple: Story = {
  args: {
    items: multipleItems,
    labelHidden: false,
    multiple: true,
    label: "Field label",
  },
};

export const WithHiddenLabel: Story = {
  args: {
    items: defaultItems,
    labelHidden: true,
    label: "Field label",
  },
};

export const MultipleWithHiddenLabel: Story = {
  args: {
    items: multipleItems,
    multiple: true,
    labelHidden: true,
    label: "Field label",
  },
};
