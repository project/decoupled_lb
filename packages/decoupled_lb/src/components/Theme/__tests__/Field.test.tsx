import { render } from "@testing-library/react";
import { composeStories } from "@storybook/react";
import * as FieldStories from "../Field.stories.tsx";

const {
  WithLabel,
  WithHiddenLabel,
  WithAttributes,
  MultipleWithHiddenLabel,
  Multiple,
} = composeStories(FieldStories);

describe("Field theme component", () => {
  it("Should render with label", () => {
    const { container } = render(<WithLabel />);
    expect(container).toMatchSnapshot();
  });
  it("Should render with label hidden", () => {
    const { container } = render(<WithHiddenLabel />);
    expect(container).toMatchSnapshot();
  });
  it("Should render with attributesl", () => {
    const { container } = render(<WithAttributes />);
    expect(container).toMatchSnapshot();
  });
  it("Should render multiple with hidden label", () => {
    const { container } = render(<MultipleWithHiddenLabel />);
    expect(container).toMatchSnapshot();
  });
  it("Should render with multiple", () => {
    const { container } = render(<Multiple />);
    expect(container).toMatchSnapshot();
  });
});
