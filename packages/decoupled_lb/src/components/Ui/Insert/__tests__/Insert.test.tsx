import { render, screen, waitFor } from "@testing-library/react";
import DragContext from "@/components/LayoutEditor/DragContext.tsx";
import Insert from "@/components/Ui/Insert/Insert.tsx";
import StoreDecorator from "@/state/__tests__/StoreDecorator.tsx";

describe("Insert", () => {
  it("Should render", async () => {
    const { container } = render(
      <StoreDecorator>
        <DragContext>
          <Insert />
        </DragContext>
      </StoreDecorator>,
    );
    expect(container).toMatchSnapshot();
    await waitFor(() => {
      expect(screen.getByText("Content block")).toBeInTheDocument();
    });
  });
});
