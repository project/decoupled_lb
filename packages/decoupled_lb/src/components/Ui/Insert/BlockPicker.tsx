import React from "react";
import { selectAllBlocks, useGetBlockListQuery } from "@/state/Slices/Api";
import { decodeError } from "@/utilities/ErrorHelpers.ts";
import NewBlock from "@/components/Ui/Insert/NewBlock.tsx";
import DroppableStrictMode from "@/components/DragAndDrop/DroppableStrictMode.tsx";
import { useSelector } from "@/state/store.ts";
import { BlockInfo } from "@/types/block.types.ts";

interface BlockPickerProps {}

export const BLOCK_PICKER_DROPPABLE_ID = `block-picker`;
const BlockPicker: React.FC<BlockPickerProps> = () => {
  const { error, isLoading } = useGetBlockListQuery();
  const blocks = useSelector((state) => selectAllBlocks(state));

  if (isLoading) {
    return <div>...loading</div>;
  }
  if (error) {
    return <div>{decodeError(error, "Could not fetch block list")}</div>;
  }
  if (blocks) {
    let index = 0;
    return (
      <DroppableStrictMode
        droppableId={BLOCK_PICKER_DROPPABLE_ID}
        isDropDisabled={true}
      >
        {(provided) => (
          <div {...provided.droppableProps} ref={provided.innerRef}>
            {Object.entries(
              blocks.reduce(
                (carry: Record<string, Array<BlockInfo>>, block) => ({
                  ...carry,
                  [block.category]: [
                    ...(carry[block.category] || []),
                    ...[block],
                  ],
                }),
                {},
              ),
            ).map(([category, groupedBlocks], catIndex) => (
              <div key={catIndex}>
                <h3>{category}</h3>
                {groupedBlocks.map((block) => (
                  <NewBlock key={block.id} index={index++} {...block} />
                ))}
              </div>
            ))}
            <div hidden>{provided.placeholder}</div>
          </div>
        )}
      </DroppableStrictMode>
    );
  }
};

export default BlockPicker;
