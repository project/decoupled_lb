import { Meta, StoryObj } from "@storybook/react";
import Insert from "./Insert.tsx";
import StoreDecorator from "@/state/__tests__/StoreDecorator.tsx";

import type { ReactRenderer } from "@storybook/react";
import DragContext from "@/components/LayoutEditor/DragContext.tsx";
export type _ReactRenderer = ReactRenderer;

const meta = {
  title: "Components/Ui/Insert",
  component: Insert,
  parameters: {
    layout: "centered",
  },
  decorators: [
    (story) => (
      <StoreDecorator>
        <DragContext>{story()}</DragContext>
      </StoreDecorator>
    ),
  ],
} satisfies Meta<typeof Insert>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {};
