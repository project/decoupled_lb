import React from "react";
import { SectionInfo } from "@/types/block.types.ts";
import { Indexed } from "@/types/constants.ts";
import { Draggable } from "react-beautiful-dnd";
import { DRAG_INSERT_SECTION } from "@/types/ui.types.ts";

const NewSection: React.FC<SectionInfo & Indexed> = ({
  id,
  index,
  icon,
  icon_is_url,
  label,
}) => {
  return (
    <Draggable draggableId={`${DRAG_INSERT_SECTION}:${id}`} index={index}>
      {(sectionProvided) => (
        <div
          className={"dlb__insert-section dlb__insert-item"}
          ref={sectionProvided.innerRef}
          {...sectionProvided.draggableProps}
          {...sectionProvided.dragHandleProps}
        >
          {icon_is_url && icon && <img src={icon} alt={`Icon for ${label}`} />}
          {!icon_is_url && icon && (
            <span dangerouslySetInnerHTML={{ __html: icon }} />
          )}
          {label}
        </div>
      )}
    </Draggable>
  );
};

export default NewSection;
