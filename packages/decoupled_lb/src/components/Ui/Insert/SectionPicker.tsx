import React from "react";
import { selectAllSections, useGetSectionListQuery } from "@/state/Slices/Api";
import { decodeError } from "@/utilities/ErrorHelpers.ts";
import { useSelector } from "@/state/store.ts";
import DroppableStrictMode from "@/components/DragAndDrop/DroppableStrictMode.tsx";
import NewSection from "@/components/Ui/Insert/NewSection.tsx";

interface SectionPickerProps {}

export const SECTION_PICKER_DROPPABLE_ID = "section-picker";
const SectionPicker: React.FC<SectionPickerProps> = () => {
  const { error, isLoading } = useGetSectionListQuery();
  const sectionInfo = useSelector((state) => selectAllSections(state));
  if (isLoading) {
    return <div>...loading</div>;
  }
  if (error) {
    return <div>{decodeError(error, "Could not fetch section list")}</div>;
  }
  if (sectionInfo.length) {
    return (
      <DroppableStrictMode
        droppableId={SECTION_PICKER_DROPPABLE_ID}
        isDropDisabled={true}
      >
        {(provided) => (
          <div {...provided.droppableProps} ref={provided.innerRef}>
            <h3>Layouts</h3>
            {sectionInfo.map((section, index) => (
              <NewSection key={section.id} index={index} {...section} />
            ))}
            {provided.placeholder}
          </div>
        )}
      </DroppableStrictMode>
    );
  }
};

export default SectionPicker;
