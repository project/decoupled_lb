import "./insert.pcss";

import React from "react";
import BlockPicker from "@/components/Ui/Insert/BlockPicker.tsx";
import SectionPicker from "@/components/Ui/Insert/SectionPicker.tsx";

/**
 * Provides a sidebar pane for inserting new content into the page.
 */
const Insert: React.FC = () => {
  return (
    <>
      <BlockPicker />
      <SectionPicker />
    </>
  );
};

export default Insert;
