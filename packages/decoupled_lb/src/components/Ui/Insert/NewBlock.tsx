import React from "react";
import { BlockInfo } from "@/types/block.types.ts";
import { Draggable } from "react-beautiful-dnd";
import { Indexed } from "@/types/constants.ts";
import { DRAG_INSERT_BLOCK } from "@/types/ui.types.ts";
import classNames from "classnames";

const NewBlock: React.FC<BlockInfo & Indexed> = ({
  label,
  id,
  index,
  icon,
  icon_is_url,
}) => {
  return (
    <Draggable draggableId={`${DRAG_INSERT_BLOCK}:${id}`} index={index}>
      {(blockProvided) => (
        <div
          className={classNames("dlb__insert-block", "dlb__insert-item", {
            "dlb__insert-block--no-icon": !icon,
          })}
          ref={blockProvided.innerRef}
          {...blockProvided.draggableProps}
          {...blockProvided.dragHandleProps}
        >
          {icon_is_url && icon && <img src={icon} alt={`Icon for ${label}`} />}
          {!icon_is_url && icon && (
            <span dangerouslySetInnerHTML={{ __html: icon }} />
          )}
          {label}
        </div>
      )}
    </Draggable>
  );
};

export default NewBlock;
