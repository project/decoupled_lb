import "./outline.pcss";

import React from "react";
import { RootState, useDispatch, useSelector } from "@/state/store.ts";
import {
  selectComponents,
  selectLayoutSections,
  selectRegions,
} from "@/state/Slices/Layout/layoutSlice.ts";
import {
  selectActiveDrag,
  selectSelectedComponent,
  selectSelectedSection,
  setSelectedComponent,
} from "@/state/Slices/Ui/uiSlice.ts";
import { t } from "@/utilities/Translation.ts";
import DroppableStrictMode from "@/components/DragAndDrop/DroppableStrictMode";
import { Draggable } from "react-beautiful-dnd";
import {
  selectKeyedSections,
  useGetSectionListQuery,
} from "@/state/Slices/Api";
import classNames from "classnames";
import { DRAG_OUTLINE_BLOCK, DRAG_OUTLINE_SECTION } from "@/types/ui.types.ts";

interface OutlineProps {}
export const OUTLINE_DROPPABLE_ID = "outline";
const Outline: React.FC<OutlineProps> = () => {
  const components = useSelector(selectComponents);
  const sections = useSelector(selectLayoutSections);
  const regions = useSelector(selectRegions);
  const dispatch = useDispatch();
  const activeDrag = useSelector(selectActiveDrag);
  useGetSectionListQuery();
  const sectionInfo = useSelector((state: RootState) =>
    selectKeyedSections(state),
  );
  const selectedComponent = useSelector(selectSelectedComponent);
  const selectedSection = useSelector(selectSelectedSection);

  return (
    <DroppableStrictMode
      droppableId={OUTLINE_DROPPABLE_ID}
      isDropDisabled={
        activeDrag === null || activeDrag.type !== DRAG_OUTLINE_SECTION
      }
    >
      {(provided) => (
        <div
          className={"dlb__outline"}
          {...provided.droppableProps}
          ref={provided.innerRef}
        >
          {Object.entries(sections).map(
            ([sectionId, section], sectionIndex) => (
              <Draggable
                draggableId={`${DRAG_OUTLINE_SECTION}:${section.id}`}
                index={sectionIndex}
                key={sectionId}
              >
                {(sectionProvided) => (
                  <div
                    className={classNames("dlb__outline-section", {
                      "dlb__outline-section--selected":
                        section.id === selectedSection,
                    })}
                    key={sectionId}
                    ref={sectionProvided.innerRef}
                    {...sectionProvided.draggableProps}
                  >
                    <div
                      className={"dlb__outline-drag"}
                      {...sectionProvided.dragHandleProps}
                    >
                      ↕️
                    </div>
                    <button>
                      {section.layout_settings.label ||
                        t(`Section @section`, {
                          "@section": `${section.weight + 1}`,
                        })}
                    </button>
                    {Object.entries(regions)
                      .filter(([, region]) => region.section === sectionId)
                      .map(([regionId, region]) => (
                        <DroppableStrictMode
                          key={regionId}
                          droppableId={`${regionId}:outline`}
                          isDropDisabled={
                            activeDrag === null ||
                            activeDrag.type !== DRAG_OUTLINE_BLOCK
                          }
                        >
                          {(regionProvided) => (
                            <div
                              key={regionId}
                              className={"dlb__outline-region"}
                              {...regionProvided.droppableProps}
                              ref={regionProvided.innerRef}
                            >
                              <button>
                                {sectionInfo[section.layout_id]?.region_labels[
                                  region.region
                                ] || region.region}
                              </button>
                              {Object.entries(components)
                                .filter(
                                  ([, component]) =>
                                    component.regionId === regionId,
                                )
                                .map(
                                  (
                                    [componentId, component],
                                    componentIndex,
                                  ) => (
                                    <Draggable
                                      key={componentId}
                                      draggableId={`${DRAG_OUTLINE_BLOCK}:${componentId}`}
                                      index={componentIndex}
                                    >
                                      {(blockProvided) => (
                                        <div
                                          className={classNames(
                                            "dlb__outline-component",
                                            {
                                              "dlb__outline-component--selected":
                                                componentId ===
                                                selectedComponent,
                                            },
                                          )}
                                          ref={blockProvided.innerRef}
                                          {...blockProvided.draggableProps}
                                        >
                                          <div
                                            className={"dlb__outline-drag"}
                                            {...blockProvided.dragHandleProps}
                                          >
                                            ↕️
                                          </div>
                                          <button
                                            onClick={() =>
                                              dispatch(
                                                setSelectedComponent({
                                                  uuid: componentId,
                                                }),
                                              )
                                            }
                                          >
                                            {component.configuration.label ||
                                              t(`Component @componentId`, {
                                                "@componentId": componentId,
                                              })}
                                          </button>
                                        </div>
                                      )}
                                    </Draggable>
                                  ),
                                )}
                              {regionProvided.placeholder}
                            </div>
                          )}
                        </DroppableStrictMode>
                      ))}
                  </div>
                )}
              </Draggable>
            ),
          )}
          {provided.placeholder}
        </div>
      )}
    </DroppableStrictMode>
  );
};

export default Outline;
