import type { ReactRenderer } from "@storybook/react";
import { Meta, StoryObj } from "@storybook/react";
import LayoutSettings from "./LayoutSettings.tsx";
import { storeFactory } from "@/state/store.ts";
import { dummyLayoutState } from "@/state/__tests__/DummyLayoutState.ts";
import { Provider } from "react-redux";
import { setSelectedSection } from "@/state/Slices/Ui/uiSlice.ts";

const store = storeFactory(
  {
    baseUrl: "/",
    sectionStorageType: "overrides",
    sectionStorage: "node.5",
  },
  dummyLayoutState,
);

store.dispatch(setSelectedSection("s1"));
export type _ReactRenderer = ReactRenderer;

const meta = {
  title: "Components/Ui/LayoutSettings",
  component: LayoutSettings,
  parameters: {
    layout: "centered",
  },
  tags: ["autodocs"],
  decorators: [(story) => <Provider store={store}>{story()}</Provider>],
} satisfies Meta<typeof LayoutSettings>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {};
