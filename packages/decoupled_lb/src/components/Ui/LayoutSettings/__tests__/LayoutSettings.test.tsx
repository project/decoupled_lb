import { storeFactory } from "@/state/store.ts";
import { dummyLayoutState } from "@/state/__tests__/DummyLayoutState.ts";
import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import { userEvent } from "@testing-library/user-event";
import LayoutSettings from "@/components/Ui/LayoutSettings/LayoutSettings.tsx";
import { setSelectedSection } from "@/state/Slices/Ui/uiSlice.ts";

const store = storeFactory(
  {
    baseUrl: "/",
    sectionStorageType: "overrides",
    sectionStorage: "node.5",
  },
  dummyLayoutState,
);
store.dispatch(setSelectedSection("s1"));

const user = userEvent.setup();

const setup = () => {
  return render(
    <Provider store={store}>
      <LayoutSettings />
    </Provider>,
  );
};

describe("Layout settings", () => {
  it("Should render", () => {
    const { container } = setup();
    expect(screen.getByRole("textbox", { name: "Label" })).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });
  it("Should set the label", async () => {
    setup();
    const input: HTMLInputElement = screen.getByRole("textbox", {
      name: "Label",
    });
    expect(input).toBeInTheDocument();
    expect(input.value).toEqual("s1");
    await user.clear(input);
    await user.type(input, "New label");
    expect(input.value).toEqual("New label");
    expect(
      store.getState().layout.layout.sections.s1.layout_settings.label,
    ).toEqual("New label");
  });
});
