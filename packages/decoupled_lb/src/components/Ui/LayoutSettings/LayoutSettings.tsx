import React from "react";
import { useSelector } from "@/state/store.ts";
import { selectSelectedSection } from "@/state/Slices/Ui/uiSlice.ts";
import { selectLayoutSections } from "@/state/Slices/Layout/layoutSlice.ts";
import { useLayoutPlugin } from "@/hooks/useLayoutPlugin.tsx";
import "../misc/settings.pcss";

const LayoutSettings: React.FC = () => {
  const selectedSection = useSelector(selectSelectedSection);
  const sections = useSelector(selectLayoutSections);
  const Layout = useLayoutPlugin(
    selectedSection ? sections[selectedSection].layout_id : null,
    true,
  );
  if (!selectedSection || !Layout || !Layout.Settings) {
    return;
  }
  return <Layout.Settings {...sections[selectedSection]} />;
};

export default LayoutSettings;
