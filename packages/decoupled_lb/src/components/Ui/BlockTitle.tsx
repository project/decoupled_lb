import React from "react";
import { LayoutBlock, BlockConfiguration } from "@/types/block.types.ts";
import { useDispatch } from "@/state/store.ts";
import { updateBlock } from "@/state/Slices/Layout/layoutSlice.ts";
import "./block-title.pcss";
export const Preview: React.FC<BlockConfiguration> = ({
  label,
  label_display,
}) => {
  if (label_display) {
    return <h2>{label}</h2>;
  }
};

export const Edit: React.FC<LayoutBlock> = (block) => {
  const { uuid, configuration } = block;
  const { label, label_display } = configuration;

  const dispatch = useDispatch();
  const setTitle = (title: string) => {
    dispatch(
      updateBlock({
        componentId: uuid,
        values: {
          ...block,
          configuration: {
            ...block.configuration,
            label: title,
          },
        },
      }),
    );
  };
  if (!label_display) {
    return;
  }
  return (
    <>
      <label htmlFor={`label-${uuid}`} hidden>
        Block label
      </label>
      <input
        className={"as-h2"}
        name={"label"}
        id={`label-${uuid}`}
        value={label || ""}
        onChange={(e) => setTitle(e.currentTarget.value)}
      />
    </>
  );
};
