import React from "react";
import { useDispatch, useSelector } from "@/state/store.ts";
import {
  selectorUiState,
  setActiveSidebarStartPane,
  setEndSidebarOpen,
  setMode,
} from "@/state/Slices/Ui/uiSlice.ts";
import "./topbar.pcss";
import {
  MODE_EDIT,
  MODE_SELECT,
  SIDEBAR_TAB_INSERT,
  SIDEBAR_TAB_OUTLINE,
} from "@/types/ui.types.ts";
import {
  saveLayout,
  selectLayout,
  selectLayoutState,
} from "@/state/Slices/Layout/layoutSlice.ts";
import {
  STATE_CLEAN,
  STATE_DIRTY,
  STATE_ERROR,
  STATE_LOADING,
  STATE_SAVING,
} from "@/types/constants.ts";
import classNames from "classnames";
import { selectBaseUrl } from "@/state/Slices/Drupal/drupalSlice.ts";

export interface TopBarProps {
  /**
   * Title shown in editor.
   */
  title: string;
}

/**
 * Defines the top toolbar component.
 */
const TopBar: React.FC<TopBarProps> = ({ title }) => {
  const { endSidebarOpen, startSidebarOpen, mode } =
    useSelector(selectorUiState);
  const layoutState = useSelector(selectLayoutState);
  const { sectionStorageType, sectionStorage } = useSelector(selectLayout);
  const baseUrl = useSelector(selectBaseUrl);
  const dispatch = useDispatch();
  return (
    <div className="dlb__topbar">
      <button
        onClick={() => dispatch(setActiveSidebarStartPane(SIDEBAR_TAB_INSERT))}
        aria-pressed={startSidebarOpen}
        aria-controls={"dlb-sidebar-start"}
      >
        ➕
      </button>
      <button
        onClick={() => dispatch(setActiveSidebarStartPane(SIDEBAR_TAB_OUTLINE))}
        aria-pressed={startSidebarOpen}
        aria-controls={"dlb-sidebar-start"}
      >
        📂
      </button>
      <button
        onClick={() =>
          dispatch(setMode(mode === MODE_EDIT ? MODE_SELECT : MODE_EDIT))
        }
      >
        {mode === MODE_EDIT ? "🖱️" : "✏️"}
      </button>
      <h1>{title}</h1>
      <div
        className={classNames("state", {
          "state--loading": layoutState === STATE_LOADING,
          "state--error": layoutState === STATE_ERROR,
          "state--dirty": layoutState === STATE_DIRTY,
          "state--clean": layoutState === STATE_CLEAN,
          "state--saving": layoutState === STATE_SAVING,
        })}
      >
        {layoutState === STATE_LOADING && "Loading"}
        {layoutState === STATE_ERROR && "An error occurred"}
        {layoutState === STATE_DIRTY && "Changes to be saved"}
        {layoutState === STATE_CLEAN && "All changes saved"}
        {layoutState === STATE_SAVING && "Saving changes"}
      </div>
      {layoutState === STATE_DIRTY && (
        <button
          onClick={() =>
            dispatch(
              saveLayout({
                baseUrl,
                sectionStorage,
                sectionStorageType,
              }),
            )
          }
        >
          💾
        </button>
      )}
      <button
        onClick={() => dispatch(setEndSidebarOpen(true))}
        aria-pressed={endSidebarOpen}
        aria-controls={"dlb-sidebar-end"}
      >
        ⚙️
      </button>
    </div>
  );
};
export default TopBar;
