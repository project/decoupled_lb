import type { ReactRenderer } from "@storybook/react";
import { Meta, StoryObj } from "@storybook/react";
import TopBar from "./TopBar.tsx";

import { storeFactory } from "@/state/store.ts";
import { Provider } from "react-redux";

const store = storeFactory({
  baseUrl: "/",
  sectionStorageType: "overrides",
  sectionStorage: "node.5",
});

export type _ReactRenderer = ReactRenderer;

const meta = {
  title: "Components/Ui/TopBar",
  component: TopBar,
  parameters: {
    layout: "fullscreen",
  },
  tags: ["autodocs"],
  decorators: [(story) => <Provider store={store}>{story()}</Provider>],
} satisfies Meta<typeof TopBar>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    title: "Demo layout builder",
  },
};
