import { storeFactory } from "@/state/store.ts";
import {
  dummyLayoutState,
  dummyLoadedLayout,
} from "@/state/__tests__/DummyLayoutState.ts";
import { act, render, screen, waitFor } from "@testing-library/react";
import { Provider } from "react-redux";
import { userEvent } from "@testing-library/user-event";
import TopBar from "@/components/Ui/TopBar/TopBar.tsx";
import { MODE_EDIT, MODE_SELECT } from "@/types/ui.types.ts";
import {
  loadOrSaveLayoutSuccess,
  updateLayoutSettings,
} from "@/state/Slices/Layout/layoutSlice.ts";
import {
  STATE_CLEAN,
  STATE_DIRTY,
  STATE_IDLE,
  STATE_SAVING,
} from "@/types/constants.ts";

const store = storeFactory(
  {
    baseUrl: "/",
    sectionStorageType: "overrides",
    sectionStorage: "node.5",
  },
  dummyLayoutState,
);

const user = userEvent.setup();

const setup = () => {
  return render(
    <Provider store={store}>
      <TopBar title={"Yo editor"} />
    </Provider>,
  );
};

describe("Top bar", () => {
  it("Should render", () => {
    const { container } = setup();
    expect(screen.getByText("Yo editor")).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });
  it("Should set the mode", async () => {
    setup();
    const mode = screen.getByRole("button", { name: "✏️" });
    expect(mode).toBeInTheDocument();
    expect(store.getState().ui.mode).toEqual(MODE_SELECT);
    await user.click(mode);
    expect(store.getState().ui.mode).toEqual(MODE_EDIT);
  });
  it("Should allow saving", async () => {
    setup();
    expect(store.getState().layout.state).toEqual(STATE_IDLE);
    act(() => {
      store.dispatch(loadOrSaveLayoutSuccess(dummyLoadedLayout));
    });
    expect(store.getState().layout.state).toEqual(STATE_CLEAN);
    expect(
      screen.queryByRole("button", { name: "💾" }),
    ).not.toBeInTheDocument();
    act(() => {
      store.dispatch(
        updateLayoutSettings({
          sectionId: "s1",
          layoutSettings: { label: "new label" },
        }),
      );
    });
    expect(store.getState().layout.state).toEqual(STATE_DIRTY);
    const save = await waitFor(() => {
      const el = screen.getByRole("button", { name: "💾" });
      expect(el).toBeInTheDocument();
      return el;
    });
    await user.click(save);
    expect(store.getState().layout.state).toEqual(STATE_SAVING);
    expect(
      screen.getByText("Saving changes", { exact: false }),
    ).toBeInTheDocument();
    await waitFor(() => {
      expect(
        screen.queryByRole("button", { name: "💾" }),
      ).not.toBeInTheDocument();
    });
    await waitFor(() => {
      expect(store.getState().layout.state).toEqual(STATE_CLEAN);
    });
  });
  it("Should open/close sidebars", async () => {
    setup();
    const insert = screen.getByRole("button", { name: "➕" });
    expect(store.getState().ui.startSidebarOpen).toEqual(false);
    await user.click(insert);
    expect(store.getState().ui.startSidebarOpen).toEqual(true);
    const settings = screen.getByRole("button", { name: "⚙️" });
    expect(store.getState().ui.endSidebarOpen).toEqual(false);
    await user.click(settings);
    expect(store.getState().ui.endSidebarOpen).toEqual(true);
  });
});
