import React from "react";
import { useDispatch } from "@/state/store.ts";
import { ActionCreatorWithPayload } from "@reduxjs/toolkit";

interface SidebarControlsProps {
  handler: ActionCreatorWithPayload<boolean>;
}

const SidebarControls: React.FC<SidebarControlsProps> = ({ handler }) => {
  const dispatch = useDispatch();
  const onClick = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault();
    dispatch(handler(false));
  };
  return (
    <div className={"dlb__sidebar-actions"}>
      <button onClick={onClick}>✖️</button>
    </div>
  );
};

export default SidebarControls;
