import type { ReactRenderer } from "@storybook/react";
import { Meta, StoryObj } from "@storybook/react";
import SidebarStart from "./SidebarStart.tsx";
import DragContext from "@/components/LayoutEditor/DragContext.tsx";
import { dummyLayoutState } from "@/state/__tests__/DummyLayoutState.ts";
import { storeFactory } from "@/state/store.ts";
import { Provider } from "react-redux";
import { setStartSidebarOpen } from "@/state/Slices/Ui/uiSlice.ts";

export type _ReactRenderer = ReactRenderer;

const store = storeFactory(
  {
    baseUrl: "/",
    sectionStorageType: "overrides",
    sectionStorage: "node.5",
  },
  dummyLayoutState,
);
store.dispatch(setStartSidebarOpen(true));

const meta = {
  title: "Components/Ui/SidebarStart",
  component: SidebarStart,
  parameters: {
    layout: "fullscreen",
  },
  decorators: [
    (story) => (
      <Provider store={store}>
        <DragContext>{story()}</DragContext>
      </Provider>
    ),
  ],
} satisfies Meta<typeof SidebarStart>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {},
};
