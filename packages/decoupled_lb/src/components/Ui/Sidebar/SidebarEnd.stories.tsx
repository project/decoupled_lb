import type { ReactRenderer } from "@storybook/react";
import { Meta, StoryObj } from "@storybook/react";
import SidebarEnd from "./SidebarEnd.tsx";
import DragContext from "@/components/LayoutEditor/DragContext.tsx";
import { dummyLayoutState } from "@/state/__tests__/DummyLayoutState.ts";
import { storeFactory } from "@/state/store.ts";
import { Provider } from "react-redux";
import {
  setEndSidebarOpen,
  setSelectedComponent,
} from "@/state/Slices/Ui/uiSlice.ts";
import BlockPluginProvider from "@/components/LayoutEditor/BlockPluginProvider.tsx";
import { blockPlugins } from "@/components/LayoutEditor/__tests__/mockPluginProviders.ts";

export type _ReactRenderer = ReactRenderer;

const store = storeFactory(
  {
    baseUrl: "/",
    sectionStorageType: "overrides",
    sectionStorage: "node.5",
  },
  dummyLayoutState,
);
store.dispatch(setEndSidebarOpen(true));
store.dispatch(setSelectedComponent({ uuid: "c1" }));

const meta = {
  title: "Components/Ui/SidebarEnd",
  component: SidebarEnd,
  parameters: {
    layout: "fullscreen",
  },
  decorators: [
    (story) => (
      <Provider store={store}>
        <BlockPluginProvider plugins={blockPlugins}>
          <DragContext>{story()}</DragContext>
        </BlockPluginProvider>
      </Provider>
    ),
  ],
} satisfies Meta<typeof SidebarEnd>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {},
};
