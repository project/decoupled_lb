import React from "react";
import "./sidebar.pcss";
import { useDispatch, useSelector } from "@/state/store.ts";
import {
  selectorUiState,
  setActiveSidebarStartPane,
  setStartSidebarOpen,
} from "@/state/Slices/Ui/uiSlice.ts";
import classNames from "classnames";
import Tabset from "@/components/Ui/Tabset/Tabset.tsx";
import {
  SIDEBAR_TAB_INSERT,
  SIDEBAR_TAB_OUTLINE,
  StartSidebarTab,
} from "@/types/ui.types.ts";
import SidebarControls from "@/components/Ui/Sidebar/SidebarControls.tsx";
import Outline from "@/components/Ui/Outline/Outline.tsx";
import Insert from "@/components/Ui/Insert/Insert.tsx";

const SidebarStart: React.FC = () => {
  const { startSidebarOpen, activeStartSidebarTab } =
    useSelector(selectorUiState);
  const dispatch = useDispatch();

  const selectTab = (activeTab: StartSidebarTab): void => {
    dispatch(setActiveSidebarStartPane(activeTab));
  };
  return (
    <div
      id={"dlb-sidebar-start"}
      className={classNames("dlb__sidebar", "dlb__sidebar--start", {
        "dlb__sidebar--open": startSidebarOpen,
      })}
    >
      {startSidebarOpen && (
        <>
          <SidebarControls handler={setStartSidebarOpen} />
          <Tabset
            tabs={[
              {
                label: "Insert",
                title: "Drag new components into the page",
                active: activeStartSidebarTab === SIDEBAR_TAB_INSERT,
                id: SIDEBAR_TAB_INSERT,
              },
              {
                label: "Outline",
                title: "View the outline of your built layout",
                active: activeStartSidebarTab === SIDEBAR_TAB_OUTLINE,
                id: SIDEBAR_TAB_OUTLINE,
              },
            ]}
            update={selectTab}
          >
            {activeStartSidebarTab === SIDEBAR_TAB_OUTLINE && <Outline />}
            {activeStartSidebarTab === SIDEBAR_TAB_INSERT && <Insert />}
          </Tabset>
        </>
      )}
    </div>
  );
};

export default SidebarStart;
