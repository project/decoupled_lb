import React from "react";
import "./sidebar.pcss";
import classNames from "classnames";
import { useDispatch, useSelector } from "@/state/store.ts";
import {
  selectorUiState,
  selectSelectedComponent,
  selectSelectedSection,
  setActiveSidebarEndPane,
  setEndSidebarOpen,
} from "@/state/Slices/Ui/uiSlice.ts";
import {
  EndSidebarTab,
  SIDEBAR_TAB_ENTITY,
  SIDEBAR_TAB_SETTINGS,
} from "@/types/ui.types.ts";
import Tabset from "@/components/Ui/Tabset/Tabset.tsx";
import SidebarControls from "@/components/Ui/Sidebar/SidebarControls.tsx";
import BlockSettings from "@/components/Ui/BlockSettings/BlockSettings.tsx";
import LayoutSettings from "@/components/Ui/LayoutSettings/LayoutSettings.tsx";
import { t } from "@/utilities/Translation.ts";

const SidebarEnd: React.FC = () => {
  const { endSidebarOpen, activeEndSidebarTab } = useSelector(selectorUiState);
  const dispatch = useDispatch();

  const selectTab = (activeTab: EndSidebarTab): void => {
    dispatch(setActiveSidebarEndPane(activeTab));
  };
  const blockSettings = <BlockSettings />;
  const sectionSettings = <LayoutSettings />;
  const selectedComponent = useSelector(selectSelectedComponent);
  const selectedSection = useSelector(selectSelectedSection);
  if (!selectedComponent && !selectedSection) {
    return (
      <div
        id={"dlb-sidebar-end"}
        className={classNames("dlb__sidebar", "dlb__sidebar--end", {
          "dlb__sidebar--open": endSidebarOpen,
        })}
      >
        {endSidebarOpen && (
          <>
            <SidebarControls handler={setEndSidebarOpen} />
            <div>{t("Select a section or block to configure")}</div>
          </>
        )}
      </div>
    );
  }
  if (!sectionSettings && !blockSettings) {
    return (
      <div
        id={"dlb-sidebar-end"}
        className={classNames("dlb__sidebar", "dlb__sidebar--end", {
          "dlb__sidebar--open": endSidebarOpen,
        })}
      >
        {endSidebarOpen && (
          <>
            <SidebarControls handler={setEndSidebarOpen} />
            <div>
              {t(
                "The selected block and section do not have configuration options",
              )}
            </div>
          </>
        )}
      </div>
    );
  }
  return (
    <div
      id={"dlb-sidebar-end"}
      className={classNames("dlb__sidebar", "dlb__sidebar--end", {
        "dlb__sidebar--open": endSidebarOpen,
      })}
    >
      {endSidebarOpen && (
        <>
          <SidebarControls handler={setEndSidebarOpen} />
          <Tabset
            tabs={[
              {
                label: "Layout",
                title: "Edit layout settings",
                active: activeEndSidebarTab === SIDEBAR_TAB_ENTITY,
                id: SIDEBAR_TAB_ENTITY,
              },
              {
                label: "Component",
                title: "Edit component settings",
                active: activeEndSidebarTab === SIDEBAR_TAB_SETTINGS,
                id: SIDEBAR_TAB_SETTINGS,
              },
            ]}
            update={selectTab}
          >
            {activeEndSidebarTab === SIDEBAR_TAB_ENTITY && sectionSettings}
            {activeEndSidebarTab === SIDEBAR_TAB_SETTINGS && blockSettings}
          </Tabset>
        </>
      )}
    </div>
  );
};

export default SidebarEnd;
