import React from "react";
import "./tabset.pcss";

export interface Tab<ValidTabIds> {
  id: ValidTabIds;
  label: string;
  title?: string;
  active: boolean;
}
export interface TabsetProps<ValidTabIds extends string> {
  /**
   * Active tab to render
   */
  children: React.ReactNode;
  /**
   * Array of tabs.
   */
  tabs: Array<Tab<ValidTabIds>>;
  /**
   * Callback to trigger update.
   */
  // eslint-disable-next-line no-unused-vars
  update: (activeTab: ValidTabIds) => void;
}

const Tabset = <ValidTabIds extends string>({
  tabs,
  children,
  update,
}: TabsetProps<ValidTabIds>): React.ReactNode => {
  const setActiveTab = (tabId: ValidTabIds) => {
    return (e: React.MouseEvent<HTMLElement>) => {
      e.preventDefault();
      update(tabId);
    };
  };
  return (
    <div className={"dlb-tabset"}>
      <nav className={"dlb-tabset__nav"}>
        <ul>
          {tabs.map((tab) => (
            <li key={tab.id}>
              <button
                aria-pressed={tab.active}
                title={tab.title}
                onClick={setActiveTab(tab.id)}
                data-tab-id={tab.id}
              >
                {tab.label}
              </button>
            </li>
          ))}
        </ul>
      </nav>
      <div className={"dlb-tabset__pane"}>{children}</div>
    </div>
  );
};

export default Tabset;
