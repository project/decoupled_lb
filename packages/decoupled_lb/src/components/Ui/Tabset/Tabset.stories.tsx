import { Meta, StoryObj } from "@storybook/react";
import Tabset from "./Tabset.tsx";

const meta = {
  title: "Components/Ui/Tabset",
  component: Tabset,
  parameters: {
    layout: "centered",
  },
  tags: ["autodocs"],
} satisfies Meta<typeof Tabset>;

export default meta;
type Story = StoryObj<typeof meta>;

// @todo refactor to make this interactive.
export const Default: Story = {
  args: {
    tabs: [
      {
        label: "Tab 1",
        title: "This is the title of tab 1",
        active: false,
        id: "tab-1",
      },
      {
        label: "Tab 2",
        title: "This is the title of tab 2",
        active: true,
        id: "tab-2",
      },
    ],
    update: () => {},
    children: <div>Tab 2 is active</div>,
  },
};
