import React from "react";
import "./footer.pcss";
import { RootState, useSelector } from "@/state/store.ts";
import { selectSelectedComponent } from "@/state/Slices/Ui/uiSlice.ts";
import {
  selectComponents,
  selectLayoutSections,
  selectRegions,
} from "@/state/Slices/Layout/layoutSlice.ts";
import { selectSectionById, useGetSectionListQuery } from "@/state/Slices/Api";

const Footer: React.FC = () => {
  const selectedComponent = useSelector(selectSelectedComponent);
  const sections = useSelector(selectLayoutSections);
  const components = useSelector(selectComponents);
  const regions = useSelector(selectRegions);
  const component = selectedComponent ? components[selectedComponent] : null;
  const region = component ? regions[component.regionId] : null;
  const section = region ? sections[region.section] : null;
  useGetSectionListQuery();
  const sectionInfo = useSelector((state: RootState) =>
    selectSectionById(state, section?.layout_id || "_none"),
  );
  if (!selectedComponent || !section || !region || !component) {
    return (
      <footer className={"dlb__footer"}>
        Click or press tab to select an item
      </footer>
    );
  }
  const description = `${
    section.layout_settings.label || `Section ${section.weight + 1}`
  } > ${sectionInfo?.region_labels[region.region] ?? region.region} > ${
    component.configuration.label || `Component ${component.uuid}`
  }`;
  return <footer className={"dlb__footer"}>{description}</footer>;
};

export default Footer;
