import React from "react";
import "../misc/settings.pcss";
import { useSelector } from "@/state/store.ts";
import { selectSelectedComponent } from "@/state/Slices/Ui/uiSlice.ts";
import { selectComponents } from "@/state/Slices/Layout/layoutSlice.ts";
import { useBlockPlugin } from "@/hooks/useBlockPlugin.tsx";

const BlockSettings: React.FC = () => {
  const selectedComponent = useSelector(selectSelectedComponent);
  const components = useSelector(selectComponents);
  const Block = useBlockPlugin(
    selectedComponent ? components[selectedComponent].configuration.id : null,
    true,
  );
  if (!selectedComponent || !Block || !Block.Settings) {
    return;
  }
  return <Block.Settings {...components[selectedComponent]} />;
};

export default BlockSettings;
