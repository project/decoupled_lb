import { storeFactory } from "@/state/store.ts";
import { dummyLayoutState } from "@/state/__tests__/DummyLayoutState.ts";
import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import { userEvent } from "@testing-library/user-event";
import BlockSettings from "@/components/Ui/BlockSettings/BlockSettings.tsx";
import { setSelectedComponent } from "@/state/Slices/Ui/uiSlice.ts";

const store = storeFactory(
  {
    baseUrl: "/",
    sectionStorageType: "overrides",
    sectionStorage: "node.5",
  },
  dummyLayoutState,
);
store.dispatch(setSelectedComponent({ uuid: "c1" }));

const user = userEvent.setup();

const setup = () => {
  return render(
    <Provider store={store}>
      <BlockSettings />
    </Provider>,
  );
};

describe("Block settings", () => {
  it("Should render", () => {
    const { container } = setup();
    expect(screen.getByRole("textbox", { name: "Label" })).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });
  it("Should set the display state", async () => {
    setup();
    const checkbox = screen.getByRole("checkbox", { name: "Display label" });
    expect(checkbox).toBeInTheDocument();
    expect(checkbox).toBeChecked();
    await user.click(checkbox);
    expect(checkbox).not.toBeChecked();
    expect(
      store.getState().layout.layout.components.c1.configuration.label_display,
    ).toEqual(false);
    await user.click(checkbox);
    expect(checkbox).toBeChecked();
    expect(
      store.getState().layout.layout.components.c1.configuration.label_display,
    ).toEqual(true);
  });
  it("Should set the label", async () => {
    setup();
    const input: HTMLInputElement = screen.getByRole("textbox", {
      name: "Label",
    });
    expect(input).toBeInTheDocument();
    expect(input.value).toEqual("c1");
    await user.clear(input);
    await user.type(input, "New label");
    expect(input.value).toEqual("New label");
    expect(
      store.getState().layout.layout.components.c1.configuration.label,
    ).toEqual("New label");
  });
});
