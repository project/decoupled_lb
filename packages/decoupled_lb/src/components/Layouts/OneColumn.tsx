import React from "react";
import Region from "../LayoutEditor/Region.tsx";
import { LayoutPluginProps } from "@/types/layout.types.ts";
import DefaultLayoutSettings from "@/components/LayoutEditor/DefaultLayoutSettings.tsx";

export const Preview: React.FC<LayoutPluginProps> = ({
  section,
  sectionProps,
  regions,
}) => {
  return (
    <div {...sectionProps} className={`section--${section.id} section`}>
      <Region regionId={regions.content.id} className="layout__region" />
    </div>
  );
};

export const Settings = DefaultLayoutSettings;
export const id = "layout_onecol";
