import React from "react";
import Region from "../LayoutEditor/Region.tsx";
import { LayoutPluginProps } from "@/types/layout.types.ts";
import DefaultLayoutSettings from "@/components/LayoutEditor/DefaultLayoutSettings.tsx";

export const Preview: React.FC<LayoutPluginProps> = ({
  section,
  sectionProps,
  regions,
}) => {
  return (
    <div {...sectionProps} className={`section--${section.id}`}>
      The section with weight {section.weight} is using the layout plugin{" "}
      {section.layout_id} but there is no React component for that layout. Using
      fallback instead.
      {Object.keys(regions).map((key) => (
        <Region key={key} regionId={regions[key].id} />
      ))}
    </div>
  );
};

export const Settings = DefaultLayoutSettings;
export const id = "fallback";
