import React from "react";
import Region from "../LayoutEditor/Region.tsx";
import { LayoutPluginProps } from "@/types/layout.types.ts";
import "./two-column.pcss";
import DefaultLayoutSettings from "@/components/LayoutEditor/DefaultLayoutSettings.tsx";
export const Preview: React.FC<LayoutPluginProps> = ({
  section,
  sectionProps,
  regions,
}) => {
  return (
    <div {...sectionProps} className={`section section--${section.layout_id}`}>
      <Region
        regionId={regions.first.id}
        className="layout__region layout__region--first"
      />
      <Region
        regionId={regions.second.id}
        as={"aside"}
        className="layout__region layout__region--second"
      />
    </div>
  );
};

export const Settings = DefaultLayoutSettings;

export const id = "layout_twocol_section";
