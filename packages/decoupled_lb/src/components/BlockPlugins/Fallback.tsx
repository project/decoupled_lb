import React from "react";

import { LayoutBlock } from "@/types/block.types.ts";
import DefaultBlockSettings from "@/components/LayoutEditor/DefaultBlockSettings.tsx";

export const Edit: React.FC<LayoutBlock> = ({ fallback }) => {
  return <div dangerouslySetInnerHTML={{ __html: fallback || "" }} />;
};

export const Preview = Edit;

export const Settings = DefaultBlockSettings;
export const id = "fallback";
