import React, { useMemo } from "react";
import { useDispatch } from "@/state/store.ts";
import Formatter from "@/components/LayoutEditor/Formatter.tsx";
import { LayoutBlock, BlockConfiguration } from "@/types/block.types.ts";
import { FieldValues } from "@/types/field.types.ts";
import Widget from "@/components/LayoutEditor/Widget.tsx";
import * as BlockTitle from "@/components/Ui/BlockTitle.tsx";
import { updateBlock } from "@/state/Slices/Layout/layoutSlice.ts";
import DefaultBlockSettings from "@/components/LayoutEditor/DefaultBlockSettings.tsx";
import { useGetDisplayPluginQuery } from "@/state/Slices/Api";
import { decodeError } from "@/utilities/ErrorHelpers.ts";
import Display from "@/utilities/Displays/Display.ts";

interface InlineBlockConfiguration extends BlockConfiguration {
  view_mode: string;
  block_revision_id: string | number;
  block_serialized: Record<string, Array<FieldValues>>;
  dirty: Array<string>;
  form_mode_uuid: string;
  view_mode_uuid: string;
}

export const Preview: React.FC<LayoutBlock<InlineBlockConfiguration>> = (
  component,
) => {
  const [, bundle] = component.configuration.id.split(":");
  const displayIdentifiers = useMemo(
    () => ({
      entityType: "block_content",
      bundle,
      viewMode: component.configuration.view_mode_uuid,
      type: "view" as const,
    }),
    [bundle, component.configuration.view_mode_uuid],
  );
  const {
    error,
    data: displayData,
    isLoading,
  } = useGetDisplayPluginQuery(displayIdentifiers);
  if (error) {
    return (
      <div>
        An error occurred fetching display settings{" "}
        {decodeError(error, "Could not load entity view display")}
      </div>
    );
  }
  if (isLoading) {
    return <div>loading view display...In preview {component.uuid}</div>;
  }
  if (displayData) {
    const display = new Display(displayData);
    const sortedComponents = display.getSortedComponents();

    return (
      <div>
        <BlockTitle.Preview {...component.configuration} />
        {Object.entries(sortedComponents)
          .filter(
            ([name]) =>
              Array.isArray(component.configuration.block_serialized[name]) &&
              component.configuration.block_serialized[name].length,
          )
          .map(([name, configuration]) => {
            return (
              <Formatter
                fieldName={name}
                key={name}
                itemValues={component.configuration.block_serialized[name]}
                formatter={configuration}
              />
            );
          })}
      </div>
    );
  }
};

export const Edit: React.FC<LayoutBlock<InlineBlockConfiguration>> = (
  component,
) => {
  const [, bundle] = component.configuration.id.split(":");
  const dispatch = useDispatch();
  const displayIdentifiers = useMemo(
    () => ({
      entityType: "block_content",
      bundle,
      // See \Drupal\layout_builder\Plugin\Block\InlineBlock::processBlockForm
      viewMode: component.configuration.form_mode_uuid,
      type: "form" as const,
    }),
    [bundle, component.configuration.form_mode_uuid],
  );
  const {
    error,
    data: displayData,
    isLoading,
  } = useGetDisplayPluginQuery(displayIdentifiers);
  if (error) {
    return (
      <div>
        An error occurred fetching display settings{" "}
        {decodeError(error, "Could not load entity form display")}
      </div>
    );
  }
  if (isLoading) {
    return <div>loading form display...In preview {component.uuid}</div>;
  }
  if (displayData) {
    const display = new Display(displayData);
    const sortedComponents = display.getSortedComponents();
    return (
      <div>
        <BlockTitle.Edit {...component} />
        {Object.entries(sortedComponents).map(([name, configuration]) => {
          const widget = (
            <Widget
              fieldName={name}
              key={name}
              update={(values) => {
                dispatch(
                  updateBlock({
                    componentId: component.uuid,
                    values: {
                      ...component,
                      configuration: {
                        ...component.configuration,
                        block_serialized: {
                          ...component.configuration.block_serialized,
                          [name]: values,
                        },
                      },
                    },
                  }),
                );
              }}
              itemValues={component.configuration.block_serialized[name]}
              widget={configuration}
            />
          );
          if (widget) {
            return widget;
          }
          return (
            <Formatter
              fieldName={name}
              key={name}
              itemValues={component.configuration.block_serialized[name]}
              formatter={configuration}
            />
          );
        })}
      </div>
    );
  }
  return <Preview {...component} />;
};

export const Settings = DefaultBlockSettings;
export const id = "inline_block";
