import { CKEditor } from "@ckeditor/ckeditor5-react";
import { InlineEditor } from "@ckeditor/ckeditor5-editor-inline";
import { Essentials } from "@ckeditor/ckeditor5-essentials";
import { Bold, Italic } from "@ckeditor/ckeditor5-basic-styles";
import { Paragraph } from "@ckeditor/ckeditor5-paragraph";

const editorConfiguration = {
  plugins: [Essentials, Bold, Italic, Paragraph],
  toolbar: ["bold", "italic"],
};

import React from "react";
import { EventInfo } from "@ckeditor/ckeditor5-utils";

interface EditorProps {
  data: string;
  // eslint-disable-next-line no-unused-vars
  onChange: (event: EventInfo, editor: InlineEditor) => void;
  id?: number;
}

const Editor: React.FC<EditorProps> = ({ data, onChange, id }) => {
  return (
    <CKEditor
      editor={InlineEditor}
      key={id}
      config={editorConfiguration}
      data={data}
      onChange={onChange}
    />
  );
};

export default Editor;
