import React from "react";
import Field from "../Theme/Field.tsx";

import { FormatterProps } from "@/types/formatter.types.ts";

export const Formatter: React.FC<
  FormatterProps<{ value: string; format: string; processed: string }>
> = ({ itemValues }) => {
  return (
    <Field
      items={itemValues.map((item) => ({
        // This is sanitized by Drupal.
        content: <span dangerouslySetInnerHTML={{ __html: item.processed }} />,
      }))}
    />
  );
};

export const id = "text_default";
