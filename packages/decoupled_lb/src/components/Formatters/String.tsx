import React from "react";
import Field from "../Theme/Field.tsx";

import { FormatterProps } from "@/types/formatter.types.ts";

export const Formatter: React.FC<FormatterProps<{ value: string }>> = ({
  itemValues,
}) => {
  return (
    <Field
      items={itemValues.map((item) => ({
        content: item.value,
      }))}
    />
  );
};

export const id = "string";
