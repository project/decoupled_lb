import React from "react";
import { FormatterProps } from "@/types/formatter.types.ts";

export const Formatter: React.FC<FormatterProps<{ value: string }>> = ({
  fieldName,
  formatter,
}) => {
  return (
    <div className="fallback">
      The {fieldName} field is using the {formatter.type} formatter that doesn't
      have a React version. So is being rendered by a fallback.
    </div>
  );
};

export const id = "fallback";
