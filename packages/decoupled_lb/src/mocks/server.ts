import { setupServer } from "msw/node";
import { handlers } from "./handlers";
import type { SetupServer } from "msw/node";
export type _SetupServer = SetupServer;

// This configures a request mocking server with the given request handlers.
export const server = setupServer(...handlers);
