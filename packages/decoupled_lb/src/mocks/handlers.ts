import { rest } from "msw";
import blocks from "./fixtures/blocks.json";
import layout from "./fixtures/layout.json";
import sections from "./fixtures/sections.json";
import entityViewDisplays from "./fixtures/entityViewDisplays.json";
import entityFormDisplays from "./fixtures/entityFormDisplays.json";

export const handlers = [
  rest.get(/\/api\/v1\/layout-builder\/layout\/.*/, (_req, res, ctx) => {
    return res(ctx.status(200), ctx.json(layout));
  }),
  rest.get(/\/api\/v1\/layout-builder\/blocks\/.*/, (_req, res, ctx) => {
    return res(ctx.status(200), ctx.json(blocks));
  }),
  rest.get(/\/api\/v1\/layout-builder\/sections\/.*/, (_req, res, ctx) => {
    return res(ctx.status(200), ctx.json(sections));
  }),
  rest.get(
    /\/jsonapi\/entity_view_display\/entity_view_display.*/,
    (_req, res, ctx) => {
      return res(ctx.status(200), ctx.json(entityViewDisplays));
    },
  ),
  rest.get(
    /\/jsonapi\/entity_form_display\/entity_form_display.*/,
    (_req, res, ctx) => {
      return res(ctx.status(200), ctx.json(entityFormDisplays));
    },
  ),
  rest.get("/session/token", (_req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.text("f59e6489-0181-4010-ae0a-df7028317a16"),
    );
  }),
  rest.put(/\/api\/v1\/layout-builder\/layout\/.*/, async (req, res, ctx) => {
    return res(ctx.delay(500), ctx.status(200), ctx.json(await req.json()));
  }),
];
