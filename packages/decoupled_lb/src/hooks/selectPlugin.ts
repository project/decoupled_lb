export const selectPlugin = <PluginType>(
  pluginId: string,
  context: Record<string, () => Promise<PluginType>>,
): Promise<PluginType> => {
  const candidates = [pluginId];
  if (pluginId.includes(":")) {
    const [basePluginId] = pluginId.split(":");
    candidates.push(basePluginId);
  }
  while (candidates.length) {
    const candidatePlugin = candidates.shift();
    if (candidatePlugin && context[candidatePlugin]) {
      return context[candidatePlugin]();
    }
  }
  throw new Error(
    `There is no such plugin ${pluginId}, valid options are ${Object.keys(
      context,
    ).join(", ")}`,
  );
};
