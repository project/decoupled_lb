import { useContext } from "react";
import { FormatterPluginContext } from "@/components/LayoutEditor/FormatterPluginProvider.tsx";
import { selectPlugin } from "./selectPlugin.ts";
import { FieldValues } from "@/types/field.types.ts";
import {
  AsyncFormatterPlugin,
  FormatterPlugin,
} from "@/types/formatter.types.ts";

import * as Fallback from "@/components/Formatters/Fallback.tsx";

export const useFormatterPlugin = (
  pluginId: string,
  withFallback: boolean = false,
): AsyncFormatterPlugin<FieldValues> => {
  const context = useContext(FormatterPluginContext);
  if (pluginId && pluginId in context.resolved) {
    return context.resolved[pluginId];
  }
  try {
    selectPlugin<FormatterPlugin<FieldValues>>(pluginId, context.promises).then(
      (plugin) => {
        context.resolved[pluginId] = plugin;
        context.updatePlugins(context);
      },
    );
  } catch (e) {
    if (withFallback) {
      return Fallback;
    }
    throw e;
  }
  return null;
};
