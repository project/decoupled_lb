import { useContext } from "react";
import { BlockPluginContext } from "@/components/LayoutEditor/BlockPluginProvider.tsx";

import * as Fallback from "@/components/BlockPlugins/Fallback.tsx";
import { selectPlugin } from "./selectPlugin.ts";
import { AsyncBlockPlugin, BlockPlugin } from "@/types/block.types.ts";

export const useBlockPlugin = (
  pluginId: string | null,
  withFallback: boolean = false,
): AsyncBlockPlugin => {
  const context = useContext(BlockPluginContext);
  if (pluginId === null) {
    return null;
  }
  if (pluginId && pluginId in context.resolved) {
    return context.resolved[pluginId];
  }
  try {
    selectPlugin<BlockPlugin>(pluginId as string, context.promises).then(
      (plugin) => {
        context.resolved[pluginId] = plugin;
        context.updatePlugins(context);
      },
    );
  } catch (e) {
    if (withFallback) {
      return Fallback;
    }
    throw e;
  }
  return null;
};
