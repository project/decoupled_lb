import { useContext } from "react";
import { selectPlugin } from "./selectPlugin.ts";
import { FieldValues } from "@/types/field.types.ts";
import { AsyncWidgetPlugin, WidgetPlugin } from "@/types/widget.types.ts";
import { WidgetPluginContext } from "@/components/LayoutEditor/WidgetPluginProvider.tsx";
import * as Fallback from "@/components/Widgets/Fallback.tsx";

export const useWidgetPlugin = (
  pluginId: string,
  withFallback: boolean = false,
): AsyncWidgetPlugin<FieldValues> => {
  const context = useContext(WidgetPluginContext);
  if (pluginId && pluginId in context.resolved) {
    return context.resolved[pluginId];
  }
  try {
    selectPlugin<WidgetPlugin<FieldValues>>(pluginId, context.promises).then(
      (plugin) => {
        context.resolved[pluginId] = plugin;
        context.updatePlugins(context);
      },
    );
  } catch (e) {
    if (withFallback) {
      return Fallback;
    }
    throw e;
  }

  return null;
};
