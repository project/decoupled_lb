import { useContext } from "react";
import { LayoutPluginContext } from "@/components/LayoutEditor/LayoutPluginProvider.tsx";
import { selectPlugin } from "./selectPlugin.ts";
import { AsyncLayoutPlugin, LayoutPlugin } from "@/types/layout.types.ts";
import * as Fallback from "@/components/Layouts/Fallback.tsx";

export const useLayoutPlugin = (
  pluginId: string | null,
  withFallback: boolean = false,
): AsyncLayoutPlugin => {
  const context = useContext(LayoutPluginContext);
  if (pluginId === null) {
    return null;
  }
  if (pluginId && pluginId in context.resolved) {
    return context.resolved[pluginId];
  }
  try {
    selectPlugin<LayoutPlugin>(pluginId, context.promises).then((plugin) => {
      context.resolved[pluginId] = plugin;
      context.updatePlugins(context);
    });
  } catch (e) {
    if (withFallback) {
      return Fallback;
    }
    throw e;
  }
  return null;
};
