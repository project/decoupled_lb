import { useContext } from "react";
import { RegionHoverContext } from "@/components/LayoutEditor/DragContext.tsx";

export const useRegionContext = () => useContext(RegionHoverContext);
