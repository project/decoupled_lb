import { FieldValues } from "@/types/field.types.ts";
import {
  DisplayConfiguration,
  DisplaySettings,
} from "@/types/displayMode.types.ts";
import { ReactNode } from "react";

/**
 * Props passed to a widget plugin.
 */
export interface WidgetProps<
  ValueType extends FieldValues,
  WidgetSettingsType extends DisplayConfiguration = DisplayConfiguration,
> {
  /**
   * Field values. Pass a more specific instance of FieldValues to type values.
   */
  itemValues: Array<ValueType>;
  /**
   * Widget settings.
   */
  widget: DisplaySettings<WidgetSettingsType>;
  /**
   * The field name being rendered.
   */
  fieldName: string;
  /**
   * Handler for updating values in the widget.
   */
  // eslint-disable-next-line no-unused-vars
  update: (values: Array<ValueType | FieldValues>) => void;
}

/**
 * Represents a widget plugin.
 */
export interface WidgetPlugin<
  ValueType extends FieldValues,
  WidgetSettingsType extends DisplayConfiguration = DisplayConfiguration,
> {
  /**
   * Returns the component that provides the widget output.
   *
   * @param props
   *   Widget props.
   */
  // eslint-disable-next-line no-unused-vars
  Widget(props: WidgetProps<ValueType, WidgetSettingsType>): ReactNode;
}

/**
 * Represents a widget once loaded or NULL in the meantime.
 */
export type AsyncWidgetPlugin<
  ValueType extends FieldValues,
  WidgetSettingsType extends DisplayConfiguration = DisplayConfiguration,
> = WidgetPlugin<ValueType, WidgetSettingsType> | null;
