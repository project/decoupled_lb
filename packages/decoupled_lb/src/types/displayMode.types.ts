export type DisplayConfiguration = Record<string, unknown>;

/**
 * Represents formatter or widget settings.
 */
export interface DisplaySettings<
  FormatterSettingsType extends DisplayConfiguration = DisplayConfiguration,
> {
  /**
   * The formatter or widget plugin ID.
   */
  type: string;
  /**
   * How to display the label.
   *
   * Not present in entity form displays.
   */
  label?: "hidden" | "above" | "visually_hidden" | "inline";
  /**
   * Additional settings from third-party modules.
   */
  third_party_settings: Record<string, unknown>;
  /**
   * Formatter or widget settings.
   */
  settings: FormatterSettingsType;
  /**
   * Weight of the field relative to its display.
   */
  weight: number;
  /**
   * Region to display item in.
   */
  region: "content";
}

/**
 * Describes and entity view or form display.
 */
export interface DisplayInterface {
  /**
   * Gets the component by name.
   *
   * @param name
   *   Component name.
   */
  getComponent(
    // eslint-disable-next-line no-unused-vars
    name: string,
  ): DisplaySettings | false;

  /**
   * Gets all components, sorted by weight.
   */
  getSortedComponents(): Record<string, DisplaySettings>;
}

export type DisplayType = "form" | "view";

export interface DisplayData extends TypedDisplayIdentifiers {
  components: Record<string, DisplaySettings>;
}

export interface DisplayIdentifiers {
  entityType: string;
  bundle: string;
  viewMode: string;
}

/**
 * Extension of display identifiers that includes the type.
 */
export interface TypedDisplayIdentifiers extends DisplayIdentifiers {
  type: DisplayType;
}

export interface DisplayResponse {
  data: {
    id: string;
    type: string;
    attributes: {
      content: Record<string, DisplaySettings>;
    };
  };
}
