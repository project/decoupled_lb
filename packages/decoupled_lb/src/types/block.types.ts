import { ReactNode } from "react";
import { Mapping } from "./constants.ts";

/**
 * Represents block configuration options.
 */
export interface BlockConfiguration {
  /**
   * Block plugin ID.
   */
  id: string;
  /**
   * TRUE to display the label.
   */
  label_display: boolean;
  /**
   * Label to display.
   */
  label: string | null;
  /**
   * Context mapping data.
   */
  context_mapping?: Record<string, string>;
}

/**
 * A common base interface for block objects.
 */
export interface BaseBlock<
  ConfigurationType extends BlockConfiguration = BlockConfiguration,
  AdditionalPropertiesType extends Mapping = Mapping,
> {
  /**
   * Component UUID.
   */
  uuid: string;
  /**
   * Component configuration.
   */
  configuration: ConfigurationType;
  /**
   * Component weight.
   */
  weight: number;
  /**
   * Additional configuration.
   */
  additional: AdditionalPropertiesType;
  /**
   * Fallback representation of the rendered block
   */
  fallback?: string;
}

/**
 * Internal representation of a block with unique region ID.
 */
export interface LayoutBlock<
  ConfigurationType extends BlockConfiguration = BlockConfiguration,
  AdditionalPropertiesType extends Mapping = Mapping,
> extends BaseBlock<ConfigurationType, AdditionalPropertiesType> {
  /**
   * Unique region ID. This is created during normalization.
   *
   * Used to uniquely identify a region across sections.
   *
   * @see {import('layout.types.ts')}.Region
   */
  regionId: string;
}

/**
 * Block received from Drupal's API.
 */
export interface ReceivedBlock<
  ConfigurationType extends BlockConfiguration = BlockConfiguration,
  AdditionalPropertiesType extends Mapping = Mapping,
> extends BaseBlock<ConfigurationType, AdditionalPropertiesType> {
  /**
   * The region identifier. These are strings like 'col1' or 'content'.
   */
  region: string;
}

/**
 * Describes a block plugin.
 */
export interface BlockPlugin {
  /**
   * Component callback to render the block in edit mode.
   *
   * @param block
   *   The block configuration.
   */
  // eslint-disable-next-line no-unused-vars
  Edit(block: LayoutBlock): ReactNode;

  /**
   * Component callback to render the block in preview mode.
   * @param block
   *   The block configuration
   */
  // eslint-disable-next-line no-unused-vars
  Preview(block: LayoutBlock): ReactNode;

  /**
   * Render settings component.
   * @param block
   *   The block configuration
   */
  // eslint-disable-next-line no-unused-vars
  Settings?(block: LayoutBlock): ReactNode;
}

/**
 * A loaded block plugin or null whilst it is loading.
 */
export type AsyncBlockPlugin = BlockPlugin | null;

/**
 * Block restriction.
 */
export interface Restriction {
  /**
   * Section layout plugin ID.
   */
  layout_id: string;
  /**
   * Array of region IDs allowed for this section.
   */
  regions: Array<string>;
}

/**
 * Information about available blocks.
 */
export interface BlockInfo {
  /**
   * Block plugin ID.
   */
  id: string;
  /**
   * Block label.
   */
  label: string;
  /**
   * Block category.
   */
  category: string;
  /**
   * Block icon either SVG markup or a URL.
   */
  icon: string | null;
  /**
   * TRUE if the icon is a url.
   */
  icon_is_url: boolean;
  /**
   * Default configuration for a block
   */
  default_configuration: BlockConfiguration;
  /**
   * Default layout ID.
   */
  default_layout: string;
  /**
   * Default region.
   */
  default_region: string;
  /**
   * Restrictions.
   */
  section_restrictions: Array<Restriction>;
}

/**
 * Shape of block list data.
 */
export interface BlockList {
  /**
   * Array of block info keyed by block ID.
   */
  blocks: Record<string, BlockInfo>;
}

/**
 * Section information.
 */
export interface SectionInfo {
  /**
   * Section plugin ID.
   */
  id: string;
  /**
   * Section label.
   */
  label: string;
  /**
   * Section icon either SVG markup or a URL.
   */
  icon: string | null;
  /**
   * TRUE if the icon is a url.
   */
  icon_is_url: boolean;
  /**
   * Region labels.
   */
  region_labels: Record<string, string>;
}

/**
 * Shape of section list data.
 */
export interface SectionList {
  sections: Record<string, SectionInfo>;
}
