export interface PluginCollection<PluginType> {
  /**
   * Resolved plugins.
   */
  resolved: Record<string, PluginType>;
  /**
   * Unresolved plugins.
   */
  promises: Record<string, () => Promise<PluginType>>;
  /**
   * Plugin updater.
   */
  // eslint-disable-next-line no-unused-vars
  updatePlugins: (value: PluginCollection<PluginType>) => void;
}
