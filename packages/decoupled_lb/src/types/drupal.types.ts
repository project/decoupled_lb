/**
 * Tracks overarching Drupal state.
 */
export interface DrupalState {
  /**
   * Base URL to the Drupal site. Includes a trailing slash.
   */
  baseUrl: string;
}
