/**
 * Represents field data from Drupal.
 */
export interface FieldValues {
  /**
   * Any value provided the key is a string.
   */
  [index: string]: number | string | boolean | FieldValues;
}
