import { FieldValues } from "./field.types.ts";
import { ReactNode } from "react";
import {
  DisplayConfiguration,
  DisplaySettings,
} from "@/types/displayMode.types.ts";

/**
 * Props passed to a formatter plugin.
 */
export interface FormatterProps<
  ValueType extends FieldValues,
  FormatterSettingsType extends DisplayConfiguration = DisplayConfiguration,
> {
  /**
   * Field values. Pass a more specific instance of FieldValues to type values.
   */
  itemValues: Array<ValueType>;
  /**
   * Formatter settings.
   */
  formatter: DisplaySettings<FormatterSettingsType>;
  /**
   * The field name being rendered.
   */
  fieldName: string;
}

/**
 * Represents a formatter plugin.
 */
export interface FormatterPlugin<
  ValueType extends FieldValues,
  FormatterSettingsType extends DisplayConfiguration = DisplayConfiguration,
> {
  /**
   * Returns the component that provides the formatter output.
   *
   * @param props
   *   Formatter props.
   */
  // eslint-disable-next-line no-unused-vars
  Formatter(props: FormatterProps<ValueType, FormatterSettingsType>): ReactNode;
}

/**
 * Represents a formatter once loaded or NULL in the meantime.
 */
export type AsyncFormatterPlugin<
  ValueType extends FieldValues,
  FormatterSettingsType extends DisplayConfiguration = DisplayConfiguration,
> = FormatterPlugin<ValueType, FormatterSettingsType> | null;
