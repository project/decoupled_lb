/**
 * Represents an error object
 */
export interface Error {
  /**
   * The error message.
   */
  message: string;
  /**
   * A unique identifier for the error such as a field name.
   */
  identifier?: string;
}

/**
 * A set of errors keyed by a unique identifier.
 */
export type ErrorSet = Record<string, Error>;
