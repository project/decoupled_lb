import { FieldValues } from "./field.types.ts";

export const STATE_IDLE = "idle";
export const STATE_LOADING = "loading";
export const STATE_ERROR = "error";
export const STATE_CLEAN = "clean";
export const STATE_SAVING = "saving";
export const STATE_DIRTY = "dirty";

export type LoadingState =
  | typeof STATE_IDLE
  | typeof STATE_LOADING
  | typeof STATE_ERROR
  | typeof STATE_CLEAN
  | typeof STATE_SAVING
  | typeof STATE_DIRTY;

/**
 * Utility data structure that maps to Drupal's random array soup.
 */
export interface Mapping {
  /**
   * Any value as long as the key is a string.
   */
  [index: string]:
    | number
    | string
    | boolean
    | Array<Record<string, FieldValues>>
    | Mapping;
}

/**
 * An object with an index.
 */
export interface Indexed {
  /**
   * The item's index.
   */
  index: number;
}
