import { ErrorSet } from "./errorSet.ts";
import { LayoutBlock, ReceivedBlock } from "./block.types.ts";
import { LoadingState } from "./constants.ts";
import {
  DraggableProvidedDraggableProps,
  DraggableProvidedDragHandleProps,
} from "react-beautiful-dnd";
import { DetailedHTMLProps, HTMLAttributes, ReactNode } from "react";

/**
 * Represents a minimum set of configuration for a layout (section) plugin.
 */
export interface LayoutSettings {
  /**
   * Optional label.
   */
  label?: string;
}

/**
 * A base interface for layout plugins (sections).
 */
export interface BaseSection<
  LayoutSettingsType extends LayoutSettings = LayoutSettings,
> {
  /**
   * Layout plugin ID. e.g. 'two_col'.
   */
  layout_id: string;
  /**
   * Layout settings.
   */
  layout_settings: LayoutSettingsType;
}

/**
 * Represents section configuration as returned from the Drupal API.
 */
export interface ReceivedSection<
  LayoutSettingsType extends LayoutSettings = LayoutSettings,
> extends BaseSection<LayoutSettingsType> {
  /**
   * Map of components (blocks) keyed by UUID.
   */
  components: Record<string, ReceivedBlock>;
  third_party_settings: {
    decoupled_lb_api: {
      uuid: string;
      region_uuids: Record<string, string>;
    };
    [index: string]: Record<string, unknown>;
  };
}

/**
 * Represents a normalized section with a unique ID and weight for sorting.
 */
export interface LayoutSection<
  LayoutSettingsType extends LayoutSettings = LayoutSettings,
> extends BaseSection<LayoutSettingsType> {
  /**
   * Section weight, used for ordering in drag and drop.
   */
  weight: number;
  /**
   * Unique section ID, created during normalization. Used to uniquely identify
   * a section and persists after re-ordering unlike the 'delta' used by Drupal.
   */
  id: string;
  third_party_settings?: {
    [index: string]: Record<string, unknown>;
  };
}

/**
 * Normalized representation of a region.
 */
export interface LayoutRegion {
  /**
   * Unique region ID, created during normalization. Used to uniquely identify
   * a region to simplify drag and drop operations.
   */
  id: string;
  /**
   * String region machine name as used by Drupal. e.g. 'col1'.
   */
  region: string;
  /**
   * Reference to the section ID which contains this region.
   *
   * @see LayoutSection
   */
  section: string;
}

/**
 * Normalized representation of layout received from Drupal APIs.
 */
export interface NormalizedLayout {
  /**
   * Sections keyed by unique section IDs.
   */
  sections: Record<string, LayoutSection>;
  /**
   * Regions keyed by unique region IDs.
   */
  regions: Record<string, LayoutRegion>;
  /**
   * Components (blocks) keyed by Drupal UUIDs.
   */
  components: Record<string, LayoutBlock>;
}

/**
 * Current state of the layout.
 */
export interface LayoutState {
  /**
   * Any errors that occurred during loading the layout from Drupal's APIs.
   */
  errors: ErrorSet;
  /**
   * Section storage type, e.g. 'overrides' or 'defaults'
   */
  sectionStorageType: string;
  /**
   * Section storage identifier. e.g. node.5 for overrides or node.basic.default
   * for a default layout.
   */
  sectionStorage: string;
  /**
   * Representation of the layout.
   */
  layout: NormalizedLayout;
  /**
   * Loading state.
   */
  state: LoadingState;
}

/**
 * Representation of the layout as received from Drupal's APIs.
 */
export interface ReceivedLayout {
  /**
   * Array of sections. The order is the section delta which we normalize to
   * weights.
   */
  sections: Array<ReceivedSection>;
}

/**
 * Properties of a section.
 */
interface SectionProps
  extends Partial<
    DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>
  > {
  /**
   * Internal react ref that should be attached to the element.
   *
   * @param element
   *   Current ref if set.
   */
  // eslint-disable-next-line no-unused-vars
  ref: (element: HTMLElement | null) => void;
}

/**
 * Props provided to a layout plugin.
 */
export type LayoutPluginProps = {
  /**
   * Current section which this layout represents.
   */
  section: LayoutSection;
  /**
   * Properties to apply to the section's outer element.
   */
  sectionProps: SectionProps &
    Partial<DraggableProvidedDraggableProps> &
    Partial<DraggableProvidedDragHandleProps> & { "data-selected"?: boolean };
  /**
   * Map of regions keyed by region name. e.g. 'col1',
   */
  regions: Record<string, LayoutRegion>;
};

/**
 * Represents a layout plugin.
 */
export interface LayoutPlugin {
  /**
   * Component callback to render the layout in preview mode.
   *
   * @param section
   *   The section data.
   *
   * @constructor
   */
  // eslint-disable-next-line no-unused-vars
  Preview(section: LayoutPluginProps): ReactNode;

  /**
   * Component callback to render the layout settings.
   *
   * @param section
   *   The section data.
   * @constructor
   */
  // eslint-disable-next-line no-unused-vars
  Settings?(section: LayoutSection): ReactNode;
}

/**
 * Represents a layout plugin once loaded and NULL in the meantime.
 */
export type AsyncLayoutPlugin = LayoutPlugin | null;

/**
 * A set of unique identifiers for a layout.
 */
export interface LayoutIdentifiers {
  /**
   * Base URL from which to fetch the layout over an API.
   */
  baseUrl: string;
  /**
   * Section storage type, e.g. 'overrides' or 'defaults'
   */
  sectionStorageType: string;
  /**
   * Section storage identifier. e.g. node.5 for overrides or node.basic.default
   * for a default layout.
   */
  sectionStorage: string;
}
