import React from "react";
import { LayoutBlock, BlockInfo, SectionInfo } from "@/types/block.types.ts";
import { LayoutSection } from "@/types/layout.types.ts";

/**
 * A generic interface for a menu link/item callback.
 */
interface Action {
  /**
   * Title of the button.
   */
  title: string;
  /**
   * Callback to trigger when the button is clicked.
   *
   * @param event
   *   Click event.
   */
  // eslint-disable-next-line no-unused-vars
  action: (event: React.PointerEvent<HTMLElement>) => void;
}

/**
 * Mode constants.
 */
export const MODE_EDIT = "edit";
export const MODE_SELECT = "select";

/**
 * Sidebar tab constants.
 */
export const SIDEBAR_TAB_ENTITY = "entity";
export const SIDEBAR_TAB_SETTINGS = "settings";
export const SIDEBAR_TAB_INSERT = "insert";
export const SIDEBAR_TAB_OUTLINE = "outline";

/**
 * Drag state constants
 */
export const DRAG_SECTION = "section";
export const DRAG_BLOCK = "block";
export const DRAG_OUTLINE_BLOCK = "outline-block";
export const DRAG_OUTLINE_SECTION = "outline-section";

export const DRAG_INSERT_BLOCK = "insert-block";
export const DRAG_INSERT_SECTION = "insert-section";

export type Mode = typeof MODE_EDIT | typeof MODE_SELECT;
export type ActiveDragBlock = { type: typeof DRAG_BLOCK; info: LayoutBlock };
export type ActiveDragSection = {
  type: typeof DRAG_SECTION;
  info: LayoutSection;
};
export type ActiveDragInsertBlock = {
  type: typeof DRAG_INSERT_BLOCK;
  info: BlockInfo;
};
export type ActiveDragInsertSection = {
  type: typeof DRAG_INSERT_SECTION;
  info: SectionInfo;
};
export type ActiveDragOutlineBlock = {
  type: typeof DRAG_OUTLINE_BLOCK;
  info: LayoutBlock;
};
export type ActiveDragOutlineSection = {
  type: typeof DRAG_OUTLINE_SECTION;
  info: LayoutSection;
};
export type ActiveDrag =
  | ActiveDragBlock
  | ActiveDragSection
  | ActiveDragInsertBlock
  | ActiveDragInsertSection
  | ActiveDragOutlineBlock
  | ActiveDragOutlineSection
  | null;
export type StartSidebarTab =
  | typeof SIDEBAR_TAB_INSERT
  | typeof SIDEBAR_TAB_OUTLINE;
export type EndSidebarTab =
  | typeof SIDEBAR_TAB_ENTITY
  | typeof SIDEBAR_TAB_SETTINGS;
/**
 * Represents the state of the top bar (toolbar).
 */
export interface UiState {
  /**
   * Primary action.
   */
  primaryAction: {
    label: string;
  };
  /**
   * Additional actions.
   */
  secondaryActions: Array<Action>;
  /**
   * True if the end sidebar (right sidebar in LTR) is open.
   */
  endSidebarOpen: boolean;
  /**
   * True if the start sidebar (left sidebar in LTR) is open.
   */
  startSidebarOpen: boolean;
  /**
   * Active sidebar tab.
   */
  activeEndSidebarTab: EndSidebarTab;
  /**
   * Active sidebar tab.
   */
  activeStartSidebarTab: StartSidebarTab;
  /**
   * Selected section ID.
   */
  selectedSection: string | null;
  /**
   * Selected component ID.
   */
  selectedComponent: string | null;
  /**
   * Current mode.
   */
  mode: Mode;
  /**
   * The active drag type.
   */
  activeDrag: ActiveDrag;
}
