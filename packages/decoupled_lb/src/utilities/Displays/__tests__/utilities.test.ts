import { DisplayIdentifiers } from "@/types/displayMode.types.ts";
import { displayIdentifiersToId } from "@/utilities/Displays/utilities.ts";

describe("Id from component identifiers", () => {
  it("Should return ID", () => {
    const display: DisplayIdentifiers = {
      entityType: "foo",
      bundle: "bar",
      viewMode: "default",
    };
    expect(displayIdentifiersToId(display)).toEqual("foo.bar.default");
  });
});
