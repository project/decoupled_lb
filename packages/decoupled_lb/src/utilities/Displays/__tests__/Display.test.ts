import Display from "@/utilities/Displays/Display.ts";

const mockData = {
  type: "view" as const,
  entityType: "baz",
  bundle: "baz",
  viewMode: "default",
  components: {
    foo: {
      type: "",
      third_party_settings: {},
      settings: {},
      weight: 1,
      region: "content" as const,
    },
    bar: {
      type: "",
      third_party_settings: {},
      settings: {},
      weight: 0,
      region: "content" as const,
    },
  },
};
describe("Display", () => {
  it("Should sort components", () => {
    const display = new Display(mockData);
    expect(Object.keys(display.getSortedComponents())).toEqual(["bar", "foo"]);
  });
  it("Should return components", () => {
    const display = new Display(mockData);
    expect(display.getComponent("foo")).toEqual(mockData.components.foo);
  });
});
