import { DisplayIdentifiers } from "@/types/displayMode.types.ts";

export const displayIdentifiersToId = (display: DisplayIdentifiers): string =>
  `${display.entityType}.${display.bundle}.${display.viewMode}`;
