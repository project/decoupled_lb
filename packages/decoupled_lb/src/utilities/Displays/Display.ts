import {
  DisplayInterface,
  DisplaySettings,
  DisplayData,
} from "@/types/displayMode.types.ts";

class Display implements DisplayInterface {
  data: DisplayData;
  constructor(data: DisplayData) {
    this.data = data;
  }

  getComponent(name: string): DisplaySettings | false {
    if (!(name in this.data.components)) {
      return false;
    }
    return this.data.components[name];
  }

  getSortedComponents(): Record<string, DisplaySettings> {
    return Object.entries(this.data.components)
      .sort(([, component1], [, component2]) => {
        return component1.weight - component2.weight;
      })
      .reduce(
        (carry, [name, component]) => ({ ...carry, [name]: component }),
        {},
      );
  }
}

export default Display;
