import {
  ActiveDrag,
  DRAG_BLOCK,
  DRAG_INSERT_BLOCK,
  DRAG_INSERT_SECTION,
  DRAG_OUTLINE_BLOCK,
  DRAG_OUTLINE_SECTION,
  DRAG_SECTION,
} from "@/types/ui.types.ts";
import { BlockInfo, SectionInfo } from "@/types/block.types.ts";
import { BeforeCapture, DropResult } from "react-beautiful-dnd";
import { LayoutBuilderDispatch } from "@/state/store.ts";
import { Dictionary } from "@reduxjs/toolkit";
import {
  setActiveDrag,
  setSelectedComponent,
} from "@/state/Slices/Ui/uiSlice.ts";
import { BLOCK_PICKER_DROPPABLE_ID } from "@/components/Ui/Insert/BlockPicker.tsx";
import {
  addNewBlock,
  addNewBlockAndSection,
  addNewSection,
  moveBlock,
  reorderLayoutSections,
} from "@/state/Slices/Layout/layoutSlice.ts";
import { SECTION_PICKER_DROPPABLE_ID } from "@/components/Ui/Insert/SectionPicker.tsx";
import { OUTLINE_DROPPABLE_ID } from "@/components/Ui/Outline/Outline.tsx";
import { DROPPABLE_ID } from "@/components/LayoutEditor/DragContext.tsx";
import { NormalizedLayout } from "@/types/layout.types.ts";

export const handleDragEnd = (
  result: DropResult,
  selectedComponent: string | null,
  dispatch: LayoutBuilderDispatch,
  activeDrag: ActiveDrag,
  sectionInfo: Dictionary<SectionInfo>,
) => {
  const { destination, source } = result;
  dispatch(setActiveDrag(null));
  if (!destination) {
    return;
  }
  const restoreSelectedComponent = () => {
    if (selectedComponent) {
      dispatch(setSelectedComponent({ uuid: selectedComponent }));
    }
  };
  // Dragging in a new block from the block picker.
  if (
    source.droppableId === BLOCK_PICKER_DROPPABLE_ID &&
    activeDrag !== null &&
    activeDrag.type === DRAG_INSERT_BLOCK
  ) {
    if (destination.droppableId === DROPPABLE_ID) {
      // Dropping outside a section.
      dispatch(
        addNewBlockAndSection({
          at: destination.index,
          blockInfo: activeDrag.info,
          sectionInfo: sectionInfo[
            activeDrag.info.default_layout
          ] as SectionInfo,
        }),
      );
      dispatch(setActiveDrag(null));
      return;
    }
    // Dropping into a region.
    dispatch(addNewBlock({ at: destination, blockInfo: activeDrag.info }));
    dispatch(setActiveDrag(null));
    return;
  }
  // Dragging in a new section from the section picker.
  if (
    source.droppableId === SECTION_PICKER_DROPPABLE_ID &&
    activeDrag !== null &&
    activeDrag.type === DRAG_INSERT_SECTION &&
    destination.droppableId === DROPPABLE_ID
  ) {
    // Dropping a new section
    dispatch(
      addNewSection({
        at: destination.index,
        sectionInfo: activeDrag.info,
      }),
    );
    dispatch(setActiveDrag(null));
    return;
  }
  // Moving a whole section, either in the outline or the main layout.
  if (
    (source.droppableId === DROPPABLE_ID &&
      destination.droppableId === DROPPABLE_ID) ||
    (source.droppableId === OUTLINE_DROPPABLE_ID &&
      destination.droppableId === OUTLINE_DROPPABLE_ID)
  ) {
    dispatch(
      reorderLayoutSections({ from: source.index, to: destination.index }),
    );
    dispatch(setActiveDrag(null));
    restoreSelectedComponent();
    return;
  }
  if (
    activeDrag !== null &&
    (activeDrag.type === DRAG_BLOCK || activeDrag.type === DRAG_OUTLINE_BLOCK)
  ) {
    // Allow for outline-based drag/drop regions.
    const componentUuid = activeDrag.info.uuid;
    const [finalDroppableIdFrom] = source.droppableId.split(":");
    const [finalDroppableIdTo] = destination.droppableId.split(":");
    dispatch(
      moveBlock({
        from: { ...source, droppableId: finalDroppableIdFrom },
        to: { ...destination, droppableId: finalDroppableIdTo },
        componentUuid,
      }),
    );
    dispatch(setActiveDrag(null));
    restoreSelectedComponent();
    return;
  }
};

export const deriveActiveDrag = (
  draggableId: string,
  blockInfo: Dictionary<BlockInfo>,
  sectionInfo: Dictionary<SectionInfo>,
  layout: NormalizedLayout,
): ActiveDrag => {
  const parts = draggableId.split(":");
  const type = parts.shift();
  if (
    !type ||
    ![
      DRAG_SECTION,
      DRAG_BLOCK,
      DRAG_INSERT_BLOCK,
      DRAG_INSERT_SECTION,
      DRAG_OUTLINE_BLOCK,
      DRAG_OUTLINE_SECTION,
    ].includes(type)
  ) {
    // Not supported.
    return null;
  }
  const itemId = parts.join(":");
  if (type === DRAG_SECTION || type === DRAG_OUTLINE_SECTION) {
    return {
      type: type,
      info: layout.sections[itemId],
    };
  }
  if (type === DRAG_BLOCK || type === DRAG_OUTLINE_BLOCK) {
    return {
      type: type,
      info: layout.components[itemId],
    };
  }
  let info;
  if (type === DRAG_INSERT_BLOCK && (info = blockInfo[itemId] ?? null)) {
    return { type: DRAG_INSERT_BLOCK, info };
  }
  if (type === DRAG_INSERT_SECTION && (info = sectionInfo[itemId] ?? null)) {
    return { type: DRAG_INSERT_SECTION, info };
  }
  return null;
};

export const handleBeforeDrag = (
  dispatch: LayoutBuilderDispatch,
  result: BeforeCapture,
  blockInfo: Dictionary<BlockInfo>,
  sectionInfo: Dictionary<SectionInfo>,
  layout: NormalizedLayout,
) => {
  dispatch(setSelectedComponent({ uuid: null }));
  dispatch(
    setActiveDrag(
      deriveActiveDrag(result.draggableId, blockInfo, sectionInfo, layout),
    ),
  );
};
