import { FetchBaseQueryError } from "@reduxjs/toolkit/query";
import { SerializedError } from "@reduxjs/toolkit";

/**
 * Type predicate to narrow an unknown error to `FetchBaseQueryError`
 */
const isFetchBaseQueryError = (
  error: unknown,
): error is FetchBaseQueryError => {
  return typeof error === "object" && error != null && "status" in error;
};

/**
 * Type predicate to narrow an unknown error to an object with a string 'message' property
 */
const isErrorWithMessage = (error: unknown): error is { message: string } => {
  return (
    typeof error === "object" &&
    error != null &&
    "message" in error &&
    typeof error.message === "string"
  );
};

export const decodeError = (
  error: FetchBaseQueryError | SerializedError,
  defaultError: string,
): string => {
  if (isFetchBaseQueryError(error)) {
    return "error" in error ? error.error : JSON.stringify(error.data);
  }
  if (isErrorWithMessage(error)) {
    return error.message;
  }
  return defaultError;
};
