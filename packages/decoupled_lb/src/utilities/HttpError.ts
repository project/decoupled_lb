import { ErrorSet } from "../types/errorSet.ts";

export class HttpError extends Error {
  errors: ErrorSet;
  constructor(message: string, errors: ErrorSet) {
    super(message);
    this.errors = errors;
  }
  getErrors() {
    return this.errors;
  }
}
