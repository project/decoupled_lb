export type Translation = {
  // eslint-disable-next-line no-unused-vars
  t: (text: string, args: Record<string, string>) => string;
};

const SimpleTranslation: Translation = {
  t: (text, args) => formatString(text, args),
};

let currentTranslation = SimpleTranslation;

export const setTranslation = (translation: Translation) => {
  currentTranslation = translation;
};

export const t = (text: string, args: Record<string, string> = {}) =>
  currentTranslation.t(text, args);

export const checkPlain = (str: string | number) =>
  str
    .toString()
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;")
    .replace(/"/g, "&quot;")
    .replace(/'/g, "&#39;");

const stringReplace = (
  str: string,
  args: Record<string, string>,
  keys: Array<string> | null,
): string => {
  if (str.length === 0) {
    return str;
  }

  // If the array of keys is not passed then collect the keys from the args.
  if (!Array.isArray(keys)) {
    keys = Object.keys(args || {});

    // Order the keys by the character length. The shortest one is the first.
    keys.sort((a, b) => a.length - b.length);
  }

  if (keys.length === 0) {
    return str;
  }

  const key = keys.pop();
  if (!key) {
    return str;
  }
  const fragments = str.split(key);

  if (keys.length) {
    for (let i = 0; i < fragments.length; i++) {
      // Process each fragment with a copy of remaining keys.
      fragments[i] = stringReplace(fragments[i], args, keys.slice(0));
    }
  }

  return fragments.join(args[key]);
};
const formatString = (
  str: string,
  args: Record<string, string> = {},
): string => {
  // Keep args intact.
  const processedArgs: Record<string, string> = {};
  // Transform arguments before inserting them.
  Object.keys(args).forEach((key) => {
    processedArgs[key] = checkPlain(args[key]);
  });

  return stringReplace(str, processedArgs, null);
};
