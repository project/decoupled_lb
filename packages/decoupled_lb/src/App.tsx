import LayoutEditor from "./components/LayoutEditor/LayoutEditor.tsx";
import blockPlugins from "./debug/allBlockPlugins.ts";
import layoutPlugins from "./debug/allLayoutPlugins.ts";
import formatterPlugins from "./debug/allFormatterPlugins.ts";
import widgetPlugins from "./debug/allWidgetPlugins.ts";

// For local development we just load all the plugins on disk.
function App() {
  return (
    <>
      <LayoutEditor
        title={"Editing Vanilla sponge cake"}
        blockPlugins={blockPlugins}
        layoutPlugins={layoutPlugins}
        widgetPlugins={widgetPlugins}
        formatterPlugins={formatterPlugins}
        baseUrl={"/"}
        sectionStorage={"node.5"}
        sectionStorageType={"overrides"}
      />
    </>
  );
}

export default App;
