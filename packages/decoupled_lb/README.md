# Decoupled layout builder

This package provides Drupal's decoupled layout builder components for the @drupal/decoupled_lb npm package.

Extensive documentation is provided in the [project's storybook](https://project.pages.drupalcode.org/decoupled_lb)

## This is not a module

This is a package for development of Decoupled Layout Builder components.

If you're looking for a way to integrate this functionality into Drupal, you probably want the [Decoupled Layout Builder module](https://drupal.org/project/decoupled_lb_api).

