import { defineConfig } from "vite";
import react from "@vitejs/plugin-react-swc";
import ckeditor5 from "@ckeditor/vite-plugin-ckeditor5";
import { resolve } from "node:path";
import { createRequire } from "node:module";
import fg from "fast-glob";
import replace from "@rollup/plugin-replace";
import dts from "vite-plugin-dts";

const require = createRequire(import.meta.url);

// https://vitejs.dev/config/
const entry = fg
  .sync([
    "./src/components/BlockPlugins/*.tsx",
    "./src/components/Formatters/*.tsx",
    "./src/components/Widgets/*.tsx",
    "./src/components/Layouts/*.tsx",
    "./src/index.ts",
  ])
  .filter(
    (name) =>
      !name.includes("__tests__") &&
      !name.includes(".stories.") &&
      !name.includes("Fallback"),
  );
export default defineConfig(({ mode }) => {
  const plugins = [
    react(),
    ckeditor5({ theme: require.resolve("@ckeditor/ckeditor5-theme-lark") }),
  ];
  if (mode === "production" && !(process.argv[1] || "").includes("storybook")) {
    plugins.push([
      replace({
        "process.env.NODE_ENV": JSON.stringify(mode),
        preventAssignment: true,
        exclude: "*/node_modules/*",
      }),
    ]);
    plugins.push([dts({ rollupTypes: true })]);
  }
  return {
    envPrefix: ["VITE_", "STORYBOOK_"],
    optimizeDeps: {
      include: ["react", "react-dom", "react/jsx-runtime"],
    },
    build: {
      minify: "esbuild",
      cssCodeSplit: true,
      lib: {
        name: "LayoutEditor",
        formats: ["es"],
        entry,
      },
      rollupOptions: {
        external: ["react", "react-dom", "react/jsx-runtime"],
      },
    },
    plugins,
    resolve: {
      alias: [{ find: "@", replacement: resolve(__dirname, "src") }],
    },
  };
});
