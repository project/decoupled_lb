import type { Preview } from "@storybook/react";
import { initialize, mswLoader } from "msw-storybook-addon";
import { handlers } from "../src/mocks/handlers";

initialize({
  serviceWorker: {
    // Points to the custom location of the Service Worker file.
    url: `./mockServiceWorker.js`,
  },
});

const preview: Preview = {
  parameters: {
    msw: {
      handlers,
    },
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
  loaders: [mswLoader],
};

export default preview;
