import "@testing-library/jest-dom";
import "isomorphic-fetch";

import { server } from "@/mocks/server.ts";

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());
