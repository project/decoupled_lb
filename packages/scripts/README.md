# Scripts

This package is used to help module and theme maintainers build layout, block, formatter and widget components for use
in with the Decoupled Layout Builder

## Installation

`npm i -D @drupal/scripts`

## Creating a new project or setting up an existing one

Run

`drupal-scripts init`

And follow the prompts. If you already have a `package.json` this will add a new `scripts.build` entry to your file as
well as setup the required dependencies.

### Manually adding to an existing project

If you don't want to use the `init` command, you can add build support by editing your package.json and adding the
following build entry to your scripts section

```json
{
  "scripts": {
    "build": "drupal-scripts build -o js/dist js/src"
  }
}

```

Adjust the output (`-o`) and input path as necessary

## Scaffolding/Generating components

To generate components use `drupal-scripts generate` and follow the prompts

![Screencast of generate in action](./images/generate.gif)
