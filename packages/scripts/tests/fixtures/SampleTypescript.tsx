import React from "react";
import { BlockConfiguration, LayoutBlock } from "@drupal/decoupled_lb";
import { DefaultBlockSettings } from "@drupal/decoupled_lb";

interface MyConfiguration extends BlockConfiguration {
  foo: string;
  bar: number;
}

export const Preview: React.FC<LayoutBlock<MyConfiguration>> = (component) => {
  const { configuration } = component;
  const { foo, bar } = configuration;
  return <div data-foo={foo}>{bar}</div>;
};

export const Edit = Preview;
export const Settings = DefaultBlockSettings;
export const id = "my_component";
