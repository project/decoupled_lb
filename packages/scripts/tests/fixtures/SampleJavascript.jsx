import { DefaultBlockSettings } from "@drupal/decoupled_lb";
export const Preview = (component) => {
  const { configuration } = component;
  const { foo, bar } = configuration;
  return <div data-foo={foo}>{bar}</div>;
};

export const Edit = Preview;
export const Settings = DefaultBlockSettings;
export const id = "my_component";
