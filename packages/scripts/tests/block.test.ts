import createBlock from "../src/templates/block";

describe("Generate block", () => {
  it("Should generate a block", () => {
    expect(createBlock("Jimmy")).toMatchSnapshot();
    expect(createBlock("Timmy", true)).toMatchSnapshot();
  });
});
