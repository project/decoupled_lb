import createWidget from "../src/templates/widget";

describe("Generate widget", () => {
  it("Should generate a widget", () => {
    expect(createWidget("Jimmy")).toMatchSnapshot();
    expect(createWidget("Timmy", true)).toMatchSnapshot();
  });
});
