import { resolve } from "node:path";
import { readFileSync } from "node:fs";
import spawn from "cross-spawn";

const script = resolve(__dirname, "../dist/index.js");

describe("Build tests", () => {
  it("Should build Javascript and Typescript", async () => {
    const cwd = resolve(__dirname, "../../..");
    const args = [
      script,
      "build",
      "-o",
      resolve(__dirname, "output"),
      resolve(__dirname, "fixtures"),
    ];
    spawn.sync("node", args, {
      stdio: "inherit",
      cwd,
      env: {
        NODE_ENV: "production",
      },
    });
    const js = readFileSync(
      resolve(__dirname, "output", "SampleJavascript.js"),
    );
    expect(js.toString()).toMatchSnapshot();
    const ts = readFileSync(
      resolve(__dirname, "output", "SampleTypescript.js"),
    );
    expect(ts.toString()).toMatchSnapshot();
  });
});
