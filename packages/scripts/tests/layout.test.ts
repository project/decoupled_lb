import createLayout from "../src/templates/layout";

describe("Generate layout", () => {
  it("Should generate a layout", () => {
    expect(createLayout("first,second", false, "Jimmy")).toMatchSnapshot();
    expect(createLayout("first,second", true, "Timmy")).toMatchSnapshot();
  });
});
