import { resolve } from "node:path";
import updatePackageJson from "../src/init.ts";

describe("Init tests", () => {
  it("Should init package.json", async () => {
    const packageJson = updatePackageJson(
      resolve(__dirname, "project/new"),
      "cooltown",
    );
    expect(packageJson).toMatchSnapshot();
  });
  it("Should update existing package.json", async () => {
    const packageJson = updatePackageJson(
      resolve(__dirname, "project/existing"),
      "radsville",
    );
    expect(packageJson).toMatchSnapshot();
  });
});
