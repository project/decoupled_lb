import createFormatter from "../src/templates/formatter";

describe("Generate formatter", () => {
  it("Should generate a formatter", () => {
    expect(createFormatter("Jimmy")).toMatchSnapshot();
    expect(createFormatter("Timmy", true)).toMatchSnapshot();
  });
});
