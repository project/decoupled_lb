import { defineConfig } from "vite";

export default defineConfig({
  build: {
    target: "node18",
    minify: "esbuild",
    outDir: "dist",
    lib: {
      name: "drupal-scripts",
      formats: ["es"],
      entry: "src/index.ts",
      fileName: "index",
    },
    rollupOptions: {
      external: [
        "fsevents",
        "@vitejs/plugin-react-swc",
        "commander",
        "inquirer",
        "vite",
        "chalk",
        "node:fs",
        "node:url",
        "node:path",
        "cross-spawn",
        "fast-glob",
      ],
    },
  },
});
