const code = (name: string, typescript: boolean = false) => {
  if (typescript) {
    return `import React from "react";
import { WidgetProps, Field, FieldValues, DisplayConfiguration } from "@drupal/decoupled_lb";

/**
 * Most fields have a single 'value' property but more complex fields like e.g.
 * entity_reference have different properties like 'target_id'. Specify the
 * shape of your field items to get additional type checking.
 */
export interface ${name}FieldValues extends FieldValues {
  value: string;
}

/**
 * Many widgets have configuration settings, you can specify your widget
 * settings here to get additional type checking.
 */
export interface ${name}FieldSettings extends DisplayConfiguration {
  size: number;
}
export const Widget: React.FC<
  WidgetProps<${name}FieldValues, ${name}FieldSettings>
> = ({ itemValues, fieldName , update}) => {
  return (
    <Field
      multiple={true}
      items={itemValues.map((item, delta) => {
        // Here you would construct the markup for your widget.
        // E.g. you might use a text input or similar.
        // \`item\` is a single field item with keys that align with
        // ${name}FieldValues.
        return <div key={delta}>
          <label htmlFor={\`\${fieldName}-\${delta}\`}>\${fieldName}</label>
          <input
            type="text"
            value={item.value}
            id={\`\${fieldName}-\${delta}\`}
            onChange={(e) => {
              // Here we're updating the field values for this delta.
              const value = e.target.value;
              const newValues = [...itemValues];
              newValues.splice(delta, 1, {
                value
              });
              // And using the passed update function to change the layout state.
              update(newValues);
            }}
          />
        </div>
      })}
    />
  );
};
`;
  }
  return `import React from "react";
import { Field } from "@drupal/decoupled_lb";

export const Widget = ({ itemValues, fieldName, update }) => {
  return (
    <Field
      multiple={true}
      items={itemValues.map((item, delta) => {
        // Here you would construct the markup for your widget.
        // E.g. you might use a text input or similar.
        // \`item\` is a single field item with keys that align with
        // the field properties.
        return (
          <div key={delta}>
            <label htmlFor={\`\${fieldName}-\${delta}\`}>\${fieldName}</label>
            <input
              type="text"
              /**
               * Most fields have a single 'value' property but more complex
               * fields like e.g. entity_reference have different properties
               * like 'target_id'. The example assumes the property is 'value'.
               */
              value={item.value}
              id={\`\${fieldName}-\${delta}\`}
              onChange={(e) => {
                // Here we're updating the field values for this delta.
                const value = e.target.value;
                const newValues = [...itemValues];
                newValues.splice(delta, 1, {
                  value,
                });
                // And using the passed update function to change the layout state.
                update(newValues);
              }}
            />
          </div>
        );
      })}
    />
  );
};
`;
};

export default code;
