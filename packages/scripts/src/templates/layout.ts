const code = (
  regions: string,
  typescript: boolean = false,
  name: string,
): string => {
  let output = `import React from "react";
`;
  if (typescript) {
    output = `${output}import {
  DefaultLayoutSettings,
  LayoutPluginProps,
  Region,
} from "@drupal/decoupled_lb";

interface ${name}Props extends LayoutPluginProps {};

export const Preview: React.FC<${name}Props> = ({
  section,
  sectionProps,
  regions,
}) => {
    return (
      <div {...sectionProps} className={\`section section--\${section.layout_id}\`}>
`;
  } else {
    output = `${output}import {
  Region,
} from "@drupal/decoupled_lb";

export const Preview = ({
  section,
  sectionProps,
  regions,
}) => {
    return (
      <div {...sectionProps} className={\`
    section
    section--\${section.layout_id}
  \`}>
`;
  }
  regions.split(",").forEach((regionId) => {
    output = `${output}
     <Region
      regionId={regions.${regionId}.id}
    className="layout__region layout__region--${regionId}"
    />
`;
  });

  output = `${output}
  </div>
  );
};

export const Settings = DefaultLayoutSettings;`;
  return output;
};

export default code;
