const code = (name: string, typescript: boolean = false) => {
  if (typescript) {
    return `
import React from "react";

import { Field, FormatterProps, FieldValues } from "@drupal/decoupled_lb";

/**
 * Most fields have a single 'value' property but more complex fields like e.g.
 * entity_reference have different properties like 'target_id'. Specify the
 * shape of your field items to get additional type checking.
 */
interface ${name}FieldShape extends FieldValues {
  value: string
}
export const Formatter: React.FC<FormatterProps<${name}FieldShape>> = ({
  itemValues,
}) => {
  return (
    <Field
      items={itemValues.map((item) => ({
        content: item.value,
      }))}
    />
  );
};

`;
  }
  return `
import React from "react";

import { Field } from "@drupal/decoupled_lb";

export const Formatter = ({
  itemValues,
}) => {
  return (
    <Field
      items={itemValues.map((item) => ({
        // This assumes the field items have a value property that can is output
        // - your field type might have different properties so you should
        // look at the field type plugin to determine what you want to print
        // here. For example text-long fields have a processed field you
        // may want to use instead.
        content: item.value,
      }))}
    />
  );
};
`;
};

export default code;
