const code = (name: string, typescript: boolean = false): string => {
  if (typescript) {
    return `import React from "react";
import {
  useDispatch,
  updateBlock,
  LayoutBlock,
  BlockConfiguration,
  BlockTitle,
  DefaultBlockSettings,
} from "@drupal/decoupled_lb";

/**
 * Blocks in Drupal can define their own configuration. To get additional type
 * checking of your block, you can define the shape of your configuration with
 * an interface that extends BlockConfiguration
 */
interface ${name}Configuration extends BlockConfiguration {
  // Add additional keys as you need.
  someSetting: string;
}

/**
 * This component should represent the block when in preview (select) mode.
 */
export const Preview: React.FC<LayoutBlock<${name}Configuration>> = ({
  configuration,
  uuid,
}) => {
  // This is the block plugin ID from Drupal.
  const { id } = configuration;
  return (
    <div>
      {/* The utility BlockTitle component has Preview and Edit modes too */}
      <BlockTitle.Preview {...configuration} />
      <div>
        {/* Here you would do something with your component configuration
        and construct the preview representation of your block.
        */}
        This block ({uuid}) uses the {id} plugin It has setting{" "}
        {configuration.someSetting}
      </div>
    </div>
  );
};

/**
 * This component should represent the block when in edit mode.
 * If there is no inline-editing of your block, you can just return the Preview
 * component \`export const Edit = Preview\`
 */
export const Edit: React.FC<LayoutBlock<${name}Configuration>> = (
  component,
) => {
  // You can fetch the dispatcher to update state when edits occur.
  const dispatch = useDispatch();
  return (
    <div>
      {/* The utility BlockTitle component has Preview and Edit modes too */}
      <BlockTitle.Edit {...component} />
      <div>
        {/* Here you would do something with your component configuration
        and construct the edit representation of your block.
        */}
        <textarea
          value={component.configuration.someSetting}
          onChange={(e) => {
            dispatch(
              // The updateBlock reducer takes an object with keys componentId
              // and the new values.
              updateBlock({
                componentId: component.uuid,
                // In this case we're retaining the existing values and just
                // overwriting the someSetting value.
                values: {
                  ...component,
                  configuration: {
                    ...component.configuration,
                    someSetting: e.target.value,
                  },
                },
              }),
            );
          }}
        />
      </div>
    </div>
  );
};

/**
 * Your component can also export a settings component that is shown in the
 * sidebar. If you don't have specific settings you can just use the
 * DefaultBlockSettings component which lets the user change the block label
 * and visibility.
 */
export const Settings = DefaultBlockSettings;
`;
  }
  return `import React from "react";
import {
  useDispatch,
  updateBlock,
  BlockTitle,
  DefaultBlockSettings,
} from "@drupal/decoupled_lb";

/**
 * This component should represent the block when in preview (select) mode.
 */
export const Preview = ({
  configuration,
  uuid,
}) => {
  // This is the block plugin ID from Drupal.
  const { id } = configuration;
  return (
    <div>
      {/* The utility BlockTitle component has Preview and Edit modes too */}
      <BlockTitle.Preview {...configuration} />
      <div>
        {/* Here you would do something with your component configuration
        and construct the preview representation of your block.
        */}
        This block ({uuid}) uses the {id} plugin It has setting{" "}
        {configuration.someSetting}
      </div>
    </div>
  );
};

/**
 * This component should represent the block when in edit mode.
 * If there is no inline-editing of your block, you can just return the Preview
 * component \`export const Edit = Preview\`
 */
export const Edit = (
  component,
) => {
  // You can fetch the dispatcher to update state when edits occur.
  const dispatch = useDispatch();
  return (
    <div>
      {/* The utility BlockTitle component has Preview and Edit modes too */}
      <BlockTitle.Edit {...component} />
      <div>
        {/* Here you would do something with your component configuration
        and construct the edit representation of your block.
        */}
        <textarea
          value={component.configuration.someSetting}
          onChange={(e) => {
            dispatch(
              // The updateBlock reducer takes an object with keys componentId
              // and the new values.
              updateBlock({
                componentId: component.uuid,
                // In this case we're retaining the existing values and just
                // overwriting the someSetting value.
                values: {
                  ...component,
                  configuration: {
                    ...component.configuration,
                    someSetting: e.target.value,
                  },
                },
              }),
            );
          }}
        />
      </div>
    </div>
  );
};

/**
 * Your component can also export a settings component that is shown in the
 * sidebar. If you don't have specific settings you can just use the
 * DefaultBlockSettings component which lets the user change the block label
 * and visibility.
 */
export const Settings = DefaultBlockSettings;
`;
};

export default code;
