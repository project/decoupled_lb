import { build, InlineConfig } from "vite";
import react from "@vitejs/plugin-react-swc";
import fg from "fast-glob";

interface BuildConfig {
  filepath: string;
  outputDirectory: string;
}
export default (buildConfig: BuildConfig) => {
  const entry = fg.sync([`${buildConfig.filepath}/**/*.{tsx,jsx}`]);
  const config: InlineConfig = {
    optimizeDeps: {
      include: [
        "react",
        "react-dom",
        "react/jsx-runtime",
        "@drupal/decoupled_lb",
      ],
    },
    build: {
      minify: "esbuild",
      outDir: buildConfig.outputDirectory,
      lib: {
        formats: ["es"],
        entry,
      },
      rollupOptions: {
        external: [
          "react",
          "react-dom",
          "react/jsx-runtime",
          "@drupal/decoupled_lb",
        ],
      },
    },
    plugins: [react()],
  };
  return build(config);
};
