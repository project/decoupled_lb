import { resolve } from "node:path";
import { readFileSync, existsSync } from "node:fs";
const updatePackageJson = (path: string, name: string) => {
  const file = resolve(path, "package.json");
  const dependencies = {
    "@drupal/decoupled_lb": "^1.0.0",
    "@drupal/scripts": "^1.0.0",
    react: "^18.2.0",
    "react-dom": "^18.2.0",
  };
  const scripts = {
    build: "drupal-scripts js/src -o js/dist",
  };
  if (!existsSync(file)) {
    console.log("ℹ️ Creating a new package.json");
    return {
      name: `@drupal/${name}`,
      version: "1.0.0",
      description: `${name} module for Drupal`,
      scripts,
      type: "module",
      keywords: [
        "Drupal",
        "Decoupled",
        "Layout",
        "Builder",
        "React",
        "Components",
      ],
      license: "GPL-2.0-or-later",
      dependencies,
    };
  }
  const contents = JSON.parse(readFileSync(file).toString());
  return {
    ...contents,
    dependencies: {
      ...(contents.dependencies || {}),
      ...dependencies,
    },
    scripts: {
      ...(contents.scripts || {}),
      ...scripts,
    },
  };
};

export default updatePackageJson;
