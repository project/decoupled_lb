#!/usr/bin/env node
import { Command } from "commander";
import chalk from "chalk";
import build from "./build";
import inquirer from "inquirer";
import { existsSync, openSync, writeSync } from "node:fs";
import createLayout from "./templates/layout";
import createFormatter from "./templates/formatter";
import createWidget from "./templates/widget";
import createBlock from "./templates/block";
import fg from "fast-glob";
import { join, resolve } from "node:path";
import updatePackageJson from "./init.ts";
import spawn from "cross-spawn";

const program = new Command();

program
  .name("drupal-scripts")
  .description("CLI for building components for Decoupled Layout Builder")
  .version("1.0.0");

program
  .command("build")
  .description("Build your component")
  .option(
    "-o, --outdir <outdir>",
    "Directory to build your files into",
    "js/dist",
  )
  .argument("[filepath]", "path to your component file")
  .action((filepath, options) => {
    if (!filepath) {
      return inquirer
        .prompt([
          {
            type: "input",
            name: "filepath",
            message:
              "Enter the filepath to your components e.g. ./some/folder/src/",
            validate: (path) => {
              if (!existsSync(path)) {
                return `Folder ${path} does not exist`;
              }
              const matches = fg.sync([`${path}/**/*.{tsx,jsx}`]);
              if (matches.length === 0) {
                return `No .jsx or .tsx components found in ${path}`;
              }
              return true;
            },
          },
        ])
        .then((answers) => {
          build({
            ...answers,
            outputDirectory: options.outdir,
          })
            .then(() => {
              console.log(chalk.green(`✅ Bundled to ${options.outdir}`));
            })
            .catch((e) => {
              console.error(chalk.red(`❌ Build error: ${e}`));
            });
        })
        .catch((e) => {
          console.error(chalk.red(`❌ Build error: ${e}`));
        });
    }
    chalk.green(`⌛️ Building ${filepath}`);
    return build({
      outputDirectory: options.outdir,
      filepath,
    })
      .then(() => {
        console.log(chalk.green(`✅ Bundled to ${options.outdir}`));
      })
      .catch((e) => {
        console.error(chalk.red(`❌ Build error: ${e}`));
      });
  });

program
  .command("init")
  .description("Setup a project to build a component")
  .argument("<projectName>", "Project name to use")
  .argument("[directory]", "Project name to use")
  .action((projectName, directory) => {
    const path = resolve(process.cwd(), directory || "");
    const packageJson = updatePackageJson(path, projectName);
    const file = join(path, "package.json");
    const exists = existsSync(file);
    const fd = openSync(file, "w");
    writeSync(fd, JSON.stringify(packageJson, null, "  "));
    console.log(`✅ ${exists ? `Updated ${file}` : `Created ${file}`}`);
    return inquirer
      .prompt([
        {
          message: "Run `npm install`?",
          type: "confirm",
          name: "install",
        },
      ])
      .then((answers) => {
        if (answers.install) {
          spawn.sync("npm", ["install"], {
            stdio: "inherit",
            cwd: path,
          });
        }
        return;
      });
  });

program
  .command("generate")
  .description("Scaffold a component")
  .action(() => {
    return inquirer
      .prompt([
        {
          message: "What sort of component do you want to generate type",
          type: "list",
          name: "type",
          choices: [
            { name: "Formatter", value: "formatter", short: "f" },
            { name: "Widget", value: "widget", short: "w" },
            { name: "Layout", value: "layout", short: "l" },
            { name: "Block", value: "block", short: "b" },
          ],
        },
        {
          message:
            "What regions exist in your layout? (enter machine names separated by a comma)",
          type: "input",
          when: (answers) => answers.type === "layout",
          name: "regions",
          validate: (input) =>
            input.match(/[a-z_,]+/)
              ? true
              : "Please enter machine names separated by a comma",
        },
        {
          message: "What is the name of your component",
          type: "input",
          name: "name",
          validate: (input) => (input.length > 0 ? true : "Name is required"),
        },
        {
          message: "Where should the files be created?",
          default: "./js/src",
          type: "input",
          name: "path",
          validate: (path) => {
            if (path.length > 0 && existsSync(path)) {
              return true;
            }
            return path.length > 0
              ? `Path ${path} does not exist`
              : "Path is required";
          },
        },
        {
          message: "Select file format",
          type: "list",
          name: "language",
          choices: [
            { name: "Javascript", value: "jsx", short: "j" },
            { name: "Typescript", value: "tsx", short: "t" },
          ],
        },
      ])
      .then((answers) => {
        const { path, name, language, type } = answers;
        const filename = `${path}/${name}.${language}`;
        const doWrite = (contents: string) => {
          const fp = openSync(filename, "w");
          writeSync(fp, contents);
        };
        switch (type) {
          case "layout":
            doWrite(createLayout(answers.regions, language === "tsx", name));
            console.info(`💾 Layout component created at ${filename}`);
            break;
          case "formatter":
            doWrite(createFormatter(name, language === "tsx"));
            console.info(`💾 Formatter component created at ${filename}`);
            break;
          case "widget":
            doWrite(createWidget(name, language === "tsx"));
            console.info(`💾 Widget component created at ${filename}`);
            break;
          case "block":
            doWrite(createBlock(name, language === "tsx"));
            console.info(`💾 Block component created at ${filename}`);
            break;
          default:
            console.error(`The type: ${type} is not supported.`);
        }
      });
  });

program.parse();
