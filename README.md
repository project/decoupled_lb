# Decoupled Layout Builder

This is a mono-repo for development of npm packages for the Decoupled Layout Builder project.

## Packages

This mono-repo is used to develop the following packages

* [Drupal scripts][./packages/scripts/README.md] - Scripts for packaging Decoupled layout builder components from contrib
* [Decoupled Layout Builder](./packages/decoupled_lb/README.md) - The components used to build the Decoupled Layout Builder
